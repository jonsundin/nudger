import json

from .models import Convo


def serialize_convo(self):
    return json.dumps(
        {
            "id": self.id,
            "created_by": self.user.username,
            "name": self.name,
            "description": self.description,
            "file": str(self.file),
            "activated_on": self.activated_on.isoformat(),
            "is_public": self.is_public,
        }
    )


def unserialize_convo(serialized_convo):
    try:
        serialized_convo["user"] = serialized_convo["created_by"]
        del serialized_convo["created_by"]
        return Convo.objects.get(**json.loads(serialized_convo))
    except Convo.DoesNotExist:
        return {}
