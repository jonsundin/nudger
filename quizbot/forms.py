from django import forms
from django.forms import ModelForm

from quizbot.models import Convo


class ConvoForm(ModelForm):
    class Meta:
        model = Convo
        fields = ["file"]


class ConvoSpreadsheetForm(forms.Form):
    convo_name = forms.CharField(label="Conversation Name", required=True)
    convo_description = forms.CharField(label="Conversation Description", required=True)
    sheet_id = forms.CharField(label="Google Sheet ID", required=False)
    file = forms.FileField(required=False)

    def clean(self):
        cleaned_data = super().clean()
        uploaded_file = cleaned_data.get("file")
        sheet_id = cleaned_data.get("sheet_id")

        if not uploaded_file and not sheet_id:
            raise forms.ValidationError(
                "You must upload either a file or a Google Sheet id"
            )

        return cleaned_data
