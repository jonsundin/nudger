import io
import re
import urllib.request
import yaml
from collections import abc
from pathlib import Path

import numpy as np
import pandas as pd
from django.conf import settings

from quizbot.models import State, Message, Trigger, Convo, StateFunction

from nudger.constants import LANGS


def coerce_dialog_tree_series(states):
    """Ensure that (yamlfilepath | list) -> dict -> Series"""
    if isinstance(states, pd.Series):
        return states
    if isinstance(states, (str, Path)):
        with Path(states).open() as fin:
            states = yaml.full_load(fin)
    if isinstance(states, io.TextIOWrapper):
        states = yaml.full_load(states)
    if isinstance(states, abc.Mapping):
        return pd.Series(states)
    if isinstance(states, (list, np.ndarray)):
        states = pd.Series(
            states, index=[s.get("name", str(i)) for (i, s) in enumerate(states)]
        )
        states.index.name = "name"
        return states
    raise ValueError(
        f"Unable to coerce {type(states)} into pd.Series:\n  states={str(states)[:130]}..."
    )


def convoyaml_to_convomodel(graph, user, convo_name, convo_description) -> Convo:
    convos = [
        obj
        for obj in Convo.objects.filter(user=user)
        if convo_name in obj.name and re.match(r"\d+$", obj.name.split("_")[-1])
    ]
    max_version = max([int(obj.name.split("_")[-1]) for obj in convos] or [0])
    new_convo_name = (
        f"{convo_name}_{max_version + 1}"
        if max_version or len(Convo.objects.filter(user=user, name=convo_name))
        else convo_name
    )
    new_convo = Convo.objects.create(
        user=user,
        file=graph,
        name=new_convo_name,
        description=convo_description,
    )

    if settings.USE_S3:
        url = f"{settings.MEDIA_URL}{new_convo.file.name}"
        response = urllib.request.urlopen(url)
        file_path = io.TextIOWrapper(
            response, encoding="utf-8"
        )  # using S3 as an object storage
    else:
        file_path = (
            f"{settings.MEDIA_ROOT}/{new_convo.file.name}"  # using local storage
        )

    DIALOG_TREE = coerce_dialog_tree_series(file_path)
    dialog = {node["name"]: node for node in DIALOG_TREE}

    create_states(dialog, new_convo)
    # create_messages(dialog, new_convo)
    create_triggers(dialog, new_convo)
    # create_functions(dialog, new_convo)
    return new_convo


def create_states(dialog, convo):
    nlp_algo = dialog["start"]["nlp"]
    for name in dialog:
        if "nlp" in dialog[name]:
            nlp_algo = dialog[name]["nlp"]
        State.objects.create(state_name=name, convo=convo, nlp_algo=nlp_algo)
    return


# def create_messages(dialog, convo):
#     for s_name in dialog:
#         state = State.objects.get(
#             state_name=s_name,
#             convo=convo,
#         )
#         for lang in LANGS.values():
#             # Makes an entry in the Message table for each English message of a State
#             for i, msg in enumerate(dialog[s_name].get("actions", {}).get(lang, "")):
#                 Message.objects.create(
#                     state=state,
#                     sequence_number=i,
#                     bot_text=msg,
#                     lang=lang,
#                 )
#     return


def create_triggers(dialog, convo):
    lang = None
    for state_name, state_data in dialog.items():
        if state_name == "start":
            lang = state_data["lang"]

        from_state = State.objects.get(
            state_name=state_name,
            convo=convo,
        )
        for trig_data in state_data["triggers"]:
            to_state = State.objects.get(state_name=trig_data["target"], convo=convo)
            Trigger.objects.create(
                from_state=from_state,
                to_state=to_state,
                intent_text=trig_data["user_text"],
                lang=lang,
                is_button=False,
            )
        # for lang in LANGS.values():
        #     # TODO: See if this try can be simplified to test whether there is a langauge represented and then take the stuff if it is or pass if it isn't

        #     lang_assets = get_lang_assets(dialog, state_name, lang)
        #     triggers, btns, functions_to_call = (
        #         lang_assets["triggers"],
        #         lang_assets["buttons"],
        #     )
        #     for is_btn, items in enumerate([triggers, btns]):
        #         for intent_text, to_state_name in items:
        #             to_state = State.objects.get(state_name=to_state_name, convo=convo)
        #             Trigger.objects.create(
        #                 from_state=from_state,
        #                 to_state=to_state,
        #                 intent_text=intent_text,
        #                 update_kwargs=functions_to_call,
        #                 lang=lang,
        #                 is_button=is_btn,
        #             )
    return None


# def create_triggers(dialog, convo):
#     for s_name in dialog:
#         from_state = State.objects.get(
#             state_name=s_name,
#             convo=convo,
#         )
#         for lang in LANGS.values():
#             # TODO: See if this try can be simplified to test whether there is a langauge represented and then take the stuff if it is or pass if it isn't

#             lang_assets = get_lang_assets(dialog, s_name, lang)
#             triggers, btns, functions_to_call = (
#                 lang_assets["triggers"],
#                 lang_assets["buttons"],
#                 lang_assets["functions_to_call"],
#             )
#             for is_btn, items in enumerate([triggers, btns]):
#                 for intent_text, to_state_name in items:
#                     to_state = State.objects.get(state_name=to_state_name, convo=convo)
#                     Trigger.objects.create(
#                         from_state=from_state,
#                         to_state=to_state,
#                         intent_text=intent_text,
#                         update_kwargs=functions_to_call,
#                         lang=lang,
#                         is_button=is_btn,
#                     )
#     return None


# def create_functions(dialog, convo):
#     for state_name, state_assets in dialog.items():
#         state = State.objects.get(
#             state_name=state_name,
#             convo=convo,
#         )
#         try:
#             for key, fun_name in state_assets["functions"].items():
#                 StateFunction.objects.create(
#                     state=state, mapped_key_name=key, function_name=fun_name
#                 )
#         except KeyError:
#             continue
#     return


def get_lang_assets(dialog, state_name, lang):
    """Returns language functions to call, buttons and triggers"""
    try:
        functions_to_call = dict(
            dialog[state_name].get("triggers", {}).get("functions", "").items()
        )
    except:
        functions_to_call = None

    try:
        buttons = list(dialog[state_name].get("buttons", {}).get(lang, "").items())
    except AttributeError:
        buttons = list(dialog[state_name].get("buttons", {}).get(lang, ""))

    try:
        triggers = list(dialog[state_name].get("triggers", {}).get(lang, "").items())
    except AttributeError:
        triggers = list(dialog[state_name].get("triggers", {}))

    return {
        "functions_to_call": functions_to_call,
        "buttons": buttons,
        "triggers": triggers,
    }
