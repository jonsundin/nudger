import random
import string

from django.db import models, IntegrityError
from django.contrib.auth.models import User
from django.contrib.sessions.models import Session
from django.db import models, IntegrityError

from nudger.constants import (
    MESSAGE_DIRECTION_CHOICES,
    SENDER_TYPE_CHOICES,
    LANGUAGE_CHOICES,
)


class ConversationTracker(models.Model):
    student_id = models.CharField(
        null=False,
        max_length=100,
    )
    state_name = models.CharField(
        max_length=100,
        blank=True,
        null=True,
    )
    updated_on = models.DateTimeField(auto_now=True)


class Convo(models.Model):
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        default=None,
    )
    file = models.FileField(
        upload_to="documents/",
        null=True,
        blank=False,
        default=None,
    )
    name = models.CharField(
        max_length=100,
        blank=False,
        null=False,
        default="default",
    )
    description = models.CharField(
        max_length=100,
        blank=False,
        null=False,
        default="default",
    )
    activated_on = models.DateTimeField(
        auto_now_add=True,
        editable=False,
    )
    is_public = models.BooleanField(
        blank=False,
        null=False,
        default=False,
    )

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=[
                    "user",
                    "name",
                ],
                name="composite_key",
            )
        ]

    def __str__(self):
        return f"ID: {self.id}; BY: {self.user.username}; NAME: {self.name}; DESCRIPTION: {self.description}"


class State(models.Model):
    # Foreign key to itself
    convo = models.ForeignKey(
        Convo,
        on_delete=models.CASCADE,
    )
    next_states = models.ManyToManyField(
        "self",
        through="Trigger",
        through_fields=(
            "from_state",
            "to_state",
        ),
    )
    state_name = models.CharField(
        null=False,
        max_length=255,
        help_text="The title the system uses to recognize the state",
    )
    level = models.IntegerField(
        null=True,
        help_text="An optional field that tells the depth of the action",
    )
    nlp_algo = models.CharField(
        null=False,
        max_length=255,
        help_text="NLP algorithm used to compare trigger text with user text",
        default="keyword",
    )

    # HL: should be in the Trigger table because they are defined by what the user has said?
    update_args = models.JSONField(null=True)
    update_kwargs = models.JSONField(null=True)
    update_context = models.JSONField(
        null=True,
        help_text="May hold information such as gender, location, or history of previous exchanges",
    )

    # HL: What creates the association between what the bot says and a state name/id ?

    def __str__(self):
        return self.state_name


class Message(models.Model):
    """A table for messages the bot can send.
    Each row contains one message written in one language.
    """

    sequence_number = models.IntegerField(
        blank=False,
        null=False,
    )
    state = models.ForeignKey(
        State,
        null=False,
        blank=False,
        on_delete=models.CASCADE,
    )
    bot_text = models.TextField(
        null=False,
        help_text="The message of the state written in English",
    )
    lang = models.CharField(
        choices=LANGUAGE_CHOICES,
        default="en",
        max_length=100,
    )


class Trigger(models.Model):
    """A table of supported user inputs that connect two states.
    Users can enter the intent_text by button click or text input.
    Each row is one trigger option written in one language.
    """

    from_state = models.ForeignKey(
        State,
        related_name="rel_from_state",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
    )
    to_state = models.ForeignKey(
        State,
        related_name="rel_to_state",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
    )
    intent_text = models.CharField(
        null=False,
        max_length=1024,
        help_text="The user utterance that triggers a transition to a new bot state.",
    )
    update_args = models.JSONField(null=True)
    update_kwargs = models.JSONField(null=True)
    # FIXME: We need an nlp = CharField(null=True) to define the NLU algorithm
    #        used to recognize user intents (Trigger messages)
    lang = models.CharField(
        choices=LANGUAGE_CHOICES,
        default="en",
        max_length=100,
    )
    is_button = models.BooleanField(default=False)

    def trigger_to_dict(self):
        return {
            "from_state": self.from_state,
            "to_state": self.to_state,
            "intent_text": self.intent_text,
            "update_args": self.update_args,
            "update_kwargs": self.update_kwargs,
            "lang": self.lang,
            "is_button": self.is_button,
        }

    @staticmethod
    def stringify(trig_as_dict: dict) -> dict:
        for key, value in trig_as_dict.copy().items():
            trig_as_dict[key] = str(value)
        return trig_as_dict


class StateFunction(models.Model):
    state = models.ForeignKey(State, null=False, blank=False, on_delete=models.CASCADE)
    mapped_key_name = models.CharField(
        max_length=100, blank=False, null=False, default=""
    )  # value behalf of which the result of function will be stored in context dict
    function_name = models.CharField(
        max_length=100, blank=False, null=False, default=""
    )


class UserSession(models.Model):
    """Model to keep track sessions tied to user to be able to clean User and Session model instances from unused or old records"""

    id = models.CharField(max_length=10, primary_key=True, default=None)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    session = models.ForeignKey(Session, on_delete=models.CASCADE)

    def save(self, *args, **kwargs):
        if not self.id:
            while True:
                try:
                    self.id = self.generate_random_key()
                    break
                except IntegrityError:
                    pass
        super().save(*args, **kwargs)

    @staticmethod
    def generate_random_key():
        """Generate a random alphanumeric key of length 10."""
        characters = string.ascii_letters + string.digits
        return "".join(random.choice(characters) for _ in range(10))
