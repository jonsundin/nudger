from django.urls import re_path

from . import consumers

websocket_urlpatterns = [
    re_path(
        r"ws/quiz/(?P<username>[A-Za-z0-9]+)/(?P<convo_id>[A-Za-z0-9]+)/(?P<room_name>[A-z0-9]+)/$",
        consumers.QuizRoomConsumer.as_asgi(),
    ),
]
