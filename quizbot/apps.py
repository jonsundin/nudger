from django.apps import AppConfig
from django.db.models.signals import post_save


class QuizbotConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "quizbot"

    def ready(self):
        from .signals import delete_expired_sessions_and_relations

        post_save.connect(delete_expired_sessions_and_relations)
