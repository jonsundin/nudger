class ConvoSpreadsheetValidationError(Exception):
    pass


class ConvoSpreadsheetValidator:
    def __init__(self, df):
        self.df = df

    def has_required_columns(self):
        # Check for required column
        required_columns = [
            "Type",
            "Bot says",
            "User says",
            "Go to Row",
            "Go to Row Number",
        ]
        missing_columns = set(required_columns) - set(self.df.columns)

        if len(missing_columns) > 0:
            return ConvoSpreadsheetValidationError(
                f"Missing the following required columns: {', '. join(missing_columns)}"
            )
        return True

    def has_row_type_for_each_row(self, index, row):
        if row["Bot says"] or row["User says"]:
            if not row["Type"]:
                return ConvoSpreadsheetValidationError(
                    f"Row {index + 2} seems to be missing a value in the 'Type' column"
                )
        return True

    def has_bot_says_for_each_text_type_row(self, index, row):
        if row["Type"] == "Text":
            if not row["Bot says"]:
                return ConvoSpreadsheetValidationError(
                    f"Row {index + 2} is a 'Text' message but it is missing the bot's text"
                )
        return True

    def has_user_says_for_each_button_type_row(self, index, row):
        if row["Type"] == "Button":
            if not row["User says"]:
                return ConvoSpreadsheetValidationError(
                    f"Row {index + 2} is a 'Button' message but it is missing a button option in User says"
                )
        return True

    def has_at_least_one_button_for_button_prompt(self, index, row):
        if row["Type"] == "Button Prompt":
            next_row = index + 1
            if not self.df.loc[next_row, "Type"] == "Button":
                return ConvoSpreadsheetValidationError(
                    f"Row {next_row + 2} should be a Button for the Button Prompt in Row {index + 2}"
                )
        return True

    def has_button_prompt_or_button_before_button(self, index, row):
        if row["Type"] == "Button":
            prev_row = index - 1
            if not (
                self.df.loc[prev_row, "Type"] == "Button"
                or self.df.loc[prev_row, "Type"] == "Button Prompt"
            ):
                return ConvoSpreadsheetValidationError(
                    f"Row {index + 1} should have a Button or Button Prompt because Row {index + 2} is a Button."
                )
        return True

    def has_go_to_row_number_for_necessary_types(self, index, row):
        if row["Type"] in ["Text", "Button"]:
            if not row["Go to Row Number"] or row["Go to Row Number"] == 0:
                next_row = index + 1
                if self.df.loc[next_row, "Type"]:
                    return ConvoSpreadsheetValidationError(
                        f"Row {index + 2} needs a Go to Row Number."
                    )
        return True

    def check_spreadsheet_characteristics(self, checks_to_pass):
        for check in checks_to_pass:
            invoked_check = check()
            if not isinstance(invoked_check, bool):
                raise invoked_check

    def check_row_characteristics(self, checks_to_pass, index, row):
        for check in checks_to_pass:
            invoked_check = check(index, row)
            if not isinstance(invoked_check, bool):
                raise invoked_check

    def run(self):
        whole_spreadsheet_checks_to_pass = [
            self.has_required_columns,
        ]

        self.check_spreadsheet_characteristics(whole_spreadsheet_checks_to_pass)

        row_checks_to_pass = [
            self.has_row_type_for_each_row,
            self.has_bot_says_for_each_text_type_row,
            self.has_user_says_for_each_button_type_row,
            self.has_at_least_one_button_for_button_prompt,
            self.has_button_prompt_or_button_before_button,
            self.has_go_to_row_number_for_necessary_types,
        ]
        for index, row in self.df.iterrows():
            self.check_row_characteristics(row_checks_to_pass, index, row)

        return ""
