import markdown

from quizbot.models import Message, Trigger


class MessageConverter:
    @staticmethod
    def messages_to_list_of_dicts(state, lang):
        msgs = []
        for m in Message.objects.filter(state=state, lang=lang):
            msgs.append(
                {
                    "sequence_number": m.sequence_number,
                    "bot_text": markdown.markdown(m.bot_text, extensions=["attr_list"]),
                }
            )
        return msgs


class TriggerConverted:
    @staticmethod
    def triggers_to_list_of_dicts(from_state, lang, **kwargs):
        """Converts Trigger QuerySet to list of dictionaries

        :params:
            kwargs: a dict with OPTIONAL additional conditions to query Trigger.
        """
        kwargs["from_state"] = from_state
        kwargs["lang"] = lang
        trigs = []

        for is_btn in (kwargs["is_button"]) if "is_button" in kwargs else (True, False):
            kwargs["is_button"] = is_btn
            for t in Trigger.objects.filter(**kwargs):
                trigs.append(t.trigger_to_dict())
        return trigs
