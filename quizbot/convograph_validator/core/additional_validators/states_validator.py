from nudger.constants import (
    START_STATE_NAME,
    STOP_STATE_NAME,
    YAML_FORMAT_VERSION_TO_ASSETS_MAPPING,
)
from ...convograph_validator_errors import ConvoGraphValidationError
from ...helpers import is_state_name_valid


class StateValidator:
    def __init__(self, convograph):
        self._convograph = convograph
        self._format_version = "default"
        self._required_attrs = None
        self._allowed_attrs = None
        self.set_format_dependent_assets()

    def set_required_attrs(self):
        self

    def set_format_dependent_assets(self):
        format_version_assets = YAML_FORMAT_VERSION_TO_ASSETS_MAPPING.get(
            self._format_version
        )
        self._required_attrs = format_version_assets["required_state_attrs"]
        self._allowed_attrs = format_version_assets["allowed_state_attrs"]
        return

    def states_have_required_attrs_or_error(self):
        for state_data in self._convograph:
            if "name" in state_data and state_data["name"] in ("start", "stop"):
                continue
            for attr in self._required_attrs:
                if attr not in state_data:
                    raise ConvoGraphValidationError(
                        f'"{state_data["name"]}" state doesn\'t have "{attr}" attribute.'
                    )
        return

    def required_states_exist_or_error(self):
        state_names = (s["name"] for s in self._convograph)
        required_states = ("start", "stop")
        for s in required_states:
            if s not in state_names:
                raise ConvoGraphValidationError(
                    f'No "{s}" state was found. It is recognized as one having "name" attribute assigned a "{s}" value.'
                )
        return True

    def state_names_valid_or_error(self):
        for state_data in self._convograph:
            state_name = state_data["name"]
            if not is_state_name_valid(state_name):
                raise ConvoGraphValidationError(
                    f'"{state_name}" isn\'t valid name for state. It may contain alphanumerical, "_", and/or "/" characters.'
                )
        return True

    def state_names_unique_or_error(self):
        state_names = set()
        for state_data in self._convograph:
            state_name = state_data["name"]
            if state_name not in state_names:
                state_names.add(state_name)
            else:
                raise ConvoGraphValidationError(
                    f'"{state_name}" state occurs more than once.'
                )
        return True

    def state_attrs_allowed_or_error(self):
        for state_data in self._convograph:
            state_name = state_data["name"]
            if state_name in ("start", "stop"):
                continue
            for attr in state_data:
                if attr not in self._allowed_attrs:
                    raise ConvoGraphValidationError(
                        f'Unknown attribute "{attr}" of "{state_name}" state detected.'
                    )
        return True

    def run(self):
        validations = [
            self.states_have_required_attrs_or_error,
            self.required_states_exist_or_error,
            self.state_names_valid_or_error,
            self.state_names_unique_or_error,
            self.state_attrs_allowed_or_error,
        ]
        for v in validations:
            v()
        return


class StartStopStatesBaseValidator:
    def __init__(self, convograph, state_name, allowed_attrs):
        self._convograph = convograph
        self._state_data = self.get_state_by_name(state_name)
        self._allowed_attrs = self._required_attrs = allowed_attrs

    def get_state_by_name(self, state_name):
        for state_data in self._convograph:
            if state_data["name"] == state_name:
                return state_data
        return

    def state_attrs_allowed_or_error(self):
        for attr in self._state_data:
            if attr not in self._allowed_attrs:
                state_name = self._state_data["name"]
                raise ConvoGraphValidationError(
                    f'Unknown or forbidden attribute "{attr}" of "{state_name}" state detected.'
                )
        return

    def get_first_trigger_data(self):
        try:
            return self._state_data["triggers"][0]
        except IndexError:
            return {}


class StartStateValidator(StartStopStatesBaseValidator):
    nlp_algos = ("exact", "keyword", "re_match", "re_search", "spacy")

    def __init__(self, convograph, allowed_attrs):
        super().__init__(convograph, START_STATE_NAME, allowed_attrs)

    def start_state_has_required_attrs_or_error(self):
        for attr in self._required_attrs:
            if attr not in self._state_data:
                raise ConvoGraphValidationError(
                    f'"start" state doesn\'t have "{attr}" attribute.'
                )
        return

    def convo_name_is_valid_or_error(self):
        if not is_state_name_valid(self._state_data["convo_name"]):
            raise ConvoGraphValidationError(
                f'"{self._state_data["convo_name"]}" isn\'t valid value for "convo_name" attribute for "start" state. It may contain alphanumerical, "_", and/or "/" characters.'
            )
        return

    def nlp_algo_is_recognized_or_error(self):
        if self._state_data["nlp"] not in self.nlp_algos:
            raise ConvoGraphValidationError(
                f'Unknown nlp algorithm "{self._state_data["nlp"]}" specified for "start" state. Possible values are {str([algo for algo in self.nlp_algos])[1:-1]}.'
            )
        return

    def start_state_has_only_next_trigger_or_error(self):
        raise NotImplementedError(
            '"start_state_has_only_next_trigger_or_error" method of core "StartStateValidator" class isn\'t implemented.'
        )

    def run(self):
        validations = [
            self.start_state_has_required_attrs_or_error,
            self.state_attrs_allowed_or_error,
            self.convo_name_is_valid_or_error,
            self.start_state_has_only_next_trigger_or_error,
        ]
        for v in validations:
            v()
        return


class StopStateValidator(StartStopStatesBaseValidator):
    _allowed_attrs = ("name", "triggers")

    def __init__(self, convograph):
        super().__init__(convograph, STOP_STATE_NAME, self._allowed_attrs)

    def stop_state_has_required_attrs_or_error(self):
        for attr in self._required_attrs:
            if attr not in self._state_data:
                raise ConvoGraphValidationError(
                    f'"stop" state doesn\'t have "{attr}" attribute.'
                )
        return

    def stop_state_has_only_default_trigger_or_error(self):
        raise NotImplementedError(
            '"stop_state_has_only_default_trigger_or_error" method of core "StopStateValidator" isn\'t implemented.'
        )

    def run(self):
        validations = [
            self.stop_state_has_required_attrs_or_error,
            self.state_attrs_allowed_or_error,
            self.stop_state_has_only_default_trigger_or_error,
        ]
        for v in validations:
            v()
        return
