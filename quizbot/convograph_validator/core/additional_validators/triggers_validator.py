class TriggerValidator:
    def __init__(self, convograph):
        self._convograph = convograph
        self._states_names = self.get_all_states_names()

    def get_all_states_names(self):
        states = set()
        for s in self._convograph:
            states.add(s["name"])
        return states

    def triggers_valid_or_error(self):
        raise NotImplementedError(
            '"triggers_valid_or_error" method of core "TriggerValidator" class isn\'t implemented'
        )

    def triggers_have_essential_or_error(self):
        raise NotImplementedError(
            '"triggers_have_essential_or_error" method of core "TriggerValidator" class isn\'t implemented'
        )

    def states_comply_with_next_trigger_or_error(self):
        raise NotImplementedError(
            '"states_comply_with_next_trigger_or_error" method of core "TriggerValidator" class isn\'t implemented'
        )

    def run(self):
        validations = [
            self.triggers_valid_or_error,
            self.triggers_have_essential_or_error,
            self.states_comply_with_next_trigger_or_error,
        ]
        for v in validations:
            v()
        return
