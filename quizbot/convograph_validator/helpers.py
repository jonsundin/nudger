import re


def is_state_name_valid(state_name):
    return bool(re.match("[a-zA-Z0-9_/.]+", state_name))
