from ..core.convograph_validator import ConvoGraphValidator as CoreConvoGraphValidator
from .additional_validators.actions_validator import ActionValidator
from .additional_validators.states_validator import (
    StateValidator,
    StartStateValidator,
    StopStateValidator,
)
from .additional_validators.triggers_validator import TriggerValidator


class ConvoGraphValidator(CoreConvoGraphValidator):
    def __init__(self, yaml_to_python):
        """
        :params:
            yaml_to_python: python representation of yaml file content (can be obtained with `yaml.safe_load()` func)
        """
        super().__init__(yaml_to_python)
        self.include_additional_validations()

    def include_additional_validations(self):
        self._additional_validations = [
            ActionValidator(self._convograph),
            StateValidator(self._convograph),
            StartStateValidator(self._convograph),
            StopStateValidator(self._convograph),
            TriggerValidator(self._convograph),
        ]
        return
