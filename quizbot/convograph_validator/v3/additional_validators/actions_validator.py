from quizbot.actions_system.action_runners import is_allowed as is_action_allowed
from ...convograph_validator_errors import ConvoGraphValidationError
from ...core.additional_validators.actions_validator import (
    ActionValidator as CoreActionValidator,
)


class ActionValidator(CoreActionValidator):
    def actions_valid_or_error(self):
        for state_data in self._convograph:
            if state_data["name"] in ("start", "stop"):
                continue
            self.state_actions_is_list_or_error(state_data)
            self.state_actions_valid_or_error(state_data)
        return

    def state_actions_is_list_or_error(self, state_data):
        if not isinstance(state_data["actions"], list):
            raise ConvoGraphValidationError(
                f'"{state_data["name"]}" state has wrong value specified for "actions". It must be of `list` type.'
            )
        return

    def state_actions_valid_or_error(self, state_data):
        for action_data in state_data["actions"]:
            self._state_name = state_data["name"]
            self._action_data = action_data
            self.action_data_is_valid_or_error()
        return

    def action_data_is_valid_or_error(self):
        self.action_data_is_dict_or_error()
        self.action_data_has_single_action_or_error()
        self.action_name_is_str_or_error()
        self.action_is_allowed_or_error()
        self.action_data_has_nested_action_data_or_error()
        return

    def action_data_is_dict_or_error(self):
        if not isinstance(self._action_data, dict):
            raise ConvoGraphValidationError(
                f'Some action inside "{self._state_name}" state isn\'t a `dict` type.'
            )
        return

    def action_data_has_single_action_or_error(self):
        if len(self._action_data.keys()) != 1:
            raise ConvoGraphValidationError(
                f'Action "{self._action_data}" inside "{self._state_name}" state doesn\'t have or has more than one key which is forbidden.'
            )
        return

    def action_name_is_str_or_error(self):
        action_name = list(self._action_data.keys())[0]
        if not (isinstance(action_name, str)):
            raise ConvoGraphValidationError(
                f'Action name inside "{self._action_data}" action data of "{self._state_name}" state must be string.'
            )
        return

    def action_is_allowed_or_error(self):
        action_name = list(self._action_data.keys())[0]
        if not is_action_allowed(action_name):
            raise ConvoGraphValidationError(
                f'Action "{action_name}" inside "{self._state_name}" state isn\'t recognized or allowed.'
            )
        return

    def action_data_has_nested_action_data_or_error(self):
        action_name = list(self._action_data.keys())[0]
        nested_action_data = list(self._action_data.values())[0]
        if not isinstance(nested_action_data, dict):
            raise ConvoGraphValidationError(
                f'Value of "{action_name}" action inside "{self._action_data}" action data of "{self._state_name}" state must be `dict`.'
            )
        return
