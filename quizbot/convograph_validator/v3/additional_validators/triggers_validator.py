from ...convograph_validator_errors import ConvoGraphValidationError
from ...core.additional_validators.triggers_validator import (
    TriggerValidator as CoreTriggerValidator,
)


class TriggerValidator(CoreTriggerValidator):
    def __init__(self, convograph):
        super().__init__(convograph)
        self.state_name = None
        self.trigger_data = None
        self.required_trigger_data_keys = ("user_text", "target")

    def triggers_valid_or_error(self):
        for state_data in self._convograph:
            if state_data["name"] in ("start", "stop"):
                continue
            self.state_triggers_is_list_or_error(state_data)
            self.state_triggers_valid_or_error(state_data)
            self.triggers_have_essential_or_error()
        return

    def state_triggers_is_list_or_error(self, state_data):
        state_triggers = state_data["triggers"]
        if not isinstance(state_triggers, list):
            raise ConvoGraphValidationError(
                f'"{state_data["name"]}" state has wrong value specified for "triggers". It must be of `list` type.'
            )
        return

    def state_triggers_valid_or_error(self, state):
        for trigger_data in state["triggers"]:
            self.trigger_data = trigger_data
            self.state_name = state["name"]
            self.trigger_data_is_valid_or_error()
        return

    def trigger_data_is_valid_or_error(self):
        self.trigger_data_is_dict_or_error()
        self.trigger_data_has_required_keys()
        self.trigger_points_on_existing_state_or_error()
        return

    def trigger_data_is_dict_or_error(self):
        if not isinstance(self.trigger_data, dict):
            raise ConvoGraphValidationError(
                f'Some trigger data inside "{self.state_name}" state isn\'t a `dict` type.'
            )
        return

    def trigger_data_has_required_keys(self):
        for key in self.required_trigger_data_keys:
            if key not in self.trigger_data:
                raise ConvoGraphValidationError(
                    f'"{self.trigger_data}" trigger data doesn\'t have required "{key}" key.'
                )
        return

    def trigger_points_on_existing_state_or_error(self):
        target_state_name = self.trigger_data["target"]
        if target_state_name not in self._states_names:
            raise ConvoGraphValidationError(
                f'"{self.trigger_data}" trigger of "{self.state_name}" state points on unexisting state.'
            )
        return

    def triggers_have_essential_or_error(self):
        for state_data in self._convograph:
            if state_data["name"] in ("start", "stop"):
                continue
            self.next_and_default_not_used_together_or_error(state_data)
        return

    def next_and_default_not_used_together_or_error(self, state_data):
        triggers = self.get_triggers_texts(state_data)
        forbidden_triggers_combination = set(("__default__", "__next__"))
        if forbidden_triggers_combination <= triggers:
            return ConvoGraphValidationError(
                f'"__default__" and "__next__" trigger can\'t be used together in "{state_data["name"]}" state.'
            )
        elif "__default__" not in triggers and "__next__" not in triggers:
            return ConvoGraphValidationError(
                f'Neither "__default__ " nor "__next__" trigger was found inside triggers of "{state_data["name"]}" state.'
            )

    def get_triggers_texts(self, state_data):
        triggers = set()
        for trig_data in state_data["triggers"]:
            trig_text = trig_data["user_text"]
            triggers.add(trig_text)
        return triggers

    def states_comply_with_next_trigger_or_error(self):
        for state_data in self._convograph:
            if state_data["name"] in ("start", "stop"):
                continue
            triggers_names = set()
            for trig_data in state_data["triggers"]:
                trig_text = trig_data["user_text"]
                triggers_names.add(trig_text)
            if "__next__" in triggers_names:
                if len(triggers_names) > 1:
                    return ConvoGraphValidationError(
                        f'No triggers can be used inside triggers of "{state_data["name"]}" state along with "__next__" trigger.'
                    )
        return True

    def run(self):
        validations = [
            self.triggers_valid_or_error,
            self.triggers_have_essential_or_error,
            self.states_comply_with_next_trigger_or_error,
        ]
        for v in validations:
            v()
        return
