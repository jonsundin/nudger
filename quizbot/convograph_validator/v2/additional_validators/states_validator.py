from ...convograph_validator_errors import ConvoGraphValidationError
from ...core.additional_validators.states_validator import (
    StateValidator as CoreStateValidator,
    StartStateValidator as CoreStartStateValidator,
    StopStateValidator as CoreStopStateValidator,
)


class StateValidator(CoreStateValidator):
    pass


class StartStateValidator(CoreStartStateValidator):
    _allowed_attrs = (
        "name",
        "convo_name",
        "nlp",
        "triggers",
        "convo_description",
        "version",
    )

    def __init__(self, convograph):
        super().__init__(convograph, self._allowed_attrs)

    def start_state_has_only_next_trigger_or_error(self):
        state_triggers = self._state_data["triggers"]
        for lang, lang_trigs in state_triggers.items():
            if set(["__next__"]) != set(lang_trigs):
                raise ConvoGraphValidationError(
                    f'"{lang}" triggers of "start" state must have only "__next__" trigger specified.'
                )
        return


class StopStateValidator(CoreStopStateValidator):
    def stop_state_has_only_default_trigger_or_error(self):
        state_triggers = self._state_data["triggers"]
        expected_data = {"__default__": "start"}
        for lang, trig_data in state_triggers.items():
            if trig_data != expected_data:
                raise ConvoGraphValidationError(
                    f'"{lang}" triggers of "stop" state must have only "__default__" trigger specified pointing on "start" state.'
                )
