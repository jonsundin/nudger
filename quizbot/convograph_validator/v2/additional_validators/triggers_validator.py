from ...convograph_validator_errors import ConvoGraphValidationError
from ...core.additional_validators.triggers_validator import (
    TriggerValidator as CoreTriggerValidator,
)


class TriggerValidator(CoreTriggerValidator):
    def triggers_valid_or_error(self):
        for state in self._convograph:
            if state["name"] in ("start", "stop"):
                continue
            if not isinstance(state["triggers"], dict):
                raise ConvoGraphValidationError(
                    f'"{state["name"]}" state has wrong value specified for "tiggers". It must be of `dict` type.'
                )
            for lang, lang_trigs in state["triggers"].items():
                if not isinstance(lang_trigs, dict):
                    raise ConvoGraphValidationError(
                        f'"{state["name"]}" state has wrong value specified for "{lang}" triggers. It must be of `dict` type.'
                    )
                if not len(lang_trigs):
                    raise ConvoGraphValidationError(
                        f'"{lang}" triggers are empty for "{state["name"]}" state.'
                    )
            trigger_data = state["triggers"]
            self.trigger_data_is_valid_or_error(trigger_data)
        return True

    def trigger_data_is_valid_or_error(self):
        self.trigger_data_is_dict_or_error()
        self.trigger_data_has_required_keys()
        self.trigger_points_on_existing_state_or_error()
        return

    def trigger_points_on_existing_state_or_error(self):
        for state in self._convograph:
            if state["name"] in ("start", "stop"):
                continue
            self.state_triggers_is_list_or_error(state)
            self.state_triggers_valid_or_error(state)
        return

    def buttons_valid_or_error(self):
        for state in self._convograph:
            if state["name"] in ("start", "stop") or "buttons" not in state:
                continue
            if not isinstance(state["buttons"], dict):
                raise ConvoGraphValidationError(
                    f'"{state["name"]}" state has wrong value specified for "buttons". It must be of `dict` type.'
                )
            for lang, lang_btns in state["buttons"].items():
                if not isinstance(lang_btns, dict):
                    raise ConvoGraphValidationError(
                        f'"{state["name"]}" state has wrong value specified for "{lang}" buttons. It must be of `list` type.'
                    )
                if not len(lang_btns):
                    raise ConvoGraphValidationError(
                        f'"{lang}" buttons are empty for "{state["name"]}" state.'
                    )
        return True

    def triggers_have_essential_or_error(self):
        for state in self._convograph:
            if state["name"] in ("start", "stop"):
                continue
            for lang, lang_trigs in state["triggers"].items():
                if set(("__default__", "__next__")) <= set(lang):
                    raise ConvoGraphValidationError(
                        f'Neither "__default__ " nor "__next__" trigger was found in "{lang}" language triggers of "{state["name"]}" state.'
                    )
                elif "__default__" not in lang_trigs and "__next__" not in lang_trigs:
                    raise ConvoGraphValidationError(
                        f'There mustn\'t be "__default__" and "__next__" triggers used together. Please, fix this inside "{lang}" triggers of "{state["name"]}" state.'
                    )
        return True

    def states_comply_with_next_trigger_or_error(self):
        for state in self._convograph:
            for lang, lang_trigs in state["triggers"].items():
                if "__next__" in lang_trigs:
                    if len(lang_trigs) > 1:
                        raise ConvoGraphValidationError(
                            f'No triggers can be used in "{lang}" language triggers of "{state["name"]}" state along with "__next__" trigger.'
                        )
                    if (
                        len(state["triggers"]) == 1
                        and "buttons" in state
                        and lang in state["buttons"]
                        and state["buttons"][lang]
                    ):
                        raise ConvoGraphValidationError(
                            f'It\'s meaningless to put "{lang}" language buttons for "{state["name"]}" state because "{lang}" language triggers have "__next__" trigger.'
                        )
        return True

    def run(self):
        validations = [
            self.triggers_valid_or_error,
            self.trigger_points_on_existing_state_or_error,
            self.buttons_valid_or_error,
            self.triggers_have_essential_or_error,
            self.states_comply_with_next_trigger_or_error,
        ]
        for v in validations:
            v()
        return
