from django.urls import path

from quizbot import views

urlpatterns = [
    # sequence is important!
    path("", views.quiz, name="quiz"),
    path("bothub", views.bothub, name="bothub"),
    path("dashboard", views.dashboard, name="dashboard"),
    path("upload/", views.upload, name="quizbot-upload"),
    path("spreadsheet-upload/", views.csv_upload, name="spreadsheet-upload"),
    path("bot_widget_icon", views.get_bot_widget_icon, name="bot-widget-icon"),
    path(
        "bothub/<str:convo_id>",
        views.handle_public_convo,
        name="public-convo",
    ),
    path("<str:room_name>/", views.room, name="quizbot-room"),  # Keep this at the end.
]
