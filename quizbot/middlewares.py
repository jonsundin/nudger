import random
import string

from django.contrib.auth import login
from django.contrib.auth.models import AnonymousUser, User
from django.contrib.sessions.models import Session
from django.db import IntegrityError

from .models import UserSession


def generate_random_alphanumeric(length):
    characters = string.ascii_letters + string.digits
    random_alphanumeric = "".join(random.choice(characters) for _ in range(length))
    return random_alphanumeric


def create_random_user():
    while True:
        try:
            new_user = User.objects.create(username=generate_random_alphanumeric(10))
            break
        except IntegrityError:
            pass
    return new_user


def populate_session_middleware(get_response):
    def middleware(request):
        if not request.session.get("user_id", False):
            request_user = request.user
            user = (
                create_random_user()
                if isinstance(request_user, AnonymousUser)
                else request_user
            )
            login(request, user)
            request.user = user
            request.session[
                "user_id"
            ] = (
                user.id
            )  # will create or update django.contrib.sessions.models.Session object
            session = Session.objects.get(session_key=request.session.session_key)
            UserSession.objects.create(user=user, session=session)

        response = get_response(request)

        return response

    return middleware
