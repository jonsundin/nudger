import numpy as np
import pandas as pd
import re

from django.contrib.auth.models import User
from quizbot.load_yaml import (
    create_states,
    create_messages,
    create_triggers,
    create_functions,
)
from quizbot.models import Convo
from quizbot.spreadsheet_linter import ConvoSpreadsheetValidator


def create_new_convo(user, file, convo_name, convo_description):
    convos = [
        obj
        for obj in Convo.objects.filter(user=user)
        if convo_name in obj.name and re.match(r"\d+$", obj.name.split("_")[-1])
    ]
    max_version = max([int(obj.name.split("_")[-1]) for obj in convos] or [0])
    new_convo_name = (
        f"{convo_name}_{max_version + 1}"
        if max_version or len(Convo.objects.filter(user=user, name=convo_name))
        else convo_name
    )
    new_convo = Convo.objects.create(
        user=user,
        file=file,
        name=new_convo_name,
        description=convo_description,
    )
    return new_convo


def read_spreadsheet_to_df(file_source, source_type):
    """Reads a CSV, Excel, or Google Sheet into a dataframe"""
    df = None
    if source_type == "csv":
        df = pd.read_csv(file_source)
    elif source_type == "xlsx":
        df = pd.read_excel(
            file_source,
            engine="openpyxl",
            header=0,
        )
    return df


def normalize_df(script_df):
    """Replaces all empty values with 0 and ensures numbers are in integer format"""
    script_df = script_df.fillna(0)
    script_df = script_df.replace("", 0)
    script_df["Go to Row Number"] = script_df["Go to Row Number"].apply(np.int64)
    return script_df


def create_start_state():
    """Sets the default start state for the conversation graph"""
    start_state = {
        "name": "start",
        "convo_name": "default",
        "convo_description": "default convo",
        "nlp": "re_search",
        "triggers": {"en": {"__next__": f"row3"}},
    }
    return start_state


def set_current_and_next_state_names(index, row):
    current_state = f"row{index+2}"
    next_state = f"row{row['Go to Row Number']}"
    if next_state == "row0":
        next_state = "stop"
    return current_state, next_state


def build_triggers_object(script_df, index, row, next_state):
    """Builds the predefined user inputs available for each state"""
    triggers = {"en": {"__next__": next_state}}
    if row["Type"] == "Button Prompt":
        row_check = True
        next_row = index + 1
        trigger_options = {}
        while row_check:
            if script_df.loc[next_row, "Type"] == "Button":
                trigger_text = script_df.loc[next_row, "Go to Row"]
                next_state = f'row{script_df.loc[next_row, "Go to Row Number"]}'
                trigger_option = {
                    trigger_text: next_state,
                }
                trigger_options.update(trigger_option)
                next_row += 1
            else:
                row_check = False
        triggers = {"en": trigger_options}
    return triggers


def process_script_df_rows(script_df):
    full_dialog = {}

    full_dialog["start"] = create_start_state()

    for index, row in script_df.iterrows():
        if row["Type"] in ["Text", "Button Prompt"]:
            current_state, next_state = set_current_and_next_state_names(index, row)
            actions = {"en": [row["Bot says"]]}
            triggers = build_triggers_object(script_df, index, row, next_state)

            entry = {
                "state_name": current_state,
                "actions": actions,
                "triggers": triggers,
            }
            full_dialog[current_state] = entry

    full_dialog["stop"] = {"name": "stop", "triggers": {"en": {"__default__": "start"}}}

    return full_dialog


def convert_spreadsheet_to_dictionary(file_source, file_type):
    """Converts each row of the spreadsheet to state object in a dialog dictionary"""
    df = read_spreadsheet_to_df(file_source, file_type)
    df = normalize_df(df)

    ConvoSpreadsheetValidator(df).run()

    return process_script_df_rows(df)


def upload_spreadsheet_to_db(
    user, file_source, file_type, convo_name, convo_description
):
    dialog = convert_spreadsheet_to_dictionary(file_source, file_type)

    user = User.objects.filter(username=user).first()
    new_convo = create_new_convo(user, file_source, convo_name, convo_description)
    create_states(dialog, new_convo)
    create_messages(dialog, new_convo)
    create_triggers(dialog, new_convo)
    create_functions(dialog, new_convo)
