"""
Communicate the bot state or conversation session (called `context`) between the frontend chat widget and the backend.
This AsyncWebsocketConsumer does not send directly to the frontend, but rather sends the context object to Celery.
scheduling the context to be sent to the frontend.

### Context

The `context` object sent to Celery contains information that the bot knows about the user and the conversation plan:
This includes:

- Convo plan for the current node (state) in the convograph:
  - **state_name**: The `models.Convo.state.name` str
  - **bot_messages**: list of dicts with the HTML text messages to be sent to the user 
  - **triggers**: `models.Convo.state.triggers.values` list of dicts
  - **convo_id**: `models.Convo.id` int
- Recent text messages from the user or the bot:
- The context may also contain additional info about the history of the conversation:
  - **user_text**: last text message from the user that triggered the transition to this bot state
  - **message_history**: list of dicts containing the text from the user and bot message bubbles displayed in the widget
- User info:
  - **lang**: The currently selected language ISO code from `constants.LANGUAGES`, e.g. "en" or "ua"

The context object sent from the frontend and received by this `AsyncWebsocketConsumer`
echos the context sent from the backend after being updated with new user actions:

- **user_text**: The user's response to the current bot messages.
- **lang**: Any updates to the user's prefered language setting (2-character ISO code)

The `context` object is used in a query to the database to find the next `Convo.state`.
object to switch to for the next dialog turn.
The `context` for this conversation session is then updated with the new `Convo.state` information.
And the updated `context` dict is scheduled for delivery by Celery task which will continue the conversation.

Example context object sent to Celery for a send_message task

```json
{
    "state_name": "q2",
    "bot_messages": [
        {"sequence_number": 0, "bot_text": "<p>Exactly right!</p>"},
        {"sequence_number": 0, "bot_text": "<p>16, 17, 18</p>"},
    ],
    "triggers": [
        {
            '"from_state"': "q2",
            '"to_state"': "q2",
            '"intent_text"': '"OK"',
            '"update_args"': "null",
            '"update_kwargs"': "null",
            '"lang"': '"en"',
            '"is_button"': "true",
        },
        {
            '"from_state"': "q2",
            '"to_state"': "correct-answer-q2",
            '"intent_text"': '"19"',
            '"update_args"': "null",
            '"update_kwargs"': "null",
            '"lang"': '"en"',
            '"is_button"': "false",
        },
        {
            '"from_state"': "q2",
            '"to_state"': "wrong-answer-q2",
            '"intent_text"': '"__default__"',
            '"update_args"': "null",
            '"update_kwargs"': "null",
            '"lang"': '"en"',
            '"is_button"': "false",
        },
    ],
    "lang": "en",
    "convo_id": "8",
    "functions_execution_result": {},
}
```

Example context object sent to the backend front frontend

```json
{
    "state_name": "q1",
    "bot_messages": [
        {
            "sequence_number": 0,
            "bot_text": "<p>Let's start by counting up by 1s....Now you try 😄</p>",
        },
        {"sequence_number": 1, "bot_text": "<p>8, 9, 10</p>"},
    ],
    "triggers": [
        {
            '"from_state"': "q1",
            '"to_state"': "q1",
            '"intent_text"': '"OK"',
            '"update_args"': "null",
            '"update_kwargs"': "null",
            '"lang"': '"en"',
            '"is_button"': "true",
        },
        {
            '"from_state"': "q1",
            '"to_state"': "correct-answer-q1",
            '"intent_text"': '"11"',
            '"update_args"': "null",
            '"update_kwargs"': "null",
            '"lang"': '"en"',
            '"is_button"': "false",
        },
        {
            '"from_state"': "q1",
            '"to_state"': "correct-answer-q1",
            '"intent_text"': '"eleven"',
            '"update_args"': "null",
            '"update_kwargs"': "null",
            '"lang"': '"en"',
            '"is_button"': "false",
        },
        {
            '"from_state"': "q1",
            '"to_state"': "wrong-answer-q1",
            '"intent_text"': '"__default__"',
            '"update_args"': "null",
            '"update_kwargs"': "null",
            '"lang"': '"en"',
            '"is_button"': "false",
        },
    ],
    "lang": "en",
    "convo_id": "8",
    "functions_execution_result": {},
    "user_text": "11"
}
```
"""
import json
import logging

from channels.db import database_sync_to_async
from channels.generic.websocket import AsyncWebsocketConsumer

from quizbot.bot_manager import StateParser
from quizbot.tasks import schedule_sending_chat_context

from nudger.constants import (
    RESPONSE_DELAY_PER_CHAR,
    RESPONSE_DELAY_MIN,
    START_STATE_NAME,
)

log = logging.getLogger(__name__)


async def destructure_context_into_messages(bot_messages, context):
    for i in range(len(bot_messages)):
        bot_messages[i] = bot_messages[i].format(**context)

    return bot_messages


class QuizRoomConsumer(AsyncWebsocketConsumer):
    """ASGI web socket consumer for url ws/quiz/.

    TODO: simplify & refactor this class to look like
       - https://stackoverflow.com/a/68850387/623735
    """

    def __init__(self, **kwargs):
        """FIXME: Why is this necessary? And why is it synchronous instead of async?"""
        log.info(f"QuizRoomConsumer.__init__({kwargs})")
        super(QuizRoomConsumer, self).__init__(**kwargs)
        self.room_group_name = None
        # self.convo_id = ?

    @database_sync_to_async
    def fill_context_with_next_bot_state(self, convo_id, data):
        """TBD: wrapper to retrieve next_state using only the info found in context

        Context should already include `(convo_id, state_name, lang, user_text)` keys
        """
        state_name = data["state_name"]
        try:
            lang = data["context"]["lang"]
        except KeyError:
            lang = "en"
        user_text = data["user_text"]
        context = StateParser(
            {
                "convo_id": convo_id,
                "state_name": state_name,
                "lang": lang,
                "user_text": user_text,
            }
        ).fill_context_with_next_state()
        return context

    async def connect(self):
        """Connect to a chat room and assign a user a group.

        This method is called the first time a user clicks on the faceofqary widget or
        any client attempts to connect to the websocket and start a new session/convo.
        """
        log.info(f"QuizRoomConsumer.connect()")
        room_name = self.scope["url_route"]["kwargs"]["room_name"]
        log.warning(
            f'self.scope["url_route"]["kwargs"]: {self.scope["url_route"]["kwargs"]}'
        )
        self.room_group_name = f"chat_{room_name}"
        log.warning(f"self.room_group_name: {self.room_group_name}")
        self.convo_id = self.scope["url_route"]["kwargs"]["convo_id"]
        log.warning(f"self.convo_id: {self.convo_id}")

        # Join room group
        await self.channel_layer.group_add(self.room_group_name, self.channel_name)

        # Accept the connection
        await self.accept()

        # Sends the first message to the room
        initial_context = {
            "state_name": START_STATE_NAME,
            "user_text": "",
            "lang": "en",
        }
        log.warning(
            f"QuizRoomConsumer.fill_context_with_next_bot_state(convo_id={self.convo_id}, data={initial_context})"
        )
        context = await self.fill_context_with_next_bot_state(
            self.convo_id, initial_context
        )
        log.warning(
            f"QuizRoomConsumer.send_context_to_celery_task(state_data={context})"
        )
        await self.send_context_to_celery_task(context)
        return self

    async def send_context_to_celery_task(self, context):
        """Receives full state data object, and schedules a celery worker to send out each part of the state

        Called within self.receive() and self.connect()  (Perhaps receive should be called within connect?)

        Send 4 things:
          - room_group_name str (channels)
          - bot message str
          - delay in seconds
          - dictionary of interaction data

        TODO: find out if this can sometime contain a user_message in addition to a bot_message
        """
        log.info(f"{__name__}.QuizRoomConsumer.send_context_to_celery_task({context})")
        # Schedule_chat_message creates a task in celery_beat
        log.warning(f"context: {context}")

        if "bot_messages" not in context:
            log.warning(f"Unable to find bot_messages in {context}")
            context["bot_messages"] = []
            return

        for i, message_data in enumerate(context["bot_messages"]):
            message_data["sequence_number"] = i

        schedule_sending_chat_context.delay(
            room_group_name=self.room_group_name, context=context
        )

    async def receive(self, text_data):
        """Get the next state including actions and triggers using the .
        Returns the user_text (the user's selected response), the current state context, and the next state context.
        """
        log.info(f"QuizRoomConsumer.receive({text_data})")
        context = json.loads(text_data)

        context = await self.fill_context_with_next_bot_state(self.convo_id, context)
        log.info(f"QuizRoomConsumer.fill_context_with_next_bot_state({context})")

        # state_data["bot_messages"] = await destructure_context_into_messages(
        #     state_data["bot_messages"], state_data["context"]
        # )

        await self.send_context_to_celery_task(context=context)

    async def chat_message_event(self, event_payload):
        """Handles the `chat_message event` type. Sends out a message via websocket after receive()

        This event object is more similar to the Celery task object, so it is not named 'context'
        {
            "state_name": "q4",
            "bot_messages": [
                {"sequence_number": 0, "bot_text": "<p>Well done!</p>"},
                {"sequence_number": 1, "bot_text": "<p>98, 99, 100</p>"},
            ],
            "triggers": [
                {
                    '"from_state"': "q4",
                    '"to_state"': "q4",
                    '"intent_text"': '"OK"',
                    '"update_args"': "null",
                    '"update_kwargs"': "null",
                    '"lang"': '"en"',
                    '"is_button"': "true",
                },
                {
                    '"from_state"': "q4",
                    '"to_state"': "correct-answer-q4",
                    '"intent_text"': '"101"',
                    '"update_args"': "null",
                    '"update_kwargs"': "null",
                    '"lang"': '"en"',
                    '"is_button"': "false",
                },
                {
                    '"from_state"': "q4",
                    '"to_state"': "wrong-answer-q4",
                    '"intent_text"': '"__default__"',
                    '"update_args"': "null",
                    '"update_kwargs"': "null",
                    '"lang"': '"en"',
                    '"is_button"': "false",
                },
            ],
            "lang": "en",
            "convo_id": "8",
            "functions_execution_result": {},
        }
        """
        log.info(f"QuizRoomConsumer.chat_message({event_payload})")
        context = event_payload["context"]

        # Send message via WebSocket
        await self.send(text_data=json.dumps(context))

    async def disconnect(self, close_code):
        """Called when the socket closes. Leaves the channel, room, and user group"""
        log.info(f"QuizRoomConsumer.disconnect({close_code})")
        await self.channel_layer.group_discard(self.room_group_name, self.channel_name)
