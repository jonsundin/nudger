import json
import logging

from quizbot.models import Convo, State, Trigger, StateFunction
from quizbot.model_converters import MessageConverter, TriggerConverted
from quizbot.understanders import TriggerMatcher
from quizbot.actions_system import action_runners
from nudger.constants import START_STATE_NAME

from django.contrib.auth import get_user_model


User = get_user_model()
log = logging.getLogger(__name__)


class StateParser:
    def __init__(self, context):
        self.context = context
        convo_id = context["convo_id"]
        self.convo = Convo.objects.get(id=convo_id)

        self.state_name = context.get("state_name", START_STATE_NAME)
        self.user_text = context.get("user_text", "")
        self.key_to_function_result_mapping = {}
        self.bot_messages = context.get("bot_messages", [])
        self.lang = context.get("lang", "en")
        self.triggers = context.get("triggers", [])

    def fill_context_with_next_state(self):
        self.extract_next_state()
        self.update_context()
        return self.context

    def extract_next_state(self):
        if self.state_name == START_STATE_NAME:
            self.process_start_state()  # only for just started conversation

        self.move_to_next_state()
        self.extract_next_state_assets()
        self.process_next_states_having_next_trigger()

    def process_start_state(self):
        key_to_function_name_mapping = self.key_to_function_name_mapping()
        self.execute_state_functions(key_to_function_name_mapping)
        self.extract_start_state_messages()
        self.user_text = "__next__"

    def key_to_function_name_mapping(self):
        state = State.objects.get(convo=self.convo, state_name=self.state_name)
        state_functions = StateFunction.objects.filter(state=state)
        key_to_function_name_mapping = {}
        for state_func in state_functions:
            key = state_func.mapped_key_name
            func_name = state_func.function_name
            key_to_function_name_mapping[key] = func_name
        return key_to_function_name_mapping

    def execute_state_functions(self, key_to_function_name_mapping):
        if self.user_text:
            for key, fun_name in key_to_function_name_mapping.items():
                self.key_to_function_result_mapping.update(
                    action_runners.run(fun_name, self.user_text, self.context, key)
                )
        return self.key_to_function_result_mapping

    def extract_start_state_messages(self):
        if self.state_has_next_trigger():
            start_state = State.objects.get(
                state_name=START_STATE_NAME, convo=self.convo
            )
            self.bot_messages = MessageConverter.messages_to_list_of_dicts(
                start_state, self.lang
            )
        return self.bot_messages

    def state_has_next_trigger(self):
        from_state = State.objects.get(state_name=self.state_name, convo=self.convo)
        triggers = Trigger.objects.filter(from_state=from_state, is_button=False)
        return any(True if t.intent_text == "__next__" else False for t in triggers)

    def move_to_next_state(self):
        next_state_transition_attributes = self.get_next_state_transition_attributes()
        self.state_name = next_state_transition_attributes["next_state_name"]
        self.user_text = next_state_transition_attributes["through_trigger"]

    def get_next_state_transition_attributes(self):
        from_state = State.objects.get(state_name=self.state_name, convo=self.convo)
        nlp_algo = from_state.nlp_algo
        triggers = TriggerConverted.triggers_to_list_of_dicts(from_state, self.lang)
        next_state = None
        through_trigger = "__default__"
        for t in triggers:
            if TriggerMatcher().is_match(
                algo_name=nlp_algo, trig_text=t["intent_text"], user_text=self.user_text
            ):
                next_state = t["to_state"]
                through_trigger = t["intent_text"]
                break
            if t["intent_text"] == "__default__":
                default_next_state = t["to_state"]
        if next_state is None:
            next_state = default_next_state
        return {
            "next_state_name": next_state.state_name,
            "through_trigger": through_trigger,
        }

    def extract_next_state_assets(self) -> dict:
        to_state = State.objects.get(state_name=self.state_name, convo=self.convo)
        next_state_messages = MessageConverter.messages_to_list_of_dicts(
            to_state, self.lang
        )
        next_state_triggers = TriggerConverted.triggers_to_list_of_dicts(
            to_state, self.lang
        )
        self.bot_messages += next_state_messages
        self.triggers = next_state_triggers
        self.convert_triggers_to_json()

    def convert_triggers_to_json(self):
        triggers = []
        for t in self.triggers:
            stringified_trig = {}
            for key, value in t.items():
                try:
                    stringified_trig[json.dumps(key)] = json.dumps(value)
                except TypeError:
                    stringified_trig[json.dumps(key)] = str(value)
            triggers.append(stringified_trig)
        self.triggers = triggers

    def process_next_states_having_next_trigger(self):
        while self.state_has_next_trigger():
            self.user_text = "__next__"
            self.move_to_next_state()
            self.extract_next_state_assets()
            key_to_function_name_mapping = self.key_to_function_name_mapping()
            self.execute_state_functions(key_to_function_name_mapping)

    def update_context(self):
        self.context.update(
            {
                "state_name": self.state_name,
                "user_text": self.user_text,
                "bot_messages": self.bot_messages,
                "triggers": self.triggers,
            }
        )
        self.context.update(self.key_to_function_result_mapping)
        return self.context
