""" Processes *.dialog.v2.yml files to define Finite State Machine rules """
# from qary.chat.dialog import TurnsPreparation, compose_statement, load_dialog_turns
import logging
import re

import numpy as np

from quizbot.spacy_language_model import nlp


log = logging.getLogger(__name__)


class TriggerMatcher:
    def __init__(self):
        self.__algorithms = {
            "re_match": self._is_re_match_match,
            "re_search": self._is_re_search_match,
            "keyword": self._is_keyword_match,
            "exact": self._is_exact_match,
            "spacy": self._is_spacy_treashold_match,
        }

    def is_match(self, algo_name, trig_text, user_text):
        trig_text, user_text = trig_text.lower().strip(), user_text.lower().strip()
        if algo_name not in self.__algorithms:
            algo_name = "keyword"
        return self.__algorithms[algo_name](trig_text, user_text)

    def _is_re_match_match(self, trig_text, user_text):
        return bool(re.match(trig_text, user_text))

    def _is_re_search_match(self, trig_text, user_text):
        return bool(re.search(trig_text, user_text))

    def _is_keyword_match(self, trig_text, user_text):
        return trig_text in user_text

    def _is_exact_match(self, trig_text, user_text):
        return trig_text == user_text

    def _is_spacy_treashold_match(self, trig_text, user_text):
        threashold = 0.7
        trig_text_vector = nlp(trig_text).vector
        user_text_vector = nlp(user_text).vector

        trig_text_vector /= np.linalg.norm(trig_text_vector)
        user_text_vector /= np.linalg.norm(user_text_vector)
        return trig_text_vector.dot(user_text_vector) >= threashold
