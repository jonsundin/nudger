import convomeld
import yaml
from django.core.files import File
from django.core.files.temp import NamedTemporaryFile
from pathlib import Path

from quizbot.load_yaml import convoyaml_to_convomodel
from quizbot.models import Convo, State, Message, Trigger


def convograph_to_convomodel(
    graph, user, convo_name=None, convo_description=None
) -> Convo:
    convograph = None
    convoyaml = None

    if isinstance(graph, list):
        convograph = graph
    elif isinstance(graph, File):
        convograph = yaml.safe_load(graph.open())
        convoyaml = graph
    elif isinstance(graph, str):
        convoyaml_path = Path(graph)
        convograph = yaml.safe_load(convoyaml_path.open())
        convoyaml = File(convoyaml_path.open("rb"), convoyaml_path.name)
    elif isinstance(graph, Path):
        convograph = yaml.safe_load(graph.open())
        convoyaml = File(graph.open("rb"), graph.name)
    elif hasattr(graph, "read"):
        convograph = yaml.safe_load(graph)

    if convograph is None:
        raise TypeError(f"Unsupported type for graph: {type(graph)}")

    # Set default `convo_name` and `convo_description`
    if convo_name is None or convo_description is None:
        for state in convograph:
            if state["name"] != "start":
                continue

            if convo_name is None:
                convo_name = state["convo_name"]
            if convo_description is None:
                convo_description = state["convo_description"]
            break

    if convoyaml is None:
        tmp = NamedTemporaryFile(mode="r+b")
        yaml.safe_dump(convograph, tmp, encoding="utf-8")
        convoyaml = File(tmp, name=f"{convo_name}.yml")

    convomodel = convoyaml_to_convomodel(
        graph=convoyaml,
        user=user,
        convo_name=convo_name,
        convo_description=convo_description,
    )
    return convomodel


def convomodel_to_convograph(convomodel) -> list[dict]:
    convograph = []

    for state in State.objects.filter(convo=convomodel):
        state_dict = {
            "name": state.state_name,
            "actions": {},
            "triggers": {},
            "buttons": {},
        }
        # Fill in actions
        for message in Message.objects.filter(state=state).order_by("sequence_number"):
            lang_group = state_dict["actions"].setdefault(message.lang, [])
            lang_group.append(message.bot_text)
        # Fill in triggers and buttons
        for trigger in Trigger.objects.filter(from_state=state):
            if trigger.is_button:
                lang_group = state_dict["buttons"].setdefault(trigger.lang, {})
            else:
                lang_group = state_dict["triggers"].setdefault(trigger.lang, {})

            lang_group[trigger.intent_text] = trigger.to_state.state_name
        # Fill in 'start' state attributes
        if state.state_name == "start":
            state_dict["nlp"] = state.nlp_algo
            state_dict["convo_name"] = convomodel.name
            state_dict["convo_description"] = convomodel.description
        # Remove empty actions, triggers and buttons
        if len(state_dict["actions"]) == 0:
            state_dict.pop("actions")
        if len(state_dict["triggers"]) == 0:
            state_dict.pop("triggers")
        if len(state_dict["buttons"]) == 0:
            state_dict.pop("buttons")

        convograph.append(state_dict)

    return convograph


def merge_convos(user, convo_name, convo_description, *convos) -> Convo:
    # convographs is list[Convo]
    try:
        convographs = [convomodel_to_convograph(convomodel) for convomodel in convos]
    except TypeError:
        convographs = convos

    # convographs is list[list[dict]]
    graph = convomeld.merge_graphs(
        *convographs, convo_name=convo_name, convo_description=convo_description
    )
    return convograph_to_convomodel(graph, user)
