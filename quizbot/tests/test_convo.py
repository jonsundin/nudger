# import pytest
#
# from quizbot.convograph_linter import ConvoGraphValidator
#
#
# @pytest.fixture(scope="session")
# def valid_yaml():
#     valid_yaml = ConvoGraphValidator(
#         [
#             {
#                 "name": "start",
#                 "convo_name": "botnar microgrants",
#                 "convo_description": "grant_demo",
#                 "nlp": "keyword",
#                 "level": 0,
#                 "actions": {
#                     "en": [
#                         "Hi, I'm a chatbot at Fondation Botnar.",
#                         "We want to help urban youth around the world.",
#                     ]
#                 },
#                 "triggers": {"en": {"__next__": "microgrant_ideas"}},
#             },
#             {
#                 "name": "microgrant_ideas",
#                 "actions": {"en": ["Do you have any microgrant project ideas?"]},
#                 "triggers": {
#                     "en": {
#                         "I would like to start a youth training center for young girls in math.": "math_education",
#                         "youth": "math_education",
#                         "girls": "math_education",
#                         "math": "math_education",
#                         "__default__": "math_education",
#                     }
#                 },
#             },
#             {
#                 "name": "math_education",
#                 "actions": {
#                     "en": [
#                         "Excellent!",
#                         "This combines two of Fondation Botnar's focus areas - education and gender equity.",
#                         "Where are you located?",
#                     ]
#                 },
#                 "triggers": {
#                     "en": {"Kenya": "eastern_africa", "__default__": "eastern_africa"}
#                 },
#             },
#             {
#                 "name": "eastern_africa",
#                 "actions": {
#                     "en": [
#                         "Here is a link to some resources about math education in Kenya",
#                         '<a href="https://www.fondationbotnar.org/">Fondation Botnar website</a>',
#                     ]
#                 },
#                 "triggers": {"en": {"__default__": "stop"}},
#             },
#             {
#                 "name": "stop",
#                 "actions": {
#                     "en": [
#                         "Thank you!😇 We look forward to reading your application on our [website](https://www.fondationbotnar.org/)"
#                     ]
#                 },
#                 "triggers": {"en": {"__default__": "start"}},
#             },
#         ]
#     )
#     return valid_yaml
#
#
# def test_happy_are_actions_valid(valid_yaml):
#     assert ConvoGraphValidator.are_actions_valid(valid_yaml) is True
#
#
# def test_sad_are_actions_valid_no_actions():
#     no_actions_key_yaml = ConvoGraphValidator(
#         [
#             {
#                 "name": "start",
#                 "convo_name": "botnar microgrants",
#                 "convo_description": "grant_demo",
#                 "nlp": "keyword",
#                 "level": 0,
#                 "something_else": {
#                     "en": [
#                         "Hi, I'm a chatbot at Fondation Botnar.",
#                         "We want to help urban youth around the world.",
#                     ]
#                 },
#                 "triggers": {"en": {"__next__": "microgrant_ideas"}},
#             },
#             {
#                 "name": "microgrant_ideas",
#                 "actions": {"en": ["Do you have any microgrant project ideas?"]},
#                 "triggers": {
#                     "en": {
#                         "I would like to start a youth training center for young girls in math.": "math_education",
#                         "youth": "math_education",
#                         "girls": "math_education",
#                         "math": "math_education",
#                         "__default__": "math_education",
#                     }
#                 },
#             },
#             {
#                 "name": "math_education",
#                 "actions": {
#                     "en": [
#                         "Excellent!",
#                         "This combines two of Fondation Botnar's focus areas - education and gender equity.",
#                         "Where are you located?",
#                     ]
#                 },
#                 "triggers": {
#                     "en": {"Kenya": "eastern_africa", "__default__": "eastern_africa"}
#                 },
#             },
#             {
#                 "name": "eastern_africa",
#                 "actions": {
#                     "en": [
#                         "Here is a link to some resources about math education in Kenya",
#                         '<a href="https://www.fondationbotnar.org/">Fondation Botnar website</a>',
#                     ]
#                 },
#                 "triggers": {"en": {"__default__": "stop"}},
#             },
#             {
#                 "name": "stop",
#                 "actions": {
#                     "en": [
#                         "Thank you!😇 We look forward to reading your application on our [website](https://www.fondationbotnar.org/)"
#                     ]
#                 },
#                 "triggers": {"en": {"__default__": "start"}},
#             },
#         ]
#     )
#     with pytest.raises(KeyError):
#         ConvoGraphValidator.are_actions_valid(no_actions_key_yaml)
#
#
# def test_sad_are_actions_valid_not_list():
#     """note that we don't actually get to call this function because we get an error upon attempting to create
#     a ConvoGraphValidator object with a dictionary"""
#     with pytest.raises(Exception) as excinfo:
#         not_list_yaml = ConvoGraphValidator(
#             {
#                 {
#                     "name": "start",
#                     "convo_name": "botnar microgrants",
#                     "convo_description": "grant_demo",
#                     "nlp": "keyword",
#                     "level": 0,
#                     "actions": {
#                         "en": [
#                             "Hi, I'm a chatbot at Fondation Botnar.",
#                             "We want to help urban youth around the world.",
#                         ]
#                     },
#                     "triggers": {"en": {"__next__": "microgrant_ideas"}},
#                 },
#                 {
#                     "name": "microgrant_ideas",
#                     "actions": {"en": ["Do you have any microgrant project ideas?"]},
#                     "triggers": {
#                         "en": {
#                             "I would like to start a youth training center for young girls in math.": "math_education",
#                             "youth": "math_education",
#                             "girls": "math_education",
#                             "math": "math_education",
#                             "__default__": "math_education",
#                         }
#                     },
#                 },
#                 {
#                     "name": "math_education",
#                     "actions": {
#                         "en": [
#                             "Excellent!",
#                             "This combines two of Fondation Botnar's focus areas - education and gender equity.",
#                             "Where are you located?",
#                         ]
#                     },
#                     "triggers": {
#                         "en": {
#                             "Kenya": "eastern_africa",
#                             "__default__": "eastern_africa",
#                         }
#                     },
#                 },
#                 {
#                     "name": "eastern_africa",
#                     "actions": {
#                         "en": [
#                             "Here is a link to some resources about math education in Kenya",
#                             '<a href="https://www.fondationbotnar.org/">Fondation Botnar website</a>',
#                         ]
#                     },
#                     "triggers": {"en": {"__default__": "stop"}},
#                 },
#                 {
#                     "name": "stop",
#                     "actions": {
#                         "en": [
#                             "Thank you!😇 We look forward to reading your application on our [website](https://www.fondationbotnar.org/)"
#                         ]
#                     },
#                     "triggers": {"en": {"__default__": "start"}},
#                 },
#             }
#         )
#     assert str(excinfo.value) == "unhashable type: 'dict'"
#
#
# def test_happy_does_dialog_have_start_state(valid_yaml):
#     assert valid_yaml.does_dialog_have_start_state() is True
#
#
# def test_sad_does_dialog_have_start_state():
#     no_start_state_yaml = ConvoGraphValidator(
#         [
#             {
#                 "name": "something_else",
#                 "convo_name": "botnar microgrants",
#                 "convo_description": "grant_demo",
#                 "nlp": "keyword",
#                 "level": 0,
#                 "actions": {
#                     "en": [
#                         "Hi, I'm a chatbot at Fondation Botnar.",
#                         "We want to help urban youth around the world.",
#                     ]
#                 },
#                 "triggers": {"en": {"__next__": "microgrant_ideas"}},
#             },
#             {
#                 "name": "microgrant_ideas",
#                 "actions": {"en": ["Do you have any microgrant project ideas?"]},
#                 "triggers": {
#                     "en": {
#                         "I would like to start a youth training center for young girls in math.": "math_education",
#                         "youth": "math_education",
#                         "girls": "math_education",
#                         "math": "math_education",
#                         "__default__": "math_education",
#                     }
#                 },
#             },
#             {
#                 "name": "math_education",
#                 "actions": {
#                     "en": [
#                         "Excellent!",
#                         "This combines two of Fondation Botnar's focus areas - education and gender equity.",
#                         "Where are you located?",
#                     ]
#                 },
#                 "triggers": {
#                     "en": {"Kenya": "eastern_africa", "__default__": "eastern_africa"}
#                 },
#             },
#             {
#                 "name": "eastern_africa",
#                 "actions": {
#                     "en": [
#                         "Here is a link to some resources about math education in Kenya",
#                         '<a href="https://www.fondationbotnar.org/">Fondation Botnar website</a>',
#                     ]
#                 },
#                 "triggers": {"en": {"__default__": "stop"}},
#             },
#             {
#                 "name": "stop",
#                 "actions": {
#                     "en": [
#                         "Thank you!😇 We look forward to reading your application on our [website](https://www.fondationbotnar.org/)"
#                     ]
#                 },
#                 "triggers": {"en": {"__default__": "start"}},
#             },
#         ]
#     )
#     custom_error = no_start_state_yaml.does_dialog_have_start_state().error
#     assert str(custom_error) == "Dialog doesn't have state called 'start'"
#
#
# def test_happy_are_state_names_vaild(valid_yaml):
#     assert valid_yaml.are_state_names_valid() is True
#
#
# def test_sad_are_state_names_valid():
#     invalid_names_yaml = ConvoGraphValidator(
#         [
#             {
#                 "name": "start",
#                 "convo_name": "botnar microgrant$",
#                 "convo_description": "grant_demo",
#                 "nlp": "keyword",
#                 "level": 0,
#                 "actions": {
#                     "en": [
#                         "Hi, I'm a chatbot at Fondation Botnar.",
#                         "We want to help urban youth around the world.",
#                     ]
#                 },
#                 "trigger$": {"*n": {"__next__": "microgrant_ideas"}},
#             },
#             {
#                 "name": "microgrant_idea$",
#                 "actions": {"en": ["Do you have any microgrant project ideas?"]},
#                 "triggers": {
#                     "en": {
#                         "I would like to start a youth training center for young girls in math.": "math_education",
#                         "youth": "math_education",
#                         "girls": "math_education",
#                         "math": "math_education",
#                         "__default__": "math_education",
#                     }
#                 },
#             },
#             {
#                 "name": "m@th_educ@tion",
#                 "actions": {
#                     "en": [
#                         "Excellent!",
#                         "This combines two of Fondation Botnar's focus areas - education and gender equity.",
#                         "Where are you located?",
#                     ]
#                 },
#                 "triggers": {
#                     "en": {"Kenya": "eastern_africa", "__default__": "eastern_africa"}
#                 },
#             },
#             {
#                 "name": "#astern_africa",
#                 "actions": {
#                     "en": [
#                         "Here is a link to some resources about math education in Kenya",
#                         '<a href="https://www.fondationbotnar.org/">Fondation Botnar website</a>',
#                     ]
#                 },
#                 "triggers": {"en": {"__default__": "stop"}},
#             },
#             {
#                 "name": "$!*p",
#                 "actions": {
#                     "en": [
#                         "Thank you!😇 We look forward to reading your application on our [website](https://www.fondationbotnar.org/)"
#                     ]
#                 },
#                 "triggers": {"en": {"__default__": "start"}},
#             },
#         ]
#     )
#     custom_error = invalid_names_yaml.are_state_names_valid().error
#     expected = "isn't valid name for state!"
#     assert expected in str(custom_error)
#
#
# def test_happy_is_state_name_valid(valid_yaml):
#     assert valid_yaml.is_state_name_valid("hello") is True
#
#
# def test_sad_is_state_name_valid(valid_yaml):
#     assert valid_yaml.is_state_name_valid("*hello*") is False
#
#
# def test_happy_are_state_names_unique(valid_yaml):
#     assert valid_yaml.are_state_names_unique()
#
#
# def test_sad_are_state_names_unique_one():
#     state_names_not_unique_yaml_one = ConvoGraphValidator(
#         [
#             {
#                 "name": "start",
#                 "convo_name": "botnar microgrants",
#                 "convo_description": "grant_demo",
#                 "nlp": "keyword",
#                 "level": 0,
#                 "actions": {
#                     "en": [
#                         "Hi, I'm a chatbot at Fondation Botnar.",
#                         "We want to help urban youth around the world.",
#                     ]
#                 },
#                 "triggers": {"en": {"__next__": "microgrant_ideas"}},
#             },
#             {
#                 "name": "botnar microgrants",
#                 "actions": {"en": ["Do you have any microgrant project ideas?"]},
#                 "triggers": {
#                     "en": {
#                         "I would like to start a youth training center for young girls in math.": "math_education",
#                         "youth": "math_education",
#                         "girls": "math_education",
#                         "math": "math_education",
#                         "__default__": "math_education",
#                     }
#                 },
#             },
#             {
#                 "name": "botnar microgrants",
#                 "actions": {
#                     "en": [
#                         "Excellent!",
#                         "This combines two of Fondation Botnar's focus areas - education and gender equity.",
#                         "Where are you located?",
#                     ]
#                 },
#                 "triggers": {
#                     "en": {"Kenya": "eastern_africa", "__default__": "eastern_africa"}
#                 },
#             },
#             {
#                 "name": "botnar microgrants",
#                 "actions": {
#                     "en": [
#                         "Here is a link to some resources about math education in Kenya",
#                         '<a href="https://www.fondationbotnar.org/">Fondation Botnar website</a>',
#                     ]
#                 },
#                 "triggers": {"en": {"__default__": "stop"}},
#             },
#             {
#                 "name": "stop",
#                 "actions": {
#                     "en": [
#                         "Thank you!😇 We look forward to reading your application on our [website](https://www.fondationbotnar.org/)"
#                     ]
#                 },
#                 "triggers": {"en": {"__default__": "start"}},
#             },
#         ]
#     )
#     custom_error = state_names_not_unique_yaml_one.are_state_names_unique().error
#     expected = "state occurs more than once!"
#     assert expected in custom_error
#
#
# def test_sad_are_state_names_unique_multiple():
#     state_names_not_unique_yaml_multiple = ConvoGraphValidator(
#         [
#             {
#                 "name": "start",
#                 "convo_name": "botnar microgrants",
#                 "convo_description": "grant_demo",
#                 "nlp": "keyword",
#                 "level": 0,
#                 "actions": {
#                     "en": [
#                         "Hi, I'm a chatbot at Fondation Botnar.",
#                         "We want to help urban youth around the world.",
#                     ]
#                 },
#                 "triggers": {"en": {"__next__": "microgrant_ideas"}},
#             },
#             {
#                 "name": "microgrant_ideas",
#                 "actions": {"en": ["Do you have any microgrant project ideas?"]},
#                 "triggers": {
#                     "en": {
#                         "I would like to start a youth training center for young girls in math.": "math_education",
#                         "youth": "math_education",
#                         "girls": "math_education",
#                         "math": "math_education",
#                         "__default__": "math_education",
#                     }
#                 },
#             },
#             {
#                 "name": "microgrant_ideas",
#                 "actions": {
#                     "en": [
#                         "Excellent!",
#                         "This combines two of Fondation Botnar's focus areas - education and gender equity.",
#                         "Where are you located?",
#                     ]
#                 },
#                 "triggers": {
#                     "en": {"Kenya": "eastern_africa", "__default__": "eastern_africa"}
#                 },
#             },
#             {
#                 "name": "eastern_africa",
#                 "actions": {
#                     "en": [
#                         "Here is a link to some resources about math education in Kenya",
#                         '<a href="https://www.fondationbotnar.org/">Fondation Botnar website</a>',
#                     ]
#                 },
#                 "triggers": {"en": {"__default__": "stop"}},
#             },
#             {
#                 "name": "botnar microgrants",
#                 "actions": {
#                     "en": [
#                         "Thank you!😇 We look forward to reading your application on our [website](https://www.fondationbotnar.org/)"
#                     ]
#                 },
#                 "triggers": {"en": {"__default__": "start"}},
#             },
#         ]
#     )
#     custom_error = state_names_not_unique_yaml_multiple.are_state_names_unique().error
#     expected = "state occurs more than once!"
#     assert expected in custom_error
#
#
# def test_happy_do_all_triggers_have_essential(valid_yaml):
#     no_response = valid_yaml.do_all_triggers_have_essential()
#     assert no_response is None
#
#
# def test_sad_do_all_triggers_have_essential():
#     """if no triggers, get KeyError"""
#     no_triggers_yaml = ConvoGraphValidator(
#         [
#             {
#                 "name": "start",
#                 "convo_name": "botnar microgrants",
#                 "convo_description": "grant_demo",
#                 "nlp": "keyword",
#                 "level": 0,
#                 "actions": {
#                     "en": [
#                         "Hi, I'm a chatbot at Fondation Botnar.",
#                         "We want to help urban youth around the world.",
#                     ]
#                 },
#                 "tigers": {"en": {"__next__": "microgrant_ideas"}},
#             },
#             {
#                 "name": "microgrant_ideas",
#                 "actions": {"en": ["Do you have any microgrant project ideas?"]},
#                 "tigers": {
#                     "en": {
#                         "I would like to start a youth training center for young girls in math.": "math_education",
#                         "youth": "math_education",
#                         "girls": "math_education",
#                         "math": "math_education",
#                         "__default__": "math_education",
#                     }
#                 },
#             },
#             {
#                 "name": "math_education",
#                 "actions": {
#                     "en": [
#                         "Excellent!",
#                         "This combines two of Fondation Botnar's focus areas - education and gender equity.",
#                         "Where are you located?",
#                     ]
#                 },
#                 "tigers": {
#                     "en": {"Kenya": "eastern_africa", "__default__": "eastern_africa"}
#                 },
#             },
#             {
#                 "name": "eastern_africa",
#                 "actions": {
#                     "en": [
#                         "Here is a link to some resources about math education in Kenya",
#                         '<a href="https://www.fondationbotnar.org/">Fondation Botnar website</a>',
#                     ]
#                 },
#                 "tigers": {"en": {"__default__": "stop"}},
#             },
#             {
#                 "name": "stop",
#                 "actions": {
#                     "en": [
#                         "Thank you!😇 We look forward to reading your application on our [website](https://www.fondationbotnar.org/)"
#                     ]
#                 },
#                 "tigers": {"en": {"__default__": "start"}},
#             },
#         ]
#     )
#     with pytest.raises(KeyError):
#         no_triggers_yaml.do_all_triggers_have_essential()
#
#
# def test_sad_do_all_triggers_have_essential_empty_string():
#     empty_triggers_yaml = ConvoGraphValidator(
#         [
#             {
#                 "name": "start",
#                 "convo_name": "botnar microgrants",
#                 "convo_description": "grant_demo",
#                 "nlp": "keyword",
#                 "level": 0,
#                 "actions": {
#                     "en": [
#                         "Hi, I'm a chatbot at Fondation Botnar.",
#                         "We want to help urban youth around the world.",
#                     ]
#                 },
#                 "triggers": {},
#             },
#             {
#                 "name": "microgrant_ideas",
#                 "actions": {"en": ["Do you have any microgrant project ideas?"]},
#                 "triggers": {},
#                 "triggers": {
#                     "en": {"Kenya": "eastern_africa", "__default__": "eastern_africa"}
#                 },
#             },
#             {
#                 "name": "eastern_africa",
#                 "actions": {
#                     "en": [
#                         "Here is a link to some resources about math education in Kenya",
#                         '<a href="https://www.fondationbotnar.org/">Fondation Botnar website</a>',
#                     ]
#                 },
#                 "triggers": {"en": {"__default__": "stop"}},
#             },
#             {
#                 "name": "stop",
#                 "actions": {
#                     "en": [
#                         "Thank you!😇 We look forward to reading your application on our [website](https://www.fondationbotnar.org/)"
#                     ]
#                 },
#                 "triggers": {"en": {"__default__": "start"}},
#             },
#         ]
#     )
#     no_response = empty_triggers_yaml.do_all_triggers_have_essential()
#     assert no_response is None
#
#
# def test_sad_do_all_triggers_have_essential_bad_string():
#     valid_yaml = YamlLinter(
#         [
#             {
#                 "name": "start",
#                 "convo_name": "botnar microgrants",
#                 "convo_description": "grant_demo",
#                 "nlp": "keyword",
#                 "level": 0,
#                 "actions": {
#                     "en": [
#                         "Hi, I'm a chatbot at Fondation Botnar.",
#                         "We want to help urban youth around the world.",
#                     ]
#                 },
#                 "triggers": {"en": {"__next__": "microgrant_ideas"}},
#             },
#             {
#                 "name": "microgrant_ideas",
#                 "actions": {"en": ["Do you have any microgrant project ideas?"]},
#                 "triggers": {
#                     "en": {
#                         "I would like to start a youth training center for young girls in math.": "math_education",
#                         "youth": "math_education",
#                         "girls": "math_education",
#                         "math": "math_education",
#                         "__default__": "math_education",
#                     }
#                 },
#             },
#             {
#                 "name": "math_education",
#                 "actions": {
#                     "en": [
#                         "Excellent!",
#                         "This combines two of Fondation Botnar's focus areas - education and gender equity.",
#                         "Where are you located?",
#                     ]
#                 },
#                 "triggers": {
#                     "en": {"Kenya": "eastern_africa", "__default__": "eastern_africa"}
#                 },
#             },
#             {
#                 "name": "eastern_africa",
#                 "actions": {
#                     "en": [
#                         "Here is a link to some resources about math education in Kenya",
#                         '<a href="https://www.fondationbotnar.org/">Fondation Botnar website</a>',
#                     ]
#                 },
#                 "triggers": {"en": {"__default__": "stop"}},
#             },
#             {
#                 "name": "stop",
#                 "actions": {
#                     "en": [
#                         "Thank you!😇 We look forward to reading your application on our [website](https://www.fondationbotnar.org/)"
#                     ]
#                 },
#                 "triggers": {"en": {"__default__": "start"}},
#             },
#         ]
#     )
#
#
# def test_happy_is_dunder_trigger_name_valid(valid_yaml):
#     assert valid_yaml.is_dunder_trigger_name_valid("trigger")
#
#
# def sad_is_dunder_trigger_name_valid():
#     pass
#
#
# def test_happy_are_target_states_exist(valid_yaml):
#     assert valid_yaml.are_target_states_exist()
#
#
# def sad_are_target_states_exist():
#     pass
#
#
# def test_happy_do_all_states_have_triggers(valid_yaml):
#     assert valid_yaml.do_all_states_have_triggers()
#
#
# def test_sad_do_all_states_have_triggers_all():
#     triggers_replaced_with_not_yaml = YamlLinter(
#         [
#             {
#                 "name": "start",
#                 "convo_name": "botnar microgrants",
#                 "convo_description": "grant_demo",
#                 "nlp": "keyword",
#                 "level": 0,
#                 "actions": {
#                     "en": [
#                         "Hi, I'm a chatbot at Fondation Botnar.",
#                         "We want to help urban youth around the world.",
#                     ]
#                 },
#                 "not": {"en": {"__next__": "microgrant_ideas"}},
#             },
#             {
#                 "name": "microgrant_ideas",
#                 "actions": {"en": ["Do you have any microgrant project ideas?"]},
#                 "not": {
#                     "en": {
#                         "I would like to start a youth training center for young girls in math.": "math_education",
#                         "youth": "math_education",
#                         "girls": "math_education",
#                         "math": "math_education",
#                         "__default__": "math_education",
#                     }
#                 },
#             },
#             {
#                 "name": "math_education",
#                 "actions": {
#                     "en": [
#                         "Excellent!",
#                         "This combines two of Fondation Botnar's focus areas - education and gender equity.",
#                         "Where are you located?",
#                     ]
#                 },
#                 "not": {
#                     "en": {"Kenya": "eastern_africa", "__default__": "eastern_africa"}
#                 },
#             },
#             {
#                 "name": "eastern_africa",
#                 "actions": {
#                     "en": [
#                         "Here is a link to some resources about math education in Kenya",
#                         '<a href="https://www.fondationbotnar.org/">Fondation Botnar website</a>',
#                     ]
#                 },
#                 "not": {"en": {"__default__": "stop"}},
#             },
#             {
#                 "name": "stop",
#                 "actions": {
#                     "en": [
#                         "Thank you!😇 We look forward to reading your application on our [website](https://www.fondationbotnar.org/)"
#                     ]
#                 },
#                 "not": {"en": {"__default__": "start"}},
#             },
#         ]
#     )
#     custom_error = triggers_replaced_with_not_yaml.do_all_states_have_triggers().error
#     expected = "state doesn't have triggers!"  # should need to change to doesnt
#     assert expected in custom_error
#     # what error prints out'{\'name\': \'start\', \'convo_name\': \'botnar microgrants\', \'convo_description\': \'grant_demo\', \'nlp\': \'keyword\', \'level\': 0, \'actions\': {\'en\': ["Hi, I\'m a chatbot at Fondation Botnar.", \'We want to help urban youth around the world.\']}, \'not\': {\'en\': {\'__next__\': \'microgrant_ideas\'}}} state doesn\'t have triggers!'
#
#
# def test_sad_do_all_states_have_triggers_one():
#     one_trigger_replaced_with_yaml = YamlLinter(
#         [
#             {
#                 "name": "start",
#                 "convo_name": "botnar microgrants",
#                 "convo_description": "grant_demo",
#                 "nlp": "keyword",
#                 "level": 0,
#                 "actions": {
#                     "en": [
#                         "Hi, I'm a chatbot at Fondation Botnar.",
#                         "We want to help urban youth around the world.",
#                     ]
#                 },
#                 "triggers": {"en": {"__next__": "microgrant_ideas"}},
#             },
#             {
#                 "name": "microgrant_ideas",
#                 "actions": {"en": ["Do you have any microgrant project ideas?"]},
#                 "triggers": {
#                     "en": {
#                         "I would like to start a youth training center for young girls in math.": "math_education",
#                         "youth": "math_education",
#                         "girls": "math_education",
#                         "math": "math_education",
#                         "__default__": "math_education",
#                     }
#                 },
#             },
#             {
#                 "name": "math_education",
#                 "actions": {
#                     "en": [
#                         "Excellent!",
#                         "This combines two of Fondation Botnar's focus areas - education and gender equity.",
#                         "Where are you located?",
#                     ]
#                 },
#                 "not": {
#                     "en": {"Kenya": "eastern_africa", "__default__": "eastern_africa"}
#                 },
#             },
#             {
#                 "name": "eastern_africa",
#                 "actions": {
#                     "en": [
#                         "Here is a link to some resources about math education in Kenya",
#                         '<a href="https://www.fondationbotnar.org/">Fondation Botnar website</a>',
#                     ]
#                 },
#                 "triggers": {"en": {"__default__": "stop"}},
#             },
#             {
#                 "name": "stop",
#                 "actions": {
#                     "en": [
#                         "Thank you!😇 We look forward to reading your application on our [website](https://www.fondationbotnar.org/)"
#                     ]
#                 },
#                 "triggers": {"en": {"__default__": "start"}},
#             },
#         ]
#     )
#     custom_error = one_trigger_replaced_with_yaml.do_all_states_have_triggers().error
#     expected = "state doesn't have triggers!"
#     assert expected in custom_error
