""" Find and run doctests """
import doctest
import unittest

from users import utils
from quizbot.actions_system import extractors
from quizbot import consumers, bot_manager

# FIXME: does not discover tests in users or nudger, only quizbot
#        entire package nudger/convohub should be importable and installed with pyproject.toml
testsuite = unittest.TestLoader().discover("..")


def load_tests(*args, **kwargs):
    test_all_doctests = unittest.TestSuite()
    for module in (extractors, consumers, bot_manager, utils):
        test_all_doctests.addTest(
            doctest.DocTestSuite(
                module, optionflags=(doctest.ELLIPSIS | doctest.NORMALIZE_WHITESPACE)
            )
        )
    return test_all_doctests
