"""Tests user creation, convo creation, state manager"""

from django.contrib.auth import get_user_model
from django.test import TestCase

from quizbot.bot_manager import StateParser
from quizbot.models import Convo
from scripts.create_default_convo_on_db import create_convo

from nudger.constants import DEFAULT_CONVOGRAPH_YAML_PATH


class QuizbotConvoTest(TestCase):
    """Tests convo creation and state manager"""

    user_dict = {
        "username": "normal_user",
        "password": "NORMAL_USER_PASSWORD",
        "email": "first.last@mail.qary.ai",
    }

    default_yaml_path = DEFAULT_CONVOGRAPH_YAML_PATH

    def setUp(self):
        self.user = get_user_model().objects.create_user(**self.user_dict)
        self.convo = create_convo(self.default_yaml_path, self.user)

    def tearDown(self):
        self.user.delete()
        self.convo.delete()

    def test_convo_creation(self):
        """Test if convo is a Convo object"""
        self.assertTrue(isinstance(self.convo, Convo))

    def test_retrive_users_convos(self):
        """Test if we can retrieve convo for the user"""
        convo_qs = Convo.objects.filter(user=self.user)
        self.assertEqual(convo_qs.count(), 1)

    def test_state_data_package_1(self):
        """Tests data package 1 with the correct response"""
        context = {
            "state_name": "q1",
            "user_text": "11",
            "lang": "en",
            "convo_id": self.convo.id,
        }
        updated_context = StateParser(context).fill_context_with_next_state()
        test_context = {
            "state_name": "q2",
            "user_text": "__next__",
            "bot_messages": [
                {"sequence_number": 0, "bot_text": "<p>Exactly right!</p>"},
                {"sequence_number": 0, "bot_text": "<p>16, 17, 18</p>"},
            ],
            "triggers": [
                {
                    '"from_state"': "q2",
                    '"to_state"': "q2",
                    '"intent_text"': '"OK"',
                    '"update_args"': "null",
                    '"update_kwargs"': "null",
                    '"lang"': '"en"',
                    '"is_button"': "true",
                },
                {
                    '"from_state"': "q2",
                    '"to_state"': "correct-answer-q2",
                    '"intent_text"': '"19"',
                    '"update_args"': "null",
                    '"update_kwargs"': "null",
                    '"lang"': '"en"',
                    '"is_button"': "false",
                },
                {
                    '"from_state"': "q2",
                    '"to_state"': "wrong-answer-q2",
                    '"intent_text"': '"__default__"',
                    '"update_args"': "null",
                    '"update_kwargs"': "null",
                    '"lang"': '"en"',
                    '"is_button"': "false",
                },
            ],
            "lang": "en",
            "convo_id": 1,
        }
        self.assertEquals(updated_context, test_context)

    def test_state_data_package_2(self):
        """Tests data package 2 with a wrong response"""
        context = {
            "state_name": "q1",
            "user_text": "99",
            "lang": "en",
            "convo_id": self.convo.id,
        }
        updated_context = StateParser(context).fill_context_with_next_state()
        test_response = {
            "state_name": "q1",
            "user_text": "__next__",
            "bot_messages": [
                {"sequence_number": 0, "bot_text": "<p>Oops!</p>"},
                {"sequence_number": 0, "bot_text": "<p>8, 9, 10</p>"},
            ],
            "triggers": [
                {
                    '"from_state"': "q1",
                    '"to_state"': "q1",
                    '"intent_text"': '"OK"',
                    '"update_args"': "null",
                    '"update_kwargs"': "null",
                    '"lang"': '"en"',
                    '"is_button"': "true",
                },
                {
                    '"from_state"': "q1",
                    '"to_state"': "correct-answer-q1",
                    '"intent_text"': '"11"',
                    '"update_args"': "null",
                    '"update_kwargs"': "null",
                    '"lang"': '"en"',
                    '"is_button"': "false",
                },
                {
                    '"from_state"': "q1",
                    '"to_state"': "correct-answer-q1",
                    '"intent_text"': '"eleven"',
                    '"update_args"': "null",
                    '"update_kwargs"': "null",
                    '"lang"': '"en"',
                    '"is_button"': "false",
                },
                {
                    '"from_state"': "q1",
                    '"to_state"': "wrong-answer-q1",
                    '"intent_text"': '"__default__"',
                    '"update_args"': "null",
                    '"update_kwargs"': "null",
                    '"lang"': '"en"',
                    '"is_button"': "false",
                },
            ],
            "lang": "en",
            "convo_id": 1,
        }
        self.assertEquals(updated_context, test_response)
