""" Ensure that a SpaCy language model has been downloaded and is a Monad (Singleton) """

import spacy


from nudger.constants import SPACY_MODEL


def load(spacy_model=SPACY_MODEL):
    spacy_model = spacy_model or SPACY_MODEL or "en_core_web_md"
    try:
        return spacy.load(SPACY_MODEL)
    except OSError:
        spacy.cli.download(SPACY_MODEL)
        return spacy.load(SPACY_MODEL)


nlp = load()
