""" Functions that the conversation designer can call within a v3 Convograph

TODO: Secure server against arbitrary code execution
  - cryptographic signing of python functions (checksum + encryption)
    - decorator to add signatures as function attributes
    - extract_proper_nouns.__auth__ == ENV.get("__auth__", "")
  - gRPC = remote procedure calls
  - custom REST API endpoints for all actions
  - cloud/lambda functions on (Digital Ocean/cloudflare/AWS) (can they handle large RAM ML models?)
  - ephemeral Docker containers managed with Kubernetes or Terraform
  - javascript or WASM (web assembly) in the user's browser - only works for faceofqary chat widget, not SMS
"""
import logging
import re

from nudger.constants import USER_TEXT_KEYS, LANGUAGES, LANG_MAPPINGS


log = logging.getLogger(__name__)


def find_user_text(context, keys=USER_TEXT_KEYS):
    """Try to find the key in the context where the user_text is stored"""
    for k in keys:
        if k in context:
            return context[k]
    raise ValueError(
        f"Unable to find a user_text (user_utterance) in context={context}"
    )


def extract_lang(context):
    """Return the 2-char ISO name of a spoken language (e.g. English) extracted from a str
    >>> extract_lang(context=dict(user_text='I speak english'))
    'en'
    >>> extract_lang(context=dict(user_text='I like Ukrainian'))
    'ua'
    """
    context = context.get("context", context)
    user_text = find_user_text(context=context)
    languages = context.get("languages", LANGUAGES)
    if user_text is None:
        return None
    matches = re.findall("\\b(" + "|".join(languages) + ")\\b", user_text.lower())
    if matches:
        return LANG_MAPPINGS.get(matches[0])
    return LANG_MAPPINGS
