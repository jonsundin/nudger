import re

from nudger.constants import LANG_MAPPINGS
from quizbot.spacy_language_model import nlp


def extract_proper_nouns(context, key="user_text", pos="PROPN", ent_type=None):
    """Extract the names of persons, places, and things as a list of strs

    >>> text = "My name is Cetin, HIS NAME IS Ruslan Borislov."
    >>> extract_proper_nouns(context=dict(user_text=text))
    ['Cetin', 'Ruslan Borislov']
    """
    text = context.get(key)
    if not text:
        return None
    doc = nlp(text)
    names = []
    i = 0
    while i < len(doc):
        # if not ((tok.pos_ != pos) or (tok.ent_type_ != ent_type)):
        tok = doc[i]
        if (pos is None or tok.pos_ == pos) and (
            ent_type is None or tok.ent_type_ != ent_type
        ):
            person = [tok.text]
            i += 1
            while i < len(doc):
                t = doc[i]
                i += 1
                if not (
                    (pos is None or t.pos_ == pos)
                    and (ent_type is None or t.ent_type_ != ent_type)
                ):
                    break
                person.append(t.text)
            names.append(" ".join(person))
        else:
            i += 1
    return names


def extract_lang(context, key="user_text"):
    """Return the 2-char ISO name of a spoken language (e.g. English) extracted from a str

    >>> extract_lang(context=dict(user_text='I speak english'))
    'en'
    >>> extract_lang(context=dict(user_text='I misspell Ukrainean'))
    >>> extract_lang(context=dict(user_text='I misspell Ukrainian'))
    'uk'
    """
    text = context.get(key)
    languages = list(LANG_MAPPINGS)
    if text:
        pattern = "\\b(" + "|".join(languages) + ")\\b"
        matches = re.findall(pattern, text.lower())
        if matches:
            return LANG_MAPPINGS.get(matches[0])
