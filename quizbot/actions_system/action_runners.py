from nudger.constants import ALLOWED_ACTIONS


def update_context(context, key, value):
    """Add a key value pair to a context dict"""
    if context is None:
        context = dict()
    context = dict(context)
    key = str(key)
    context[key] = value
    return context


def is_allowed(name: str):
    """Check the action name (str) to see if it is among the list of actions allowed"""
    return (
        name in ALLOWED_ACTIONS
        # and name in globals()
        and not name.startswith("_")
        and not name.endswith("_")
    )


def run(action, text="", context=None, key=None):
    """Run the requested action (function) and return the updated context

    # FIXME: The following test results in
    # {'user_text': 'My name is Cetin, HIS NAME IS rUSLAN  Borislov.', 'proper_nouns': ['Cetin', 'Borislov']}
    text = "My name is Cetin, HIS NAME IS rUSLAN  Borislov."
    run(action='extract_proper_nouns', text=text)
    {'extract_proper_nouns': ['Cetin', 'rUSLAN Borisov']}
    """
    fun = action

    # FIXME: is_allowed(action) produces the error: NameError: No function named 'quizbot.actions.extract_proper_nouns' or 'extract_proper_nouns'.
    # if isinstance(action, str) and is_allowed(action):
    if isinstance(action, str):
        fun = globals().get(action, None)
    if not callable(fun):
        raise NameError(f"No function named '{__name__}.{action}' or '{fun}'. ")
    if not key:
        key = fun.__name__
        if key.startswith("extract_"):
            key = key[8:]  # 8 == len('extract_')
    result = fun(text)
    return update_context(context=context, key=key, value=result)
