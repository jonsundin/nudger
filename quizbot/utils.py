## TODO> transform to REST API
import os

from datetime import datetime

from django.contrib.auth.models import User

from .models import Convo


class ConvoAction:
    def __init__(self):
        self._actions = {"make_public": self.publish, "make_private": self.unpublish}

    def apply_action(self, action, user, convo_ids):
        return self._actions[action](user, convo_ids)

    def publish(self, user, convo_ids):
        published_convos = []
        for id in convo_ids:
            convo = Convo.objects.filter(user=user, id=id).update(is_public=True)
            published_convos.append(convo)
        return published_convos

    def unpublish(self, user, convo_ids):
        published_convos = []
        for id in convo_ids:
            convo = Convo.objects.filter(user=user, id=id).update(is_public=False)
            published_convos.append(convo)
        return published_convos


class ConvoQuery:
    query_arguments = {
        "search_convo": "search",
        "id": "id",
        "name": "name",
        "description": "description",
        "username": "user__username",
        "start_date": "start_date",
        "end_date": "end_date",
    }

    def __init__(self, user_id, query):
        self._user_id = user_id
        self._user = User.objects.get(id=user_id)
        self._query = query

    def get_query_result(self):
        if not self._query:
            convos = set(Convo.objects.filter(user__id=self._user_id))
            for c in Convo.objects.filter(is_public=True):
                convos.add(c)
            return convos
        if self.query_arguments["search_convo"] in self._query:
            return self.search_convos()
        return self.filter_convos()

    def get_query_by_attributes(self):
        search_by = [
            self.query_arguments["id"],
            self.query_arguments["name"],
            self.query_arguments["description"],
            self.query_arguments["username"],
        ]
        for i, item in enumerate(search_by.copy()):
            search_by[i] = item + "__contains"
        return search_by

    def search_convos(self):
        """used to return Convo list when queried via search bar hence self._query contains `search` key"""
        search_by = self.get_query_by_attributes()
        convos = set()
        search_value = self._query[self.query_arguments["search_convo"]]
        for item in search_by:
            user_convos = Convo.objects.filter(
                user__id=self._user_id, **{item: search_value}
            )
            if user_convos:
                for c in user_convos:
                    convos.add(c)
        return convos

    @staticmethod
    def convert_str_date_to_datetime(js_date_string: str):
        """
        >>> convert_str_date_to_datetime("Wed Jul 28 1993")
        1993-07-28 00:00:00
        """
        js_date_format = "%a %b %d %Y"

        python_datetime = datetime.strptime(js_date_string, js_date_format)
        return python_datetime

    def filter_convos(self):
        filter_by = {}
        for filter in self.get_query_by_attributes():
            for arg, val in self._query.items():
                if filter.startswith(arg):
                    filter_by[filter] = val
                    break
        activated_on_range = [None, None]
        if ("start_date", "end_date") <= tuple(self._query):
            activated_on_range[0] = self.convert_str_date_to_datetime(
                self._query["start_date"]
            )
            activated_on_range[1] = self.convert_str_date_to_datetime(
                self._query["end_date"]
            )
            filter_by["activated_on__range"] = activated_on_range
        return Convo.objects.filter(user_id=self._user_id, **filter_by).order_by(
            "-activated_on"
        )


class BotHubConvoQuery(ConvoQuery):
    def search_convos(self):
        """used to return Convo list when queried via search bar hence self._query contains `search` key"""
        search_by = self.get_query_by_attributes()
        search_value = self._query[self.query_arguments["search_convo"]]
        convos = super().search_convos()
        for item in search_by:
            for c in Convo.objects.filter(is_public=True, **{item: search_value}):
                convos.add(c)
        return convos

    def filter_private_convos(self):
        filter_by = {}
        for filter in ("name", "description"):
            if filter in self._query:
                filter_by[filter + "__contains"] = self._query[filter]
        activated_on_range = [None, None]
        if ("start_date", "end_date") <= tuple(self._query):
            activated_on_range[0] = self._query["start_date"]
            activated_on_range[1] = self._query["end_date"]
            filter_by["activated_on__range"] = activated_on_range
        return Convo.objects.filter(user=self.user, **filter_by).order_by(
            "-activated_on"
        )

    def filter_public_convos(self):
        filter_by = {}
        for filter in ("name", "description"):
            if filter in self._query:
                filter_by[filter + "__contains"] = self._query[filter]
        activated_on_range = [None, None]
        if ("start_date", "end_date") <= tuple(self._query):
            activated_on_range[0] = self._query["start_date"]
            activated_on_range[1] = self._query["end_date"]
            filter_by["activated_on__range"] = activated_on_range
        return Convo.objects.filter(is_public=True, **filter_by).order_by(
            "-activated_on"
        )

    def filter_convos(self):
        convos = set(super().filter_convos())
        filter_by = {}
        for filter in self.get_query_by_attributes():
            for arg, val in self._query.items():
                if filter.startswith(arg):
                    filter_by[filter] = val
                    break
        activated_on_range = [None, None]
        if ("start_date", "end_date") <= tuple(self._query):
            activated_on_range[0] = self.convert_str_date_to_datetime(
                self._query["start_date"]
            )
            activated_on_range[1] = self.convert_str_date_to_datetime(
                self._query["end_date"]
            )
            filter_by["activated_on__range"] = activated_on_range
        for c in Convo.objects.filter(is_public=True, **filter_by):
            convos.add(c)
        return convos


class DashboardConvoQuery(ConvoQuery):
    def get_query_result(self):
        if not self._query:
            convos = set(Convo.objects.filter(user__id=self._user_id))
            for c in Convo.objects.filter(is_public=True):
                convos.add(c)
            return convos
        if (
            "action" in self._query
            and "entries" in self._query
            and len(self._query["entries"])
        ):
            return self.apply_action()
        return Convo.objects.filter(user=self._user)

    def search_convos(self):
        """used to return Convo list when queried via search bar hence self._query contains `search` key"""
        search_by = self.get_query_by_attributes()
        search_value = self._query[self.query_arguments["search_convo"]]
        convos = super().search_convos()
        for item in search_by:
            for c in Convo.objects.filter(user=self._user, **{item: search_value}):
                convos.add(c)
        return convos

    def filter_convos(self):
        convos = set(super().filter_convos())
        filter_by = {}
        for filter in self.get_query_by_attributes():
            for arg, val in self._query.items():
                if filter.startswith(arg):
                    filter_by[filter] = val
                    break
        activated_on_range = [None, None]
        if ("start_date", "end_date") <= tuple(self._query):
            activated_on_range[0] = self.convert_str_date_to_datetime(
                self._query["start_date"]
            )
            activated_on_range[1] = self.convert_str_date_to_datetime(
                self._query["end_date"]
            )
            filter_by["activated_on__range"] = activated_on_range
        for c in Convo.objects.filter(user=self._user, **filter_by):
            convos.add(c)
        return convos

    def apply_action(self):
        action_handler = ConvoAction()
        action_handler.apply_action(
            self._query["action"], self._user, self._query["entries"]
        )
        return Convo.objects.filter(user=self._user)


def set_publisher_to_convos(user_id, convos):
    for c in convos:
        c.publisher = c.user.username
        if c.user.id == user_id:
            c.publisher = "You"
    return convos


def get_convo(user):
    convo = Convo.objects.filter(user=user).order_by("-activated_on").first()
    if not convo:
        default_user = User.objects.get(
            username=os.environ.get("DJANGO_DEFAULT_USERNAME")
        )
        convo = (
            Convo.objects.filter(user=default_user).order_by("-activated_on").first()
        )
    return convo
