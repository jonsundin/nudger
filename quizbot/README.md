All endpoints starting from '/quiz' are handled here. There are 2 main goals of this application:

1. give user an chance to upload his own yaml file which is transformed to conversation he can participate in opening a bot widget residing on a page with '/quiz' url.
2. see the list of uploaded yaml files (can be treated as separate conversations in bot) and by desire change active conversation.

Here is the instruction of how to use it all (we suppose you run it locally, production version has the same flow):

1. Open your browser
2. Go to http://localhost:8000/quiz/upload endpoint
3. Upload your own yaml file
4. You have been redirected to http://localhost:8000/quiz/documents/ endpoint where you see all uploaded files. It should look like this (number of documents may differ): quizbot-document-list.png
5. Next to each document you see 'Create or Update' button. Press it whenever you want to populate/switch active conversation in bot widget.
6. Follow http://localhost:8000/quiz link. At the right bottom corner you see a circle with picture inside. Press it and you will see rectangle chat area appeared you can start chatting to

This application suppose you to use yaml files to populate conversation in chat bot widget:
You can see examples of such files following next links:
https://gitlab.com/tangibleai/community/nudger/-/blob/main/data/countByOne_0003.yml
https://gitlab.com/tangibleai/community/nudger/-/blob/main/data/DevByTwo_0002.yml

How to create yaml file to try it out? Here are steps with concrete example provided to build minimum viable conversation:

1. First of all, every yaml file in our system can be splitted to multiple items (let's call each of them state) starting with "-" symbol and having multiple `key: value` pairs. Just keep in mind each state must have 2 keywords present - name and triggers.
2. Let's create our first state. Start typing `- name: `. By following this syntax you inform the system that you want to create a state. `name` keyword is responsible for naming your state, it may not be that meaningful, so don't worry if nothing creative came to mind by now, just give it `q1` value. So now you should have next yaml file content:

```
- name: q1
```

3. After that let's add second required argument called `triggers`. It's responsible for redirecting user to next state especially when we want him to give some answer that triggers that redirect (that's not required as you will see later). Currently system supports only English so the value should be `en: <list of triggers>` where `<list of triggers>` is set of key-value pairs of triggers (key is answer user should give to go to state specified by value). Let's consider example:

```
triggers:
  en:
    1: correct-answer-q1
    one: correct-answer-q1
```

As you can see after `en:` I specified pairs `1: correct-answer-q1` and `one: correct-answer-q1` which means that user can put answer "1" as well as "one" to go to state called "correct-answer-q1". Also very important detail is to provide a handler for wrong answer (when user didn't answer "1" or "one"). For that case we have to use `__default__: <wrong-answer-state>` syntax where `<wrong-asnwer-state>` is the name of state we move to in case of wrong answer. So up to now the complete yaml file should take a look:

```
- name: q1
  triggers:
    en:
      1: correct-answer-q1
      one: correct-answer-q1
      __default__: wrong-answer-q1
```

4. As this is our first state we can assign it 1 level. Later we will increase this number by one when the difficulty increases accordingly:

```
- name: q1
  convo_name: my first convo
  convo_description: This is demo yaml file to learn how to create ones in the future
  level: 1
  triggers:
    en:
      1: number-one-state
      one: number-one-state
      __default__: wrong-answer-q1
```

5. And finally to complete our state we should add a question user should answer. We specify it using `actions` keyword, here we also put `en` key and start our question with "-" sign:

```
- name: q1
  convo_name: my first convo
  convo_description: This is demo yaml file to learn how to create ones in the future
  level: 1
  actions:
    en:
      - I we count up by 1 what number would be next after 8, 9, 10?
  triggers:
    en:
      11: number-one-state
      eleven: number-one-state
      __default__: wrong-answer-q1
```

6. Last _optional_ thing is called `buttons`. It gives user an ability to click a button and go to some state. For example, putting this

```
buttons:
  en:
    OK: q1
```

to your yaml file will let user click a button with text "OK" and go to state `q1` (which actually will just print question again, but in place of `q1` there could be another name which would transfer user to some other state). Final yaml file content:

```
- name: q1
  convo_name: my first convo
  convo_description: This is demo yaml file to learn how to create ones in the future
  level: 1
  actions:
    en:
      - I we count up by 1 what number would be next after 8, 9, 10?
  buttons:
    en:
      OK: q1
  triggers:
    en:
      11: number-one-state
      eleven: number-one-state
      __default__: wrong-answer-q1
```

Take a look, we just defined a state. But unfortunately it's not comprehensive to make conversation alive and describe all syntax available. So, let's add some other ones. If you were careful we asked a math question in the example above which is not very user-friendly way to start a conversation, so it's better to create some introductory state in which we greet a user and put some welcome message. The process is similar except of little details:
**Only for the very first state** we should provide `convo_name` and `convo_description` parameters which match our conversation name and description:

```
- name: start
  convo_name: my first convo
  convo_description: This is our first conversation made with yaml file
  level: 0
  actions:
    en:
      - This is the very first message user will see clicking bot widget icon 😄. Here we can introduce ourselves, welcome user or just wish him a luck
  triggers:
    en:
      __next__: q1
```

As you see this state is very similar to that one we've already defined, but there are slight differences. Only in the last one we specified `convo_name` and `convo_description` parameters, set level to be 0 and the most important part we encountered new `__next__` keyword in triggers. What does it mean? It means that getting into this state we should print user a text from `actions` and immediately go to state `q1`. Now the whole yaml file should take a look of:

```
- name: start
  convo_name: my first convo
  convo_description: This is our first conversation made with yaml file
  level: 0
  actions:
    en:
      - This is the very first message user will see clicking bot widget icon 😄. Here we can introduce ourselves, welcome user or just wish a luck
  triggers:
    en:
      __next__: q1
- name: q1
  convo_name: my first convo
  convo_description: This is demo yaml file to learn how to create ones in the future
  level: 1
  actions:
    en:
      - If we count up by 1 what number would be next after 8, 9, 10?
  buttons:
    en:
      OK: q1
  triggers:
    en:
        11: correct-answer-q1
        eleven: correct-answer-q1
        __default__: wrong-answer-q1
```

Great! So up to this moment we created 2 states. Let's define a bunch more to handle right and wrong user answers.
If user answered "11" or "elevent" our first questing he should move to `correct-answer-q1` state. Let's define it:

```
- name: correct-answer-q1
  actions:
    en:
      - Exactly right!
  triggers:
    en:
      "__next__": q2
```

As you see the main goal of this state is just to print message `Exactly right!` and switch to `q2` state (not defined yet).
The definition of `wrong-answer-q1` state:

```
- name: wrong-answer-q1
  actions:
    en:
      - Oops!
  triggers:
    en:
      "__next__": q1
```

Lets compose it all together. At the end I will add `q2` state (now you should be able to understand it):

```
- name: start
  convo_name: my first convo
  convo_description: This is our first conversation made with yaml file
  level: 0
  actions:
    en:
      - This is the very first message user will see clicking bot widget icon 😄. Here we can introduce ourselves, welcome user or just wish a luck
  triggers:
    en:
      __next__: q1
- name: q1
  convo_name: my first convo
  convo_description: This is demo yaml file to learn how to create ones in the future
  level: 1
  actions:
    en:
      - If we count up by 1 what number would be next after 8, 9, 10?
  buttons:
    en:
      OK: q1
  triggers:
    en:
        11: correct-answer-q1
        eleven: correct-answer-q1
        __default__: wrong-answer-q1
- name: correct-answer-q1
  actions:
    en:
      - Exactly right!
  triggers:
    en:
      "__next__": q2
- name: wrong-answer-q1
  actions:
    en:
      - Oops!
  triggers:
    en:
      "__next__": q1
- name: q2
  level: 2
  actions:
    en:
      - 16, 17, 18
  buttons:
    en:
      OK: q2
  triggers:
    en:
      19: correct-answer-q2
      "__default__": wrong-answer-q2
```

Here is the graph showing how our conversation works now:
image

By adding more and more states you will make conversation endless and endless. So keep it going and create the most engaging one
