# Generated by Django 4.1.3 on 2023-06-26 07:22

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import uuid


class Migration(migrations.Migration):
    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name="Checkpoint",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("created_on", models.DateTimeField(auto_now_add=True)),
                (
                    "description",
                    models.CharField(blank=True, max_length=100, null=True),
                ),
                ("published", models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name="ConversationTracker",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("student_id", models.CharField(max_length=100)),
                ("state_name", models.CharField(blank=True, max_length=100, null=True)),
                ("updated_on", models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name="State",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "state_name",
                    models.CharField(
                        help_text="The title the system uses to recognize the state",
                        max_length=255,
                    ),
                ),
                (
                    "level",
                    models.IntegerField(
                        help_text="An optional field that tells the depth of the action",
                        null=True,
                    ),
                ),
                ("update_args", models.JSONField(null=True)),
                ("update_kwargs", models.JSONField(null=True)),
                (
                    "update_context",
                    models.JSONField(
                        help_text="May hold information such as gender, location, or history of previous exchanges",
                        null=True,
                    ),
                ),
                (
                    "checkpoint",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        to="quizbot.checkpoint",
                    ),
                ),
            ],
        ),
        migrations.CreateModel(
            name="StateManager",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
            ],
        ),
        migrations.CreateModel(
            name="Trigger",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "intent_text",
                    models.CharField(
                        help_text="The user utterance that triggers a transition to a new bot state.",
                        max_length=255,
                    ),
                ),
                ("update_args", models.JSONField(null=True)),
                ("update_kwargs", models.JSONField(null=True)),
                (
                    "lang",
                    models.CharField(
                        choices=[
                            ("en", "en"),
                            ("es", "es"),
                            ("zh-hans", "zh-hans"),
                            ("zh-hant", "zh-hant"),
                        ],
                        default="en",
                        max_length=100,
                    ),
                ),
                ("is_button", models.BooleanField(default=False)),
                (
                    "from_state",
                    models.ForeignKey(
                        blank=True,
                        null=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="rel_from_state",
                        to="quizbot.state",
                    ),
                ),
                (
                    "to_state",
                    models.ForeignKey(
                        blank=True,
                        null=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="rel_to_state",
                        to="quizbot.state",
                    ),
                ),
            ],
        ),
        migrations.AddField(
            model_name="state",
            name="next_states",
            field=models.ManyToManyField(through="quizbot.Trigger", to="quizbot.state"),
        ),
        migrations.CreateModel(
            name="Message",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("sequence_number", models.IntegerField()),
                (
                    "bot_text",
                    models.TextField(
                        help_text="The message of the state written in English"
                    ),
                ),
                (
                    "lang",
                    models.CharField(
                        choices=[
                            ("en", "en"),
                            ("es", "es"),
                            ("zh-hans", "zh-hans"),
                            ("zh-hant", "zh-hant"),
                        ],
                        default="en",
                        max_length=100,
                    ),
                ),
                (
                    "state",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE, to="quizbot.state"
                    ),
                ),
            ],
        ),
        migrations.CreateModel(
            name="InteractionData",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "message_id",
                    models.UUIDField(
                        default=uuid.uuid4,
                        help_text="A unique id assigned to each message that is sent out",
                        unique=True,
                    ),
                ),
                (
                    "message_direction",
                    models.CharField(
                        choices=[
                            ("inbound", "inbound"),
                            ("outbound", "outbound"),
                            ("unknown", "unknown"),
                        ],
                        default="en",
                        max_length=100,
                    ),
                ),
                (
                    "message_text",
                    models.TextField(help_text="The message of the state"),
                ),
                (
                    "send_time",
                    models.DateTimeField(
                        auto_now_add=True,
                        help_text="The time that the message was sent (as opposed to scheduled)",
                    ),
                ),
                (
                    "sender_type",
                    models.CharField(
                        choices=[
                            ("user", "user"),
                            ("chatbot", "chatbot"),
                            ("human agent", "human agent"),
                        ],
                        max_length=20,
                    ),
                ),
                (
                    "user_id",
                    models.CharField(
                        help_text="A student or user ID that can be mapped to the User.pk OR an anonymous ID that is unique for a single session (room)",
                        max_length=255,
                        null=True,
                    ),
                ),
                (
                    "bot_id",
                    models.CharField(
                        default="Poly Chatbot",
                        help_text="The name or id of the chatbot being interacted with",
                        max_length=255,
                    ),
                ),
                (
                    "channel",
                    models.CharField(
                        default="WSNYC Website",
                        help_text="The platform the user interacted with, such as WSNYC website",
                        max_length=128,
                    ),
                ),
                (
                    "state",
                    models.ForeignKey(
                        help_text="The state of the chatbot when a message is sent",
                        max_length=255,
                        null=True,
                        on_delete=django.db.models.deletion.SET_NULL,
                        to="quizbot.state",
                    ),
                ),
            ],
        ),
        migrations.CreateModel(
            name="Document",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("file", models.FileField(upload_to="documents/")),
                ("convo_name", models.CharField(blank=True, max_length=255)),
                ("description", models.CharField(blank=True, max_length=255)),
                ("created_on", models.DateTimeField(auto_now_add=True)),
                (
                    "user",
                    models.ForeignKey(
                        default=None,
                        on_delete=django.db.models.deletion.CASCADE,
                        to=settings.AUTH_USER_MODEL,
                    ),
                ),
            ],
        ),
        migrations.CreateModel(
            name="Convo",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("name", models.CharField(default="default", max_length=100)),
                ("description", models.CharField(default="default", max_length=100)),
                (
                    "user",
                    models.ForeignKey(
                        default=None,
                        on_delete=django.db.models.deletion.CASCADE,
                        to=settings.AUTH_USER_MODEL,
                    ),
                ),
            ],
        ),
        migrations.AddField(
            model_name="checkpoint",
            name="convo",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE, to="quizbot.convo"
            ),
        ),
        migrations.AddField(
            model_name="checkpoint",
            name="user",
            field=models.ForeignKey(
                default=None,
                on_delete=django.db.models.deletion.CASCADE,
                to=settings.AUTH_USER_MODEL,
            ),
        ),
        migrations.AddConstraint(
            model_name="convo",
            constraint=models.UniqueConstraint(
                fields=("user", "name"), name="composite_key"
            ),
        ),
    ]
