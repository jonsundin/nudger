from django.contrib.auth.models import User
from django.utils import timezone

from .models import UserSession


def delete_expired_sessions_and_relations(sender, instance=None, **kwargs):
    if isinstance(instance, UserSession):
        for user in User.objects.all():
            user_sessions = UserSession.objects.filter(user=user)
            expired_user_sessions = user_sessions.filter(
                session__expire_date__lt=timezone.now()
            )
            all_sessions_expired = len(user_sessions) == len(expired_user_sessions)
            for session in expired_user_sessions:
                session.session.delete()
                session.delete()
            if all_sessions_expired and not user.password:
                user.delete()
