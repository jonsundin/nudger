from django.contrib import admin

from rangefilter.filters import DateRangeFilterBuilder

from .filters.convo import ConvoNameFilter, ConvoDescriptionFilter

from .models import State, Message, Trigger, Convo, ConversationTracker

admin.site.register(State)
admin.site.register(Message)
admin.site.register(Trigger)
admin.site.register(ConversationTracker)


class ConvoAdmin(admin.ModelAdmin):
    search_fields = ["id", "name", "description", "user__username"]
    list_display = ("id", "name", "description", "user", "activated_on")
    list_filter = [
        ConvoNameFilter,
        ConvoDescriptionFilter,
        ("activated_on", DateRangeFilterBuilder("By activated on")),
    ]


admin.site.register(Convo, ConvoAdmin)
