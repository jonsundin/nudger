import io
import json
import os
import requests
import uuid
import yaml
from pathlib import Path

from datetime import datetime
from django.contrib import messages
from django.contrib.auth.decorators import user_passes_test
from django.contrib.auth.models import AnonymousUser, User
from django.core.files import File
from django.http import FileResponse
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.utils.html import format_html
from django.utils.safestring import mark_safe
from rest_framework.response import Response
from rest_framework.views import APIView

from nudger import settings
from quizbot.convograph_validator.convograph_validator_errors import (
    ConvoGraphValidationError,
)
from quizbot.convograph_validator.v3.convograph_validator import ConvoGraphValidator
from quizbot.spreadsheet_linter import ConvoSpreadsheetValidationError
from quizbot.forms import ConvoForm, ConvoSpreadsheetForm
from quizbot.load_yaml_v3 import convoyaml_to_convomodel
from quizbot.spreadsheet_to_db import upload_spreadsheet_to_db
from quizbot.models import Convo, State
from users.helpers import digitalocean

ENV = settings.ENV


def room(request, room_name):
    file_css = "polyface/polyface.css"
    file_js = "polyface/polyface.js"
    host = ENV.get("WEBSOCKET_URL")
    url = "/".join([host, "ws/quiz"])
    print("quizbot:", url)

    return render(
        request,
        "quizbot/mathbot.html",
        {
            "file_css": file_css,
            "file_js": file_js,
            "welcome_popup_on": json.dumps(False),
            "empty_message_allowed": json.dumps(False),
            "name": "Qary",
            "bot_picture": request.user.profile.bot_widget_image.url,
            "ws_url": url,
            "fetch_image_endpoint_url": "{host}/quiz/bot_widget_icon",
            "is_s3": json.dumps(str(settings.USE_S3) in (True, "True", "true")),
        },
    )


def quiz(request):
    room_name = uuid.uuid4().hex
    return redirect("quizbot-room", room_name)


class APINextStateReply(APIView):
    """Returns the next state based on the input

    Uses the State.get_next_state_from_database in models.py to find the next state information

    Use with /quiz/api

    JSON Input Example
    {
        'state_name': 'select-language',
        'user_text': 'English',
        'context': {'lang': 'en'}
    }

    Query Parameter Example
    /quiz/api/?state_name=selected-language-welcome&user_text=Ready
    """

    def get(self, request):
        data = request.data
        data.update(request.query_params.dict())
        time = datetime.now()
        message_id = uuid.uuid4()

        response = State.get_next_state_from_database(data)

        return Response(
            {
                "state": response["state"],
                "messages": response["messages"],
                "triggers": response["triggers"],
                "context": response["context"],
                "message_id": message_id,
                "message_direction": "outbound",
                "sender_type": "chatbot",
                "user_id": "chat_j2Lk356",
                "bot_id": "Poly Chatbot",
                "channel": "WSNYC Website",
                "send_time": time,
            }
        )


def is_registered_user(user):
    """Checks whether a logged in user is actually registered or not"""
    if not user.email and not user.password:
        return False
    return True


@user_passes_test(is_registered_user)
def upload(request):
    """FIXME Sarah: create doctest and/or tests/test_convograph_validator.py"""
    if request.method == "POST":
        form = ConvoForm(request.POST, request.FILES)
        if form.is_valid():
            try:
                raw_content = request.FILES["file"].read()
                file_content = yaml.safe_load(raw_content)
                ConvoGraphValidator(file_content).run()
                convo_name = file_content[0].get("convo_name")
                convo_description = file_content[0].get("convo_description")
                convoyaml_to_convomodel(
                    graph=request.FILES["file"],
                    user=request.user,
                    convo_name=convo_name,
                    convo_description=convo_description,
                )
                messages.info(
                    request, "New conversation has been created and activated!"
                )
                return redirect("dashboard")
            except ConvoGraphValidationError as exc:
                error_msg = f"{str(exc)} "
            except Exception as e:
                error_msg = 'Wrong syntax! File content should contain multiple entries starting from "-" character. Most likely you forgot to put leading "-" before one of states. '

            error_msg += "Use the format in the "
            messages.info(
                request,
                format_html(
                    "{} {}",
                    error_msg,
                    mark_safe(
                        '<b><a href="https://gitlab.com/tangibleai/nudger/-/blob/main/data/countByOne_0001.yml">link</a></b>'
                    ),
                ),
            )
            return redirect("dashboard")

    form = ConvoForm()
    return render(request, "quizbot/upload.html", {"form": form})


def convert_file_to_django_file_class(file_input, sheet_id_input, request):
    file_type = "csv"
    if file_input:
        file_content = request.FILES["file"].read()
        file_name = request.FILES["file"].name

        if file_name.endswith(".xlsx"):
            file_type = "xlsx"
    elif sheet_id_input:
        sheet_link = (
            f"https://docs.google.com/spreadsheets/d/{sheet_id_input}/export?format=csv"
        )
        sheet_file = requests.get(sheet_link)
        file_content = sheet_file.content
        file_name = "spreadsheet.csv"

    file = File(io.BytesIO(file_content), name=file_name)
    return file, file_type


@user_passes_test(is_registered_user)
def csv_upload(request):
    if request.method == "POST":
        form = ConvoSpreadsheetForm(request.POST, request.FILES)

        if form.is_valid():
            convo_name = form.cleaned_data["convo_name"]
            convo_description = form.cleaned_data["convo_description"]
            file = form.cleaned_data["file"]
            sheet_id = form.cleaned_data["sheet_id"]

            file, file_type = convert_file_to_django_file_class(file, sheet_id, request)
            try:
                upload_spreadsheet_to_db(
                    request.user, file, file_type, convo_name, convo_description
                )
                messages.info(
                    request, "New conversation has been created and activated!"
                )
                return redirect("bothub")
            except ConvoSpreadsheetValidationError as exc:
                error_msg = f"{str(exc)} "
                form.add_error(None, error_msg)

        else:
            form = ConvoSpreadsheetForm(form.data)

    else:
        form = ConvoSpreadsheetForm()

    return render(request, "quizbot/upload.html", {"form": form})


def add_links_to_public_convos(url_host, url_scheme, url_full_path, convos):
    authority = url_scheme + url_host
    full_path_parts = [p for p in url_full_path.split("/") if p not in ("/", "")]

    if full_path_parts[-1].startswith("?"):
        del full_path_parts[-1]
    for c in convos:
        if c.is_public:
            full_path = f'/{"/".join(full_path_parts)}/{c.id}'
            c.public_url = authority + full_path
    return convos


def get_bot_widget_icon(request):
    widget_icon_relative_path = request.GET.get("widget_icon_relative_path")[1:]

    if settings.USE_S3:
        image_url = digitalocean.get_private_image_url(widget_icon_relative_path)
        response = requests.get(image_url)
        image_data = response.content
        content_type = response.headers.get("Content-Type", "application/octet-stream")
        return HttpResponse(image_data, content_type=content_type)
    elif Path(widget_icon_relative_path).is_file():
        opened_file = open(widget_icon_relative_path, "rb")
        response = FileResponse(opened_file)
        return response
    else:
        return HttpResponse(status=404)


def handle_public_convo(request, convo_id):
    file_css = "polyface/polyface.css"
    file_js = "polyface/polyface.js"
    url = "/".join(
        [ENV.get("WEBSOCKET_URL"), "ws/quiz", request.user.username, str(convo_id)]
    )
    public_convo = Convo.objects.get(id=convo_id)
    user = request.user
    if isinstance(user, AnonymousUser):
        user = User.objects.get(username=ENV.get("DJANGO_DEFAULT_USERNAME"))

    return render(
        request,
        "quizbot/mathbot.html",
        {
            "file_css": file_css,
            "file_js": file_js,
            "convo_name": public_convo.name,
            "convo_description": public_convo.description,
            "welcome_popup_on": json.dumps(False),
            "empty_message_allowed": json.dumps(False),
            "name": "Qary",
            "bot_picture": user.profile.bot_widget_image.url,
            "fetch_image_endpoint_url": "http://localhost:8000/quiz/bot_widget_icon",
            "ws_url": url,
            "is_s3": json.dumps(str(settings.USE_S3) in (True, "True", "true")),
        },
    )


def bothub(request):
    file_css = "polyface/polyface.css"
    file_js = "polyface/polyface.js"

    return render(
        request,
        "quizbot/bothub.html",
        {"file_css": file_css, "file_js": file_js},
    )


def dashboard(request):
    file_css = "polyface/polyface.css"
    file_js = "polyface/polyface.js"
    active_convo = (
        Convo.objects.filter(user=request.user).order_by("-activated_on").first()
    ) or Convo.objects.get(
        user=User.objects.get(username=ENV.get("DJANGO_DEFAULT_USERNAME"))
    )
    url = ENV.get("WEBSOCKET_URL") + f"/ws/quiz/{request.user}/{active_convo.id}"
    try:
        widget_pic = request.user.profile.bot_widget_image.url
    except AttributeError:
        widget_pic = User.objects.get(username=ENV.get("DJANGO_DEFAULT_USERNAME"))

    return render(
        request,
        "quizbot/dashboard.html",
        {
            "file_css": file_css,
            "file_js": file_js,
            "welcome_popup_on": json.dumps(False),
            "empty_message_allowed": json.dumps(False),
            "name": "Qary",
            "bot_picture": widget_pic,
            "fetch_image_endpoint_url": "http://localhost:8000/quiz/bot_widget_icon",
            "ws_url": url,
            "is_s3": json.dumps(str(settings.USE_S3) in (True, "True", "true")),
        },
    )
