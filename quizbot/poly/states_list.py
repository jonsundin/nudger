def preprocessing():
    data = [
        (
            "select-language",
            {
                "name": "select-language",
                "level": 0,
                "actions": {
                    "en": "Hello, my name is POLY - short for Polyglot, which means I can speak many languages! 🌟\n\nPlease select which language you would like me to help you in:\n"
                },
                "buttons": {
                    "en": {
                        "English": "selected-language-welcome",
                        "Spanish (Español)": "selected-language-welcome",
                        "Chinese - Simplified (简体中文)": "selected-language-welcome",
                        "Chinese - Traditional (繁體中文)": "selected-language-welcome",
                    }
                },
                "triggers": {
                    "en": {
                        "": "selected-language-welcome",
                        "English": "selected-language-welcome",
                        "british": "selected-language-welcome",
                        "us": "selected-language-welcome",
                        "en": "selected-language-welcome",
                        "american": "selected-language-welcome",
                        "Spanish (Español)": "selected-language-welcome",
                        "Spanish": "selected-language-welcome",
                        "mexican": "selected-language-welcome",
                        "spain": "selected-language-welcome",
                        "Español": "selected-language-welcome",
                        "Chinese - Simplified (简体中文)": "selected-chinese-simplified",
                        "simplified chinese": "selected-chinese-simplified",
                        "Chinese": "selected-chinese-simplified",
                        "简体中文": "selected-chinese-simplified",
                        "ch": "selected-chinese-simplified",
                        "zh": "selected-chinese-simplified",
                        "Chinese - Traditional (繁體中文)": "selected-chinese-traditional",
                        "traditional chinese": "selected-chinese-traditional",
                        "繁體中文": "selected-chinese-traditional",
                        "english": "selected-language-welcome",
                        "spanish (español)": "selected-language-welcome",
                        "spanish": "selected-language-welcome",
                        "español": "selected-language-welcome",
                        "chinese - simplified (简体中文)": "selected-chinese-simplified",
                        "chinese": "selected-chinese-simplified",
                        "chinese - traditional (繁體中文)": "selected-chinese-traditional",
                    }
                },
                "on_answer": ["extract_lang"],
                "actions_with_buttons": {
                    "en": "Hello, my name is POLY - short for Polyglot, which means I can speak many languages! 🌟\n\nPlease select which language you would like me to help you in:\n\n  > English\n  > Spanish (Español)\n  > Chinese - Simplified (简体中文)\n  > Chinese - Traditional (繁體中文)"
                },
                "iloc": 0,
            },
        ),
        (
            "selected-language-welcome",
            {
                "name": "selected-language-welcome",
                "level": 1,
                "actions": {
                    "en": "I'm part of the Let's Talk NYC team and I'm here to help you learn about our resources and services.\n\nBefore we start, can I ask you a few questions? Select \"Yes\" when ready\n",
                    "zh": '我是"我们聊聊"纽约市团队的一员，我来这里是为了帮助您了解我们的资源和服务。\n\n在我们开始之前，我能问您几个问题吗？准备就绪后请选择“是”\n\n> 是\n',
                },
                "buttons": {
                    "zh": {"是": "is-this-your-first"},
                    "en": {"yes": "is-this-your-first"},
                },
                "triggers": {
                    "zh": {"是": "is-this-your-first"},
                    "en": {"yes": "is-this-your-first"},
                },
                "actions_with_buttons": {
                    "zh": '我是"我们聊聊"纽约市团队的一员，我来这里是为了帮助您了解我们的资源和服务。\n\n在我们开始之前，我能问您几个问题吗？准备就绪后请选择“是”\n\n> 是\n\n  > 是',
                    "en": "I'm part of the Let's Talk NYC team and I'm here to help you learn about our resources and services.\n\nBefore we start, can I ask you a few questions? Select \"Yes\" when ready\n\n  > yes",
                },
                "iloc": 1,
            },
        ),
        (
            "is-this-your-first-time",
            {
                "name": "is-this-your-first-time",
                "level": 2,
                "actions": {
                    "en": "Is this your first time talking to me?",
                    "zh": "这是您第一次和我说话吗？",
                },
                "triggers": {
                    "en": {
                        "yes": "is-this-your-first-yes",
                        "no": "is-this-your-first-no",
                    },
                    "zh": {"是": "is-this-your-first-yes", "否": "is-this-your-first-no"},
                },
                "actions_with_buttons": {},
                "iloc": 2,
            },
        ),
        (
            "is-this-your-first-yes",
            {
                "name": "is-this-your-first-yes",
                "level": 3,
                "actions": {
                    "en": "Which of the following do you represent?\n",
                    "zh": "欢迎来到我们网站！\n您代表下列哪一个？\n",
                },
                "triggers": {
                    "zh": {
                        "WSNYC学生": "wsnyc-students",
                        "WSNYC志愿者+教育工作者": "wsnyc-volunteer",
                        "普通网站访问者": "regular-website-visitor",
                        "wsnyc学生": "wsnyc-students",
                        "wsnyc志愿者+教育工作者": "wsnyc-volunteer",
                    },
                    "en": {
                        "WSNYC Student": "wsnyc-students",
                        "WSNYC Volunteer + Educator": "wsnyc-volunteer",
                        "General Website Visitor": "regular-website-visitor",
                        "wsnyc student": "wsnyc-students",
                        "wsnyc volunteer + educator": "wsnyc-volunteer",
                        "general website visitor": "regular-website-visitor",
                    },
                },
                "actions_with_buttons": {},
                "iloc": 3,
            },
        ),
        (
            "is-this-your-first-no",
            {
                "name": "is-this-your-first-no",
                "level": 3,
                "actions": {"zh": "您代表下列哪一个？"},
                "triggers": {
                    "zh": {
                        "WSNYC学生": "wsnyc-students",
                        "WSNYC志愿者+教育工作者": "wsnyc-volunteer",
                        "普通网站访问者": "regular-website-visitor",
                        "wsnyc学生": "wsnyc-students",
                        "wsnyc志愿者+教育工作者": "wsnyc-volunteer",
                    },
                    "en": {
                        "WSNYC students": "wsnyc-students",
                        "WSNYC Volunteer + Educator": "wsnyc-volunteer",
                        "regular website visitor": "regular-website-visitor",
                        "wsnyc students": "wsnyc-students",
                        "wsnyc volunteer + educator": "wsnyc-volunteer",
                    },
                },
                "actions_with_buttons": {},
                "iloc": 4,
            },
        ),
        (
            "wsnyc-students",
            {
                "name": "wsnyc-students",
                "level": 4,
                "actions": {"en": ""},
                "actions_with_buttons": {},
                "iloc": 5,
            },
        ),
        (
            "wsnyc-volunteer",
            {
                "name": "wsnyc-volunteer",
                "level": 4,
                "actions": {"en": ""},
                "actions_with_buttons": {},
                "iloc": 6,
            },
        ),
        (
            "regular-website-visitor",
            {
                "name": "regular-website-visitor",
                "level": 4,
                "actions": {"zh": "有什么可以帮到您？", "en": "Can I help you?"},
                "triggers": {
                    "zh": {
                        "学习英语": "learn-english",
                        "加入课程": "signup-for-course",
                        "连接到我们的资源": "connect-to-resource",
                    },
                    "en": {
                        "learning English": "learn-english",
                        "join a course": "signup-for-course",
                        "Connect to our resource": "connect-to-resource",
                        "learning english": "learn-english",
                        "connect to our resource": "connect-to-resource",
                    },
                },
                "actions_with_buttons": {},
                "iloc": 7,
            },
        ),
        (
            "learn-english",
            {
                "name": "learn-english",
                "level": 5,
                "actions": {},
                "actions_with_buttons": {},
                "iloc": 8,
            },
        ),
        (
            "signup-for-course",
            {
                "name": "signup-for-course",
                "level": 5,
                "actions": {},
                "actions_with_buttons": {},
                "iloc": 9,
            },
        ),
        (
            "connect-to-resource",
            {
                "name": "connect-to-resource",
                "level": 5,
                "actions": {
                    "zh": "有很多资源提供给您。如果您没有看到您要查找的内容，请点击“向我显示更多选项”。",
                    "en": "There are many resources available to you. If you don't see what you're looking for, click \"Show me more options.\"",
                },
                "triggers": {
                    "zh": {
                        "新冠检测+疫苗资源": "resource-crown-vaccine",
                        "仇恨犯罪+歧视资源": "resource-hate-crime",
                        "我的孩子可用的学校资源": "resource-school-child",
                        "工作培训": "resource-job-training",
                    },
                    "en": {
                        "New crown detection + vaccine resources": "resource-crown-vaccine",
                        "Hate Crime + Discrimination Resources": "resource-hate-crime",
                        "School resources available to my child": "resource-school-child",
                        "job training": "resource-job-training",
                        "new crown detection + vaccine resources": "resource-crown-vaccine",
                        "hate crime + discrimination resources": "resource-hate-crime",
                        "school resources available to my child": "resource-school-child",
                    },
                },
                "actions_with_buttons": {},
                "iloc": 10,
            },
        ),
        (
            "resource-crown-vaccine",
            {
                "name": "resource-crown-vaccine",
                "level": 6,
                "actions": {},
                "actions_with_buttons": {},
                "iloc": 11,
            },
        ),
    ]

    return data
