#!/usr/bin/env python

import yaml
from yaml.loader import SafeLoader


def extract_english(text):
    """
    Receives YAML file name
    Extracts English copy
    Returns a dictionary of English copy
    """

    with open(text, "r", encoding="utf8") as f:
        content = yaml.load(f, Loader=SafeLoader)

        for line in content:
            try:
                name = line["name"]
                actions_en = line["actions"]["en"]
                triggers_en = line["triggers"]["en"].keys()
                triggers_en_list = list(triggers_en)
            except KeyError:
                continue

            extracted_copy = {
                "name": name,
                "action_en": actions_en,
                "trigger_en": triggers_en_list,
            }
            # print(data)
            return extracted_copy


def find_next_state(text):
    """
    Receives YAML file name
    Extract current state and possible next states
    Returns a dictionary of current and next states
    """
    with open(text, "r", encoding="utf8") as f:
        content = yaml.load(f, Loader=SafeLoader)

        for line in content:
            try:
                state_name = line["name"]
                triggers_values = line["triggers"]["en"].values()
                triggers_values_list = list(triggers_values)
            except KeyError:
                continue

            states_dest = {
                "state_name": state_name,
                "dest_states": triggers_values_list,
            }
            # print(states_dest)
            return states_dest


extract_english("poly_manual_build.yml")
find_next_state("poly_manual_build.yml")
