from django.db import models


class State(models.Model):
    # Foreign key to itself
    next_states = models.ManyToManyField(
        "self", through="Trigger", through_fields=("from_state", "to_state")
    )

    state_name = models.CharField(
        null=False,
        max_length=255,
        unique=True,
        help_text="The title the system uses to recognize the state",
    )
    message_eng = models.TextField(
        null=False, help_text="The message of the state written in English"
    )
    update_args = models.JSONField(null=True)
    update_kwargs = models.JSONField(null=True)
    update_context = models.JSONField(
        null=True,
        help_text="May hold information such as gender, location, or history of previous exchanges",
    )
    level = models.IntegerField(
        null=True, help_text="An optional field that tells the depth of the action"
    )


class Trigger(models.Model):
    # Foreign Keys
    from_state = models.ForeignKey(
        State,
        related_name="rel_from_state",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
    )
    to_state = models.ForeignKey(
        State,
        related_name="rel_to_state",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
    )
    intent_message = models.CharField(
        null=False, max_length=255, help_text="The trigger message"
    )
    update_args = models.BinaryField(null=True)
    update_kwargs = models.BinaryField(null=True)
