import { getStaticData } from "./convographStaticData.js";

document.addEventListener("DOMContentLoaded", function () {
  async function drawGraph() {
    const d3 = window.d3;
    const convograph_div = document.getElementById("convograph");
    const convograph_rect = convograph_div.getBoundingClientRect();

    const origin_x = convograph_rect.left + window.scrollX;
    const origin_y = convograph_rect.top + window.scrollY;

    const width = d3.select("#convograph").node().clientWidth;
    const height = d3.select("#convograph").node().clientHeight;

    const screenHeight = window.screen.height;

    const svgDimensions = {
      width: width,
      height: height === 0 ? screenHeight : height,
      margin: 10,
    };

    const min_x = -(svgDimensions.width / 2);
    const min_y = -(svgDimensions.height / 2);

    const svg = d3
      .select("#convograph")
      .append("svg")
      .attr("version", "1.1")
      .attr("xmlns", "http://www.w3.org/2000/svg")
      .attr("width", svgDimensions.width)
      .attr("height", svgDimensions.height)
      .attr(
        "viewBox",
        `${min_x}, ${min_y}, ${svgDimensions.width}, ${svgDimensions.height}`
      );

    const container = svg
      .append("g")
      .attr("id", "container")
      .attr("transform", `scale(0.6)`);

    let chatBotData = null;
    chatBotData = await d3.json("/convoscript/graph_data/");

    if (
      chatBotData === null ||
      chatBotData === undefined ||
      chatBotData.length === 0
    ) {
      chatBotData = getStaticData();
    }

    const firstBotStateObject = chatBotData[0];
    let visited = new Set();
    visited.add(firstBotStateObject.name);

    let graph = {};

    // if d3.hierarchy is used, nestedNodes is the data structure that d3.hierarchy() expects
    const [nestedNodes, flatNodes, links] = createGraph(chatBotData);
    graph = { nodes: flatNodes, links: links };

    // The order of the groups determines which elements are on top in svg
    // The last group is on top
    const linkGroup = container.append("g").attr("id", "links");
    const nodeGroup = container.append("g").attr("id", "nodes");

    const nodeSelection = createD3NodeSelection(nodeGroup, graph.nodes);
    const circleSelection = nodeSelection.selectAll("circle");
    const textSelection = nodeSelection.selectAll("text");

    const [linkCurvedGroupSelection, linkStraightGroupSelection] =
      createD3LinkSelection(linkGroup, graph.links);
    const curvedPathSelection = d3.selectAll(".curved-path");
    const straightLineSelection = d3.selectAll(".line");

    /////////////////////////////////////////////////////////////////
    // Create simulation
    /////////////////////////////////////////////////////////////////

    const simulation = d3
      .forceSimulation(graph.nodes)
      .force(
        "link",
        d3
          .forceLink(graph.links)
          // the below d object is a node object
          // forceLink uses the node id to determine
          // which nodes are connected to each link
          .id((d) => d.id)
          .distance(100)
          .strength(0.5)
      )
      .force(
        "x",
        d3.forceX((d) => {
          return min_x + svgDimensions.width / 2;
        })
      )
      .force(
        "y",
        d3.forceY((d, i) => {
          if (d.name === "start") {
            return min_y + svgDimensions.height / 2;
          } else {
            return min_y + i * 100;
          }
        })
      )
      .force("collide", d3.forceCollide(65))
      .on("tick", tick);

    /////////////////////////////////////////////////////////////////
    // listeners for showing and hiding tooltip
    /////////////////////////////////////////////////////////////////

    const tooltip = d3.select("body").append("div").attr("id", "tooltip");

    const tooltipTitle = tooltip.append("h4").attr("id", "tooltip-title");
    const tooltipText = tooltip.append("p").attr("id", "tooltip-text");

    nodeSelection
      .on("mouseover", function (event, d) {
        const nodeHovered = d3.select(this);
        const tagId = nodeHovered.node().id;

        tooltip
          .style("display", "block")
          .style("left", event.pageX + "px")
          .style("top", event.pageY + "px");

        tooltipTitle.text(d.name);
        tooltipText.text(d.actions.en[0]);
      })
      .on("mouseout", function (event, d) {
        const nodeHovered = d3.select(this);
        const tagId = nodeHovered.node().id;
        const tag = nodeHovered.node().tagName;

        tooltip.style("display", "none");
      });

    linkCurvedGroupSelection
      .on("mouseover", function (event, d) {
        const linkHovered = d3.select(this);
        const tagId = linkHovered.node().id;

        tooltip
          .style("display", "block")
          .style("left", event.pageX + "px")
          .style("top", event.pageY + "px");

        tooltipTitle.text("");
        tooltipText.text(d.linkLabel);
      })
      .on("mouseout", function (event, d) {
        const linkHovered = d3.select(this);
        const tagId = linkHovered.node().id;
        const tag = linkHovered.node().tagName;

        tooltip.style("display", "none");
      });

    linkStraightGroupSelection
      .on("mouseover", function (event, d) {
        const linkHovered = d3.select(this);
        const tagId = linkHovered.node().id;

        tooltip
          .style("display", "block")
          .style("left", event.pageX + "px")
          .style("top", event.pageY + "px");

        tooltipTitle.text("");
        tooltipText.text(d.linkLabel);
      })
      .on("mouseout", function (event, d) {
        const linkHovered = d3.select(this);
        const tagId = linkHovered.node().id;

        tooltip.style("display", "none");
      });

    // Clicking and dragging nodes
    const drag = d3
      .drag()
      .on("start", dragstarted)
      .on("drag", dragged)
      .on("end", dragended);

    function dragstarted(event, d) {
      if (!event.active) simulation.alphaTarget(0.3).restart();
      d.fx = d.x;
      d.fy = d.y;
    }

    function dragged(event, d) {
      d.fx = event.x;
      d.fy = event.y;
    }

    function dragended(event, d) {
      if (!event.active) simulation.alphaTarget(0);
      d.fx = null;
      d.fy = null;
    }

    // Clicking and dragging nodes
    nodeSelection.call(drag).on("click", click);

    function click(event, d) {
      delete d.fx;
      delete d.fy;
      simulation.alpha().restart();
    }

    // Zooming and panning functionality
    svg.call(
      d3.zoom().on("zoom", function (event) {
        nodeGroup.attr("transform", event.transform);
        linkGroup.attr("transform", event.transform);
      })
    );

    // Simulation tick function
    function tick() {
      nodeSelection.attr("x", (d) => d.x).attr("y", (d) => d.y);
      circleSelection.attr("cx", (d) => d.x).attr("cy", (d) => d.y);
      textSelection.attr("x", (d) => d.x).attr("y", (d) => d.y + 5);

      linkCurvedGroupSelection.attr("d", (link) => {
        const x1 = link.source.x;
        const y1 = link.source.y;
        const x2 = link.target.x;
        const y2 = link.target.y;

        const dx = x2 - x1;
        const dy = y2 - y1;
        const dr = Math.sqrt(dx * dx + dy * dy);

        return `M ${x1} ${y1} A ${dr} ${dr} 0 0 1 ${x2} ${y2}`;
      });

      curvedPathSelection.attr("d", (link) => {
        const x1 = link.source.x;
        const y1 = link.source.y;
        const x2 = link.target.x;
        const y2 = link.target.y;

        const dx = x2 - x1;
        const dy = y2 - y1;
        const dr = Math.sqrt(dx * dx + dy * dy);

        return `M ${x1} ${y1} A ${dr} ${dr} 0 0 1 ${x2} ${y2}`;
      });

      linkStraightGroupSelection
        .attr("x1", (link) => link.source.x)
        .attr("y1", (link) => link.source.y)
        .attr("x2", (link) => link.target.x)
        .attr("y2", (link) => link.target.y);

      straightLineSelection
        .attr("x1", (link) => link.source.x)
        .attr("y1", (link) => link.source.y)
        .attr("x2", (link) => link.target.x)
        .attr("y2", (link) => link.target.y);
    }

    /////////////////////////////////////////////////////////////////
    // Helper functions to transform the data
    /////////////////////////////////////////////////////////////////

    function createGraph(chatBotData) {
      let statesToVisit = [];
      let visitedStates = new Set();
      let visitedLinks = new Set();
      let depth = 0;
      let breadth = 0;

      const root = chatBotData[0];

      const rootObj = {
        id: `node__${root.name}`,
        name: root.name,
        parent: null,
        children: [],
        depth: depth,
        breadth: breadth,
        ...root,
      };

      let nestedNodes = { name: rootObj.name, children: rootObj.children };
      let flatNodes = [rootObj];
      let links = [];

      statesToVisit.push(rootObj);
      visitedStates.add(rootObj.id);

      while (statesToVisit.length > 0) {
        let chatBotState = statesToVisit.shift();

        if (chatBotState.triggers === undefined) {
          continue;
        }

        breadth = 0;
        // first loop through the triggers to create nodes
        for (const [linkLabel, chatBotStateName] of Object.entries(
          chatBotState.triggers.en
        )) {
          const nextChatBotState = chatBotData.find(
            (botState) => botState.name === chatBotStateName
          );

          const nextChatBotStateId = `node__${nextChatBotState.name}`;

          if (
            chatBotState.children.find(
              (child) => child.id === nextChatBotStateId
            )
          ) {
            continue;
          }

          if (
            !visitedStates.has(nextChatBotStateId) &&
            linkLabel !== "__next__"
          ) {
            const nextChatBotStateObj = {
              id: nextChatBotStateId,
              name: nextChatBotState.name,
              parent: `node__${chatBotState.name}`,
              children: [],
              depth: chatBotState.depth + 1,
              breadth: breadth++,
              ...nextChatBotState,
            };
            chatBotState.children.push(nextChatBotStateObj);
            flatNodes.push(nextChatBotStateObj);
            visitedStates.add(nextChatBotStateObj.id);
            statesToVisit.push(nextChatBotStateObj);
          } else if (
            !visitedStates.has(nextChatBotStateId) &&
            linkLabel === "__next__"
          ) {
            const nextChatBotStateObj = {
              id: nextChatBotStateId,
              name: nextChatBotState.name,
              parent: `node__${chatBotState.name}`,
              children: [],
              depth: chatBotState.depth,
              breadth: breadth++,
              ...nextChatBotState,
            };
            chatBotState.children.push(nextChatBotStateObj);
            flatNodes.push(nextChatBotStateObj);
            visitedStates.add(nextChatBotStateObj.id);
            statesToVisit.push(nextChatBotStateObj);
          }
        }

        // loop through the triggers again to create links
        for (const [linkLabel, nextChatBotStateName] of Object.entries(
          chatBotState.triggers.en
        )) {
          if (chatBotState.name === nextChatBotStateName) continue;
          const nextLinkId = `link__${chatBotState.name}--${nextChatBotStateName}`;
          if (!visitedLinks.has(nextLinkId)) {
            const nextChatBotStateId = `node__${nextChatBotStateName}`;
            const nextLinkObj = {
              id: nextLinkId,
              source: chatBotState.id,
              target: nextChatBotStateId,
              linkLabel: linkLabel,
            };
            visitedLinks.add(nextLinkId);
            links.push(nextLinkObj);
          }
        }
      }
      return [nestedNodes, flatNodes, links];
    }

    /////////////////////////////////////////////////////////////////
    // Helper functions to create the nodes and links
    /////////////////////////////////////////////////////////////////

    function createD3LinkSelection(linkGroup, linkObjs) {
      let curvedLinkObjs = [];
      let straightLinkObjs = [];

      linkObjs.forEach((link) => {
        if (link.linkLabel === "__default__") {
          curvedLinkObjs.push(link);
        } else {
          straightLinkObjs.push(link);
        }
      });

      const linkCurvedGroupSelection = linkGroup
        .selectAll("g[class='link-curved-group']")
        .data(curvedLinkObjs)
        .join(
          (enter) => {
            const enterSelection = enter
              .append("g")
              .classed("link-curved-group", true);

            enterSelection
              .append("path")
              .attr("id", (d) => d.id)
              .classed("curved-path", true)
              .attr("marker-end", (d) => `url(#arrow__${d.id}`);

            enterSelection
              .append("defs")
              .append("marker")
              .attr("id", (d) => `arrow__${d.id}`)
              .attr("viewBox", "0 0 10 10")
              .attr("refX", (d) => {
                if (d.id.includes("stop")) {
                  return 30;
                } else {
                  return 27;
                }
              })
              .attr("refY", (d) => {
                if (d.id.includes("stop")) {
                  return 4;
                } else {
                  return 2.5;
                }
              })
              .attr("markerWidth", 5)
              .attr("markerHeight", 5)
              .attr("orient", "auto")
              .append("path")
              .attr("d", "M 0 0 L 10 5 L 0 10 z")
              .classed("curved-path-arrow", true);

            return enterSelection;
          },
          (update) => update,
          (exit) => exit.remove()
        );

      const linkStraightGroupSelection = linkGroup
        .selectAll("g[class='link-straight-group']")
        .data(straightLinkObjs)
        .join(
          (enter) => {
            const enterSelection = enter
              .append("g")
              .classed("link-straight-group", true);

            enterSelection
              .append("line")
              .attr("id", (d) => d.id)
              .classed("line", true)
              .attr("marker-end", (d) => `url(#arrow__${d.id}`);

            enterSelection
              .append("defs")
              .append("marker")
              .attr("id", (d) => `arrow__${d.id}`)
              .attr("viewBox", "0 0 10 10")
              .attr("refX", (d) => {
                if (d.id.includes("stop")) {
                  return 32;
                } else {
                  return 27;
                }
              })
              .attr("refY", 5)
              .attr("markerWidth", 5)
              .attr("markerHeight", 5)
              .attr("orient", "auto")
              .append("path")
              .attr("d", "M 0 0 L 10 5 L 0 10 z")
              .classed("line-arrow", true);

            return enterSelection;
          },
          (update) => update,
          (exit) => exit.remove()
        );

      return [linkCurvedGroupSelection, linkStraightGroupSelection];
    }

    function createD3NodeSelection(nodeGroup, nodeObjs) {
      const nodes = nodeGroup
        .selectAll("g[class='node']")
        .data(nodeObjs)
        .join(
          (enter) => {
            const enterSelection = enter.append("g").classed("node", true);
            enterSelection
              .append("circle")
              .attr("id", (d) => d.id)
              .attr("r", (d) => {
                if (d.name === "start" || d.name === "stop") {
                  return 30;
                } else {
                  return 25;
                }
              });

            enterSelection
              .append("text")
              .classed("node-text", true)
              .text((d) => `${d.depth}.${d.breadth}`)
              .attr("id", (d) => `node-text_${d.name}`)
              .attr("font-size", (d) => {
                if (d.name === "start" || d.name === "stop") {
                  return "20px";
                } else {
                  return "18px";
                }
              });

            return enterSelection;
          },
          (update) => update,
          (exit) => exit.remove()
        );

      return nodes;
    }
  }

  drawGraph();
});
