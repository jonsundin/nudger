export function getStaticData() {
    return [
      {
        name: "start",
        convo_name: "sum by one",
        convo_description: "new lines",
        nlp: "case_insensitive",
        level: 0,
        actions: {
          en: [
            "Let's start by counting up by 1s. I will give you 3 numbers. Please answer with the next number in the list. For example, if I say 4, 5, 6 then the answer is 7.  Now you try 😄",
          ],
        },
        buttons: {
          en: {
            OK: "q1",
          },
        },
        triggers: {
          en: {
            OK: "q1",
            __next__: "q1",
          },
        },
      },
      {
        name: "q1",
        level: 1,
        actions: {
          en: ["8, 9, 10"],
        },
        buttons: {
          en: {
            OK: "q1",
          },
        },
        triggers: {
          en: {
            11: "correct-answer-q1",
            eleven: "correct-answer-q1",
            __default__: "wrong-answer-q1",
          },
        },
      },
      {
        name: "wrong-answer-q1",
        actions: {
          en: ["Oops!"],
        },
        buttons: {
          en: {
            OK: "q1",
          },
        },
        triggers: {
          en: {
            __default__: "q1",
            __next__: "q1",
          },
        },
      },
      {
        name: "correct-answer-q1",
        actions: {
          en: ["Exactly right!"],
        },
        buttons: {
          en: {
            OK: "q2",
          },
        },
        triggers: {
          en: {
            __default__: "q2",
            __next__: "q2",
          },
        },
      },
      {
        name: "q2",
        level: 2,
        actions: {
          en: ["16, 17, 18"],
        },
        buttons: {
          en: {
            OK: "q2",
          },
        },
        triggers: {
          en: {
            19: "correct-answer-q2",
            __default__: "wrong-answer-q2",
          },
        },
      },
      {
        name: "wrong-answer-q2",
        actions: {
          en: ["Looks like a mistake."],
        },
        buttons: {
          en: {
            OK: "q2",
          },
        },
        triggers: {
          en: {
            __default__: "q2",
            __next__: "q2",
          },
        },
      },
      {
        name: "correct-answer-q2",
        actions: {
          en: ["Yes!"],
        },
        buttons: {
          en: {
            OK: "q3",
          },
        },
        triggers: {
          en: {
            __default__: "q3",
            __next__: "q3",
          },
        },
      },
      {
        name: "q3",
        level: 3,
        actions: {
          en: ["28, 29, 30"],
        },
        buttons: {
          en: {
            OK: "q3",
          },
        },
        triggers: {
          en: {
            31: "correct-answer-q3",
            __default__: "wrong-answer-q3",
          },
        },
      },
      {
        name: "wrong-answer-q3",
        actions: {
          en: ["Can you try again?"],
        },
        buttons: {
          en: {
            OK: "q3",
          },
        },
        triggers: {
          en: {
            __default__: "q3",
            __next__: "q3",
          },
        },
      },
      {
        name: "correct-answer-q3",
        actions: {
          en: ["Well done!"],
        },
        buttons: {
          en: {
            OK: "q4",
          },
        },
        triggers: {
          en: {
            __default__: "q4",
            __next__: "q4",
          },
        },
      },
      {
        name: "q4",
        level: 4,
        actions: {
          en: ["98, 99, 100"],
        },
        buttons: {
          en: {
            OK: "q4",
          },
        },
        triggers: {
          en: {
            101: "correct-answer-q4",
            __default__: "wrong-answer-q4",
          },
        },
      },
      {
        name: "wrong-answer-q4",
        actions: {
          en: ["Not quite. Please try one more time."],
        },
        buttons: {
          en: {
            OK: "q4",
          },
        },
        triggers: {
          en: {
            __default__: "q4",
            __next__: "q4",
          },
        },
      },
      {
        name: "correct-answer-q4",
        actions: {
          en: ["Nice job."],
        },
        buttons: {
          en: {
            OK: "q5",
          },
        },
        triggers: {
          en: {
            __default__: "q5",
            __next__: "q5",
          },
        },
      },
      {
        name: "q5",
        level: 5,
        actions: {
          en: ["501, 502, 503"],
        },
        buttons: {
          en: {
            OK: "q5",
          },
        },
        triggers: {
          en: {
            504: "correct-answer-q5",
            __default__: "wrong-answer-q5",
          },
        },
      },
      {
        name: "wrong-answer-q5",
        actions: {
          en: ["Oops! That's not right. Try it again."],
        },
        buttons: {
          en: {
            OK: "q5",
          },
        },
        triggers: {
          en: {
            __default__: "q5",
            __next__: "q5",
          },
        },
      },
      {
        name: "correct-answer-q5",
        actions: {
          en: ["Fantastic!"],
        },
        buttons: {
          en: {
            OK: "hq6",
          },
        },
        triggers: {
          en: {
            __default__: "hq6",
            __next__: "hq6",
          },
        },
      },
      {
        name: "hq6",
        level: 6,
        actions: {
          en: [
            "Now, let's Count Up by 2s.  What number is next if you count up by 2?",
          ],
        },
        buttons: {
          en: {
            OK: "q6",
          },
        },
        triggers: {
          en: {
            __default__: "q6",
            __next__: "q6",
          },
        },
      },
      {
        name: "q6",
        level: 6,
        actions: {
          en: ["2, 4, 6"],
        },
        buttons: {
          en: {
            OK: "q6",
          },
        },
        triggers: {
          en: {
            8: "correct-answer-q6",
            __default__: "wrong-answer-q6",
          },
        },
      },
      {
        name: "wrong-answer-q6",
        actions: {
          en: ["Give it another try. okay?"],
        },
        buttons: {
          en: {
            OK: "q6",
          },
        },
        triggers: {
          en: {
            __default__: "q6",
            __next__: "q6",
          },
        },
      },
      {
        name: "correct-answer-q6",
        actions: {
          en: ["Amazing!"],
        },
        buttons: {
          en: {
            OK: "q7",
          },
        },
        triggers: {
          en: {
            __default__: "q7",
            __next__: "q7",
          },
        },
      },
      {
        name: "q7",
        level: 7,
        actions: {
          en: ["10, 12, 14"],
        },
        buttons: {
          en: {
            OK: "q7",
          },
        },
        triggers: {
          en: {
            16: "correct-answer-q7",
            __default__: "wrong-answer-q7",
          },
        },
      },
      {
        name: "wrong-answer-q7",
        actions: {
          en: ["Oh oh! That's not right."],
        },
        buttons: {
          en: {
            OK: "q7",
          },
        },
        triggers: {
          en: {
            __default__: "q7",
            __next__: "q7",
          },
        },
      },
      {
        name: "correct-answer-q7",
        actions: {
          en: ["Excellent!"],
        },
        buttons: {
          en: {
            OK: "q8",
          },
        },
        triggers: {
          en: {
            __default__: "q8",
            __next__: "q8",
          },
        },
      },
      {
        name: "q8",
        level: 8,
        actions: {
          en: ["40, 42, 44"],
        },
        buttons: {
          en: {
            OK: "q8",
          },
        },
        triggers: {
          en: {
            46: "correct-answer-q8",
            __default__: "wrong-answer-q8",
          },
        },
      },
      {
        name: "wrong-answer-q8",
        actions: {
          en: ["Look carefully and try again."],
        },
        buttons: {
          en: {
            OK: "q8",
          },
        },
        triggers: {
          en: {
            __default__: "q8",
            __next__: "q8",
          },
        },
      },
      {
        name: "correct-answer-q8",
        actions: {
          en: ["Nice one!"],
        },
        buttons: {
          en: {
            OK: "q9",
          },
        },
        triggers: {
          en: {
            __default__: "q9",
            __next__: "q9",
          },
        },
      },
      {
        name: "q9",
        level: 9,
        actions: {
          en: ["84, 86, 88"],
        },
        buttons: {
          en: {
            OK: "q9",
          },
        },
        triggers: {
          en: {
            90: "correct-answer-q9",
            __default__: "wrong-answer-q9",
          },
        },
      },
      {
        name: "wrong-answer-q9",
        actions: {
          en: ["Almost. Try it again."],
        },
        buttons: {
          en: {
            OK: "q9",
          },
        },
        triggers: {
          en: {
            __default__: "q9",
            __next__: "q9",
          },
        },
      },
      {
        name: "correct-answer-q9",
        actions: {
          en: ["You got it!"],
        },
        buttons: {
          en: {
            OK: "q10",
          },
        },
        triggers: {
          en: {
            __default__: "q10",
            __next__: "q10",
          },
        },
      },
      {
        name: "q10",
        level: 10,
        actions: {
          en: ["200, 202, 204"],
        },
        buttons: {
          en: {
            OK: "q10",
          },
        },
        triggers: {
          en: {
            206: "correct-answer-q10",
            __default__: "wrong-answer-q10",
          },
        },
      },
      {
        name: "wrong-answer-q10",
        actions: {
          en: ["Oops! Looks like a mistake."],
        },
        buttons: {
          en: {
            OK: "q10",
          },
        },
        triggers: {
          en: {
            __default__: "q10",
            __next__: "q10",
          },
        },
      },
      {
        name: "correct-answer-q10",
        actions: {
          en: ["You are a star!"],
        },
        buttons: {
          en: {
            OK: "hq11",
          },
        },
        triggers: {
          en: {
            __default__: "hq11",
            __next__: "hq11",
          },
        },
      },
      {
        name: "hq11",
        level: 11,
        actions: {
          en: [
            "Now, let's Count Up by 5s.  What number is next when you count up by 5?",
          ],
        },
        buttons: {
          en: {
            OK: "q11",
          },
        },
        triggers: {
          en: {
            __default__: "q11",
            __next__: "q11",
          },
        },
      },
      {
        name: "q11",
        level: 11,
        actions: {
          en: ["5, 10, 15"],
        },
        buttons: {
          en: {
            OK: "q11",
          },
        },
        triggers: {
          en: {
            20: "correct-answer-q11",
            __default__: "wrong-answer-q11",
          },
        },
      },
      {
        name: "wrong-answer-q11",
        actions: {
          en: ["Can you try again?"],
        },
        buttons: {
          en: {
            OK: "q11",
          },
        },
        triggers: {
          en: {
            __default__: "q11",
            __next__: "q11",
          },
        },
      },
      {
        name: "correct-answer-q11",
        actions: {
          en: ["Great job!"],
        },
        buttons: {
          en: {
            OK: "q12",
          },
        },
        triggers: {
          en: {
            __default__: "q12",
            __next__: "q12",
          },
        },
      },
      {
        name: "q12",
        level: 12,
        actions: {
          en: ["20, 25, 30"],
        },
        buttons: {
          en: {
            OK: "q12",
          },
        },
        triggers: {
          en: {
            35: "correct-answer-q12",
            __default__: "wrong-answer-q12",
          },
        },
      },
      {
        name: "wrong-answer-q12",
        actions: {
          en: ["Not quite. Please try one more time."],
        },
        buttons: {
          en: {
            OK: "q12",
          },
        },
        triggers: {
          en: {
            __default__: "q12",
            __next__: "q12",
          },
        },
      },
      {
        name: "correct-answer-q12",
        actions: {
          en: ["Right on."],
        },
        buttons: {
          en: {
            OK: "q13",
          },
        },
        triggers: {
          en: {
            __default__: "q13",
            __next__: "q13",
          },
        },
      },
      {
        name: "q13",
        level: 13,
        actions: {
          en: ["55, 60, 65"],
        },
        buttons: {
          en: {
            OK: "q13",
          },
        },
        triggers: {
          en: {
            70: "correct-answer-q13",
            __default__: "wrong-answer-q13",
          },
        },
      },
      {
        name: "wrong-answer-q13",
        actions: {
          en: ["Give it another try okay?"],
        },
        buttons: {
          en: {
            OK: "q13",
          },
        },
        triggers: {
          en: {
            __default__: "q13",
            __next__: "q13",
          },
        },
      },
      {
        name: "correct-answer-q13",
        actions: {
          en: ["Perfect!"],
        },
        buttons: {
          en: {
            OK: "q14",
          },
        },
        triggers: {
          en: {
            __default__: "q14",
            __next__: "q14",
          },
        },
      },
      {
        name: "q14",
        level: 14,
        actions: {
          en: ["85, 90, 95"],
        },
        buttons: {
          en: {
            OK: "q14",
          },
        },
        triggers: {
          en: {
            100: "correct-answer-q14",
            __default__: "wrong-answer-q14",
          },
        },
      },
      {
        name: "wrong-answer-q14",
        actions: {
          en: ["Look carefully and try again."],
        },
        buttons: {
          en: {
            OK: "q14",
          },
        },
        triggers: {
          en: {
            __default__: "q14",
            __next__: "q14",
          },
        },
      },
      {
        name: "correct-answer-q14",
        actions: {
          en: ["Exactly right!"],
        },
        buttons: {
          en: {
            OK: "q15",
          },
        },
        triggers: {
          en: {
            __default__: "q15",
            __next__: "q15",
          },
        },
      },
      {
        name: "q15",
        level: 15,
        actions: {
          en: ["120, 125, 130"],
        },
        buttons: {
          en: {
            OK: "q15",
          },
        },
        triggers: {
          en: {
            135: "correct-answer-q15",
            __default__: "wrong-answer-q15",
          },
        },
      },
      {
        name: "wrong-answer-q15",
        actions: {
          en: ["Almost. Try it again."],
        },
        buttons: {
          en: {
            OK: "q15",
          },
        },
        triggers: {
          en: {
            __default__: "q15",
            __next__: "q15",
          },
        },
      },
      {
        name: "correct-answer-q15",
        actions: {
          en: ["Yes!"],
        },
        buttons: {
          en: {
            OK: "q16",
          },
        },
        triggers: {
          en: {
            __default__: "q16",
            __next__: "q16",
          },
        },
      },
      {
        name: "hq16",
        level: 16,
        actions: {
          en: [
            "Now, let's Count Up by 10s.  What number is next when you count up by 10?",
          ],
        },
        buttons: {
          en: {
            OK: "q16",
          },
        },
        triggers: {
          en: {
            __default__: "q16",
            __next__: "q16",
          },
        },
      },
      {
        name: "q16",
        level: 16,
        actions: {
          en: [
            "Now, let's Count Up by 10s.  What number is next when you count up by 10?",
            "10, 20, 30",
          ],
        },
        buttons: {
          en: {
            OK: "q16",
          },
        },
        triggers: {
          en: {
            40: "correct-answer-q16",
            __default__: "wrong-answer-q16",
          },
        },
      },
      {
        name: "wrong-answer-q16",
        actions: {
          en: ["Oops!"],
        },
        buttons: {
          en: {
            OK: "q16",
          },
        },
        triggers: {
          en: {
            __default__: "q16",
            __next__: "q16",
          },
        },
      },
      {
        name: "correct-answer-q16",
        actions: {
          en: ["Well done!"],
        },
        buttons: {
          en: {
            OK: "q17",
          },
        },
        triggers: {
          en: {
            __default__: "q17",
            __next__: "q17",
          },
        },
      },
      {
        name: "q17",
        level: 17,
        actions: {
          en: ["20, 30, 40"],
        },
        buttons: {
          en: {
            OK: "q17",
          },
        },
        triggers: {
          en: {
            50: "correct-answer-q17",
            __default__: "wrong-answer-q17",
          },
        },
      },
      {
        name: "wrong-answer-q17",
        actions: {
          en: ["Looks like a mistake. Can you try again?"],
        },
        buttons: {
          en: {
            OK: "q17",
          },
        },
        triggers: {
          en: {
            __default__: "q17",
            __next__: "q17",
          },
        },
      },
      {
        name: "correct-answer-q17",
        actions: {
          en: ["Nice job."],
        },
        buttons: {
          en: {
            OK: "q18",
          },
        },
        triggers: {
          en: {
            __default__: "q18",
            __next__: "q18",
          },
        },
      },
      {
        name: "q18",
        level: 18,
        actions: {
          en: ["25, 35, 45"],
        },
        buttons: {
          en: {
            OK: "q18",
          },
        },
        triggers: {
          en: {
            55: "correct-answer-q18",
            __default__: "wrong-answer-q18",
          },
        },
      },
      {
        name: "wrong-answer-q18",
        actions: {
          en: ["Almost. Try it again."],
        },
        buttons: {
          en: {
            OK: "q18",
          },
        },
        triggers: {
          en: {
            __default__: "q18",
            __next__: "q18",
          },
        },
      },
      {
        name: "correct-answer-q18",
        actions: {
          en: ["Super!"],
        },
        buttons: {
          en: {
            OK: "q19",
          },
        },
        triggers: {
          en: {
            __default__: "q19",
            __next__: "q19",
          },
        },
      },
      {
        name: "q19",
        level: 19,
        actions: {
          en: ["63, 73, 83"],
        },
        buttons: {
          en: {
            OK: "q19",
          },
        },
        triggers: {
          en: {
            93: "correct-answer-q19",
            __default__: "wrong-answer-q19",
          },
        },
      },
      {
        name: "wrong-answer-q19",
        actions: {
          en: ["Give it another try. Okay?"],
        },
        buttons: {
          en: {
            OK: "q19",
          },
        },
        triggers: {
          en: {
            __default__: "q19",
            __next__: "q19",
          },
        },
      },
      {
        name: "correct-answer-q19",
        actions: {
          en: ["You are a star!"],
        },
        buttons: {
          en: {
            OK: "q20",
          },
        },
        triggers: {
          en: {
            __default__: "q20",
            __next__: "q20",
          },
        },
      },
      {
        name: "q20",
        level: 20,
        actions: {
          en: ["300, 310, 320"],
        },
        buttons: {
          en: {
            OK: "q20",
          },
        },
        triggers: {
          en: {
            330: "correct-answer-q20",
            __default__: "wrong-answer-q20",
          },
        },
      },
      {
        name: "wrong-answer-q20",
        actions: {
          en: ["Oh oh! That's not right."],
        },
        buttons: {
          en: {
            OK: "q20",
          },
        },
        triggers: {
          en: {
            __default__: "q20",
            __next__: "q20",
          },
        },
      },
      {
        name: "correct-answer-q20",
        actions: {
          en: ["Perfect!"],
        },
        buttons: {
          en: {
            OK: "end",
          },
        },
        triggers: {
          en: {
            __default__: "end",
            __next__: "end",
          },
        },
      },
      {
        name: "stop",
        level: 21,
        actions: {
          en: ["Thank you for attending!"],
        },
      },
    ];
  }