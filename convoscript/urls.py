from django.urls import path
from . import views

urlpatterns = [
    path("", views.home, name="convoscript-home"),
    path("about/", views.state_form_buttons, name="convoscript-about"),
    path("check/", views.state_form_buttons, name="convoscript_check"),
    path("export/", views.export, name="convoscript_export"),
    path("populate_form/", views.populate_form, name="convoscript_populateform"),
    path("graph_data/", views.convograph, name="convograph_data"),
]
