"""
from django.contrib import admin
from .models import State, Trigger

admin.site.register(State)
admin.site.register(Trigger)
"""
