#!/usr/bin/env bash

if [ -f ".env" ] ; then
    echo "WARNING: .env already exists! You should overwrite your .env file with the latest .env_example"
else
    echo "INFO: Creating .env from a copy of .env_example."
    cp .env_example .env
fi
source .env

pip install --upgrade pip poetry virtualenv wheel build twine

if [ ! -f ".venv/bin/activate" ] ; then
    python -m virtualenv .venv
fi
source .venv/bin/activate

pip install --upgrade --editable .[dev]

# FIXME: Dev dependencies are NOT installed using "extras [dev]" in pyproject.toml
pip install --upgrade black pytest
