# scripts/twilio_tutorial.py
from twilio.rest import Client
from dotenv import load_dotenv, dotenv_values

load_dotenv()
ENV = dotenv_values()

print(dict(username=ENV["TWILIO_ACCOUNT_SID"], password=ENV["TWILIO_AUTH_TOKEN"]))

client = Client(username=ENV["TWILIO_ACCOUNT_SID"], password=ENV["TWILIO_AUTH_TOKEN"])

# curl -X POST "https://api.twilio.com/2010-04-01/Accounts/" \
#     AC366cdde30a77b73462206b7d181e7225/Messages.json" \
#     --data-urlencode "Body=Hello from Twilio"
#     --data-urlencode "From=+18449993537"
#     --data-urlencode "To=+15039746274"
#     -u "AC366cdde30a77b73462206b7d181e7225:7fb0f3f4b43355afca20e3cb0aac7f82"

# FIXME:
# POST /Accounts/AC01c0604f28dcd892d1937cd114aab754/Messages.json
message = client.messages.create(
    body=f"Test message from {__package__}.{__name__} with docs: {__doc__}?",
    from_=f'{ENV["TWILIO_PHONENUM"]}',
    to=ENV["DEFAULT_HUMAN_PHONENUM"],
)

print(message.sid)
