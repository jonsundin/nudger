import os
import sys
from pathlib import Path

import django
import pandas as pd
import yaml
from yaml.parser import ParserError
from yaml.scanner import ScannerError

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "nudger.settings")
django.setup()
from django.conf import settings
from quizbot.convograph_linter import ConvoGraphValidator, ConvoGraphValidationError

DATA_DIR = settings.DATA_DIR


def load_yaml(file_path):
    """Loads a yaml file from the given path"""
    file_path = Path(file_path)
    with file_path.open("r") as file:
        return yaml.safe_load(file)


def print_by_rows(df: pd.DataFrame, columns):
    for k, row in df.iterrows():
        for title in columns:
            print(f"{title}: {row[title]}")
        print()


def get_file_paths(dir_path):
    """Returns all file paths in the given folder"""
    path = Path(dir_path)
    return list(path.glob("**/*.yml")) + list(path.glob("**/*.yaml"))


def validate_all_files(file_paths):
    """Validates all files with the ConvoGraphValidator"""
    df = []
    for file_path in file_paths:
        file_content = str()
        row = {}
        row["file_path"] = file_path

        try:
            file_content = load_yaml(file_path)
        except ScannerError as err:
            row["module"] = err.__module__
            row["class"] = err.__class__.__name__
            row["error"] = err
            row["state"] = "Fail"
        except ParserError as err:
            row["module"] = err.__module__
            row["class"] = err.__class__.__name__
            row["error"] = err
            row["state"] = "Fail"
        else:
            try:
                ConvoGraphValidator(file_content).run()
                row["state"] = "Success"
            except ConvoGraphValidationError as err:
                row["module"] = err.__module__
                row["class"] = err.__class__.__name__
                row["error"] = err
                row["state"] = "Fail"
        df.append(row)
    return pd.DataFrame(df)


def main(dir_path=DATA_DIR):
    """Main function"""

    paths = get_file_paths(dir_path)
    return validate_all_files(paths)


if __name__ == "__main__":
    data_dir = DATA_DIR
    if len(sys.argv) > 1:
        data_dir = Path(sys.argv[1])
    df = main(data_dir)
    print_by_rows(df, df.columns.to_list())
