"""
Don't change the sequence of the imports.
`setdefault` and `django.setup` should be on the start!
"""

import os

import django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "nudger.settings")
django.setup()

import io
import logging
from pathlib import Path
import sys
import yaml

from django.contrib.auth.models import User
from django.core.files.base import ContentFile
from django.core.files.uploadedfile import InMemoryUploadedFile

from nudger.constants import ENV, DEFAULT_CONVOGRAPH_YAML_PATH
from quizbot.convograph_validator.v2.convograph_validator import ConvoGraphValidator
from quizbot.convograph_validator.convograph_validator_errors import (
    ConvoGraphValidationError,
)
from quizbot.load_yaml import convoyaml_to_convomodel
from quizbot.models import Convo
from django.conf import settings

log = logging.getLogger(__name__)

default_user = User.objects.filter(username=ENV.get("DJANGO_DEFAULT_USERNAME")).first()
try:
    Convo.objects.all().filter(user=default_user, name="default").first().delete()
except AttributeError:
    pass


def convert_to_inmemoryuploadedfile(text_io_wrapper, filename, content_type):
    content = text_io_wrapper.read()
    in_memory_uploaded_file = InMemoryUploadedFile(
        file=ContentFile(content),
        field_name=None,
        name=filename,
        content_type=content_type,
        size=len(content),
        charset=None,
    )
    return in_memory_uploaded_file


def create_convo(
    file_path=DEFAULT_CONVOGRAPH_YAML_PATH,
    user=default_user,
    convo_name=None,
    convo_description=None,
):
    file_path = Path(file_path)
    conversation = None

    if not file_path.is_file():
        log.error(f"No file found: {file_path}")
        return
    elif not user:
        log.error("Create default user first!")
        return
    error_msg = None
    try:
        conversation = yaml.safe_load(Path(file_path).read_text())
        ConvoGraphValidator(conversation).run()
    except ConvoGraphValidationError as exc:
        error_msg = f"{str(exc)} "
    except Exception:
        error_msg = (
            "Wrong syntax! "
            + 'File content should contain multiple entries starting from "-" character.'
            + 'Most likely you forgot to put leading "-" before one of states. '
        )

    if error_msg:
        error_msg = "Some issue occurred in default conversation! " + error_msg
        error_msg += "Use the format in the https://gitlab.com/tangibleai/nudger/-/blob/main/data/countByOne_0001.yml"
        log.error(error_msg)
        return
    if settings.USE_S3:
        with open(file_path, "rb") as f:
            file = ContentFile(f.read())
        file.name = DEFAULT_CONVOGRAPH_YAML_PATH.name

    else:
        file = convert_to_inmemoryuploadedfile(
            io.TextIOWrapper(open(file_path, "rb")),
            DEFAULT_CONVOGRAPH_YAML_PATH.name,
            "text/yaml",
        )
    convo_name = conversation[0].get("convo_name") if not convo_name else convo_name
    convo_description = (
        conversation[0].get("convo_description")
        if not convo_description
        else convo_description
    )
    convo_queryset = convoyaml_to_convomodel(
        graph=file,
        user=user,
        convo_name=convo_name,
        convo_description=convo_description,
    )
    log.info(
        f"Created convo:\n  file: {file}\n  user: {user}\n"
        + f"  convo_name: {convo_name}\n  description: {convo_description}"
    )
    return convo_queryset


if __name__ == "__main__":
    file_path_ = DEFAULT_CONVOGRAPH_YAML_PATH
    if len(sys.argv) > 1:
        file_path_ = Path(sys.argv[1])
    convo_queryset = create_convo()
