#!/usr/bin/env bash
# all these commands should be run on the nudger server

# deploy.sh
#
# Should be run on an administrator's laptop (not in the cloud) within at the location of this script

## Source code and .env file should be present on your laptop
#
# cd ~
# mkdir -p code
# cd code
# git clone git@github.com:tangibleai/nudger
# cd nudger
# create .env file containing security credentials

## ~/.ssh/config
#
# Host nudger
#     Hostname      52.38.194.191
#     User          ubuntu
#     IdentityFile  ~/.ssh/id_rsa_nudger_ec2

rsync -avz ./ nudger:nudger/ --exclude .git

## Test psql connection to RDS database
# EC2 public IP: 52.38.194.191
# EC2 private IP: 172.31.45.172
# region: us-west-2 (oregon)
# vpc id: vpc-8f7459f7 (both RDS and EC2)
# VPC CIDR: 172.31.0.0/16
# EC2 private domain: ip-172-31-45-172.us-west-2.compute.internal
# EC2 firewall:
#     Inbound rules
#       Security group rule ID, Port range, Protocol, Source, Security groups
#       sgr-07eaf27a1da4f8f98   8000    TCP ::/0    launch-wizard-1
#       sgr-0416f6f48a952fbd9   22  TCP 0.0.0.0/0   launch-wizard-1
#       sgr-003290c9cd938bdcb   80  TCP 0.0.0.0/0   launch-wizard-1
#       sgr-07348e5ca099e1a0c   443 TCP 0.0.0.0/0   launch-wizard-1
#       sgr-0dc37af2e995f311c   443 TCP ::/0    launch-wizard-1
#       sgr-00e27b44b308d6b6b   80  TCP ::/0    launch-wizard-1
#       sgr-01399e7afea292c5e   8000    TCP 0.0.0.0/0   launch-wizard-1
#     Outbound rules
#       sgr-0a32b04e807d29943   All All 0.0.0.0/0   launch-wizard-1
sudo apt update
sudo apt -y upgrade

# basics/terminal
sudo apt isntall -y vim bash-completion wget build-essential curl gnupg apt-transport-https

# for rabbitmq?
sudo apt install software-properties-common apt-transport-https install erlang

# python 3.8.10
sudo apt install -y python3.8-venv python3-pip python3-dev
# because everything is run by sudo, this is the python environment used by gunicorn rabbitmq psychopg2 etc


#####################################################################
# psql (postgres) client and psycopg2 dependencies

# postgresql for psycopg2
sudo apt install -y postgresql-client-13  # postgresql-contrib-13

# Check the postgres connection from the EC2 instance (within the VPC)
ssh nudger << EOF
psql --host='daswest2.carck9qtrhyv.us-west-2.rds.amazonaws.com' --port=5432 --username=postgres --dbname=postgres --password
EOF



###############################################
# gunicorn
#
# To manually run gunicorn
# sudo chmod u+x bin/gunicorn_start

python -m pip install gunicorn
sudo ln etc/gunicorn.service /etc/systemd/system/gunicorn.service
sudo systemctl enable --now gunicorn.service
sudo systemctl daemon-reload
# sudo service gunicorn restart
# sudo service gunicorn status


################################################333
# nginx

# nginx
sudo apt-get install nginx

# rm the default nginx greeting page settings
sudo rm nginx_default /etc/nginx/sites-enabled/default 
# hard link from the sites_available/nginx_default to this etc/nginx_default
sudo ln etc/nudger_gunicorn.nginx /etc/nginx/sites-enabled/nudger_gunicorn.nginx
sudo systemctl enable --now nginx
sudo systemctl daemon-reload
# sudo service nginx restart
# sudo service nginx status

# nginx
################################################


################################################
# RabbitMQ & celery
# https://computingforgeeks.com/how-to-install-latest-rabbitmq-server-on-ubuntu-linux/
# https://www.rabbitmq.com/install-debian.html#apt-quick-start-cloudsmith

# erlang (used by RabbitMQ)
sudo apt install curl gnupg apt-transport-https
wget -O- https://packages.erlang-solutions.com/ubuntu/erlang_solutions.asc | sudo apt-key add -
echo "deb https://packages.erlang-solutions.com/ubuntu focal contrib" | sudo tee /etc/apt/sources.list.d/rabbitmq.list
sudo apt install -y erlang

# Redis RAM db may not be required but just in case...
sudo apt-get install -y redis-server

# pip may not work
# python -m pip install rabbitmq-server
# systemctl status rabbitmq-server.service
# systemctl is-enabled rabbitmq-server.service

curl -s https://packagecloud.io/install/repositories/rabbitmq/rabbitmq-server/script.deb.sh | sudo bash
sudo apt install -y rabbitmq-server
systemctl is-enabled rabbitmq-server.service
systemctl status rabbitmq-server.service


# RabbitMQ & celery
#################################################



###########################################################
# DEPRECATED!!!!!!!!!!!
# COMMANDS THAT SHOW HOW TO RUN SERVER CONFIG COMMANDS FROM LAPTOP OR GITHUB ACTIONS

# Set up media/ and static/ dirs
ssh nudger << EOF
cd nudger
# sudo systemctl restart gunicorn
cd sms_nudger
pip install pillow
python manage.py makemigrations
python manage.py migrate
sudo systemctl restart gunicorn
ls media
cd ..
ls media
python manage.py shell
cd nudger
cat settings.py
sudo systemctl restart gunicorn
cd static
cd sms_nudger
cd static
cd sms_nudger
cat main.css
ls
cd ..
ls
cd ..
ls
cd templates
ls
cd sms_nudger
ls
cat base.html
ls
cd nudger
ls
python manage.py collectstatic
cd ..
ls
cd nudger
ls
sudo systemctl restart gunicorn
cd nudger
ls
python manage.py collectstatic
sudo systemctl restart gunicorn
EOF

# set up anaconda (miniconda/conda)
ssh nudger << EOF
curl -OL https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh
bash Miniconda3-latest-Linux-x86_64.sh
~/miniconda3/bin/conda init bash
source ~/.bashrc
which conda
cd ~/nudger
conda create -n nudgerenv python==3.8.8
conda activate nudgerenv
conda env export -n nudgerenv >> environment.yml
nano environment.yml
EOF

# COMMANDS THAT SHOW HOW TO RUN SERVER CONFIG COMMANDS FROM LAPTOP OR GITHUB ACTIONS
# DEPRECATED!!!!!!!!!!!
###########################################################
