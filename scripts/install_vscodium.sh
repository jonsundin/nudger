#! /usr/bin/bash

# SEE: https://vscodium.com/#use-a-package-manager-providing-vscodium-in-their-repository

if [ -n $(which apt) ]; then
    wget -qO - https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/master/pub.gpg \
        | gpg --dearmor \
        | sudo dd of=/usr/share/keyrings/vscodium-archive-keyring.gpg

    echo 'deb [ signed-by=/usr/share/keyrings/vscodium-archive-keyring.gpg ] https://download.vscodium.com/debs vscodium main' \
        | sudo tee /etc/apt/sources.list.d/vscodium.list

    sudo echo "Running 'sudo apt install codium'..." \
        && sudo apt update \
        && sudo apt install codium
elif [ -n $(which nix-env) ]; then
    sudo echo "Running 'nix-env -iA nixpkgs.vscodium'..." \
        && yay -S vscodium-bin
        nix-env -iA nixpkgs.vscodium
elif [ -n $(which yay) ]; then
    sudo echo "Running 'yay -S vscodium-bin'..." \
        && yay -S vscodium-bin
elif [ -n $(which aura) ]; then
    sudo echo "Running 'sudo aura -A vscodium-bin'..." \
        && sudo aura -A vscodium-bin
else
    echo "ERROR: Can't install vscodium on your OS with this script."
    echo "  SEE: https://vscodium.com/#use-a-package-manager-providing-vscodium-in-their-repository"
fi

pip install --upgrade black

echo 'Creating settings.json. Make sure this line is included in your vscodium or project directories'
echo '"python.formatting.provider": "black"' >> settings.json

