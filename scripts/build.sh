#!/usr/bin/env bash

source ./scripts/build_environment.sh

python manage.py migrate

echo "Collecting static files!"
python manage.py collectstatic --noinput -v 2

# Create default user
echo "Default user creation"
python -m scripts.create_predefined_django_users

# Creating the default convo if not exits
echo "Default convo creation"
python -m scripts.create_default_convo_on_db
