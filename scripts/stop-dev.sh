PIDs=$(pgrep celery | tr '\n' ' ')
echo "pkill ${PIDs}  # Celery processes"
echo $(kill -9 ${PIDs} > /dev/null 2>&1) || echo "INFO: No existing Celery processes found."

sleep 2

echo "Killing the process attached to port 8000 (python manage.py runserver)..."
fuser 8000/tcp
echo $(fuser -k 8000/tcp > /dev/null 2>&1) || echo "INFO: No existing processes attached to port 8000..."




