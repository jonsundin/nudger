import os

import django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "nudger.settings")
django.setup()

from django.contrib.auth.models import User
from django.db.utils import IntegrityError

try:
    super_user = User.objects.create_superuser(
        username=os.environ.get("DJANGO_SUPERUSER_USERNAME"),
        password=os.environ.get("DJANGO_SUPERUSER_PASSWORD"),
    )
    print("Super user has been created!")
except IntegrityError as exc:
    print(f"{str(exc)}. Super user already exists!")


try:
    default_user = User.objects.create_user(
        username=os.environ.get("DJANGO_DEFAULT_USERNAME"),
        password=os.environ.get("DJANGO_DEFAULT_PASSWORD"),
    )  # to serve as AnonymousUser to bing it with default conversation
    print("Default user has been created!")
except IntegrityError as exc:
    print(f"{str(exc)}. Default user already exists!")
