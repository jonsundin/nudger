#!/usr/bin/env bash

# set -o errexit
set -o nounset
set -o pipefail

# FIXME: makes sure this does a `pip install` **--editable** `.`
source ./scripts/build_environment.sh

python manage.py migrate

echo "INFO: Collecting static files!"
python manage.py collectstatic --noinput -v 3

# Create default user
echo "INFO: Creating superuser and default normal user with python -m scripts.create_predefined_django_users"
# FIXME: add __name__ == '__main__' to this py script
python -m scripts.create_predefined_django_users

# Creating the default convo if not exits
echo "INFO: Creating default convo with scripts/create_default_convo_on_db.py"
# FIXME: add __name__ == '__main__' to this py script
python -m scripts.create_default_convo_on_db
# celery -A nudger.celery worker --loglevel=info --concurrency=2 -O fair &
# celery -A nudger.celery beat --loglevel=info --scheduler django_celery_beat.schedulers:DatabaseScheduler &

# echo `pwd`
if [ -f "./.env" ] ; then
    source ./.env
fi

if [ -f ".venv/bin/activate" ] ; then
    source .venv/bin/activate
fi

echo $(kill -9 $(pgrep celery | tr '\n' ' ') > /dev/null 2>&1) \
  || echo "INFO: No existing Celery processes found." 

celery -A nudger.celery worker --beat --scheduler django --loglevel=info --concurrency 4  &

sleep 3
# start-django (daphne)
# daphne -b 0.0.0.0 nudger.asgi:application &
# daphne -b 0.0.0.0 nudger.asgi:application
echo $(fuser -k 8000/tcp > /dev/null 2>&1) || echo "INFO: No existing processes attached to port 8000..."
sleep 1
python manage.py runserver &

sleep 4

echo "INFO: Success! Finished launching Celery and the Django development server (manage.py runserver)."
