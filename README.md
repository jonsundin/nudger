[![pipeline status](https://gitlab.com/tangibleai/community/ConvoHub/badges/staging/pipeline.svg)](https://gitlab.com/tangibleai/community/ConvoHub/-/commits/staging)
[![coverage report](https://gitlab.com/tangibleai/community/ConvoHub/badges/staging/coverage.svg)](https://gitlab.com/tangibleai/community/ConvoHub/-/commits/staging)


# ConvoHub
A platform for building and sharing AI assistants that actually assist rather than manipulate you. 


## Installation

Clone the repository:

```bash
git clone git@gitlab.com:tangibleai/community/ConvoHub
cd ConvoHub
```

Create a Python virtual environment with `virtualenv` (or `conda`), and install dependencies there:

### `virtualenv` (preferred approach)

```bash
pip install --upgrade pip build poetry wheel virtualenv
python -m virtualenv --python=3.10 .venv
source .venv/bin/activate
```

### Anaconda (conda)

```bash
conda create -y -n ConvoHub 'python=3.10.12'
conda activate ConvoHub
# conda env update -n ConvoHub -f environment.yml
```

## Build (install)

Once you have a clean virtual environment activated you can install all the dependencies and migrate your databases.
See `scripts/build.sh` if you want to run all the commands manually.
If `source scripts/build.sh` doesn't work on your OS, you will need to modify the `build.sh` script.
Here's the main thing you need to install the dependencies and add the ConvoHub package to your `PYTHON_PATH` OS environment variables.

#### *`scripts/build.sh`* 
```bash
pip install -e .
```

## Deploy (or run)

### Local development server

#### *`scripts/start-django-dev.sh`*
```bash
python manage.py runserver
```

## Creating Merge Requests/Pull Requests
Developers should only be working from and pushing merge/pull requests to the staging branch to make sure that we always have one live
branch for demonstrations. That means after you clone the repository locally you will want to create a staging branch. There is also 
the option to set an upstream URL.
```asciidoc
git checkout -b staging
git branch --set-upstream-to origin/staging
```


Keeping the local repository up to date with ConvoHub's GitLab repository will help to prevent merge conflicts, so make sure your local 
repository is uptodate before creating a branch to work on, make sure you are creating your branch from staging, and that you start the branch name with `Fix` or `Feature.`
```
git checkout staging
git pull origin staging
git checkout -b branch_I_am_working_on
```
Before creating a merge/pull request update the branch you are working on by merging the staging branch into it.
```asciidoc
git checkout branch_I_am_working_on
git merge staging
git push origin Fix_branch_I_am_working_on
```
Once the link to the merge/pull request is created make sure that the `Staging` branch is selected by using the Edit button
![README2.png](..%2F..%2F..%2FPictures%2FREADME2.png)
And click on the following:
* [x] Delete source branch when merge request is accepted.
* [x] Squash commits when merge request is accepted.

## Running Tests
ConvoHub is making use of both Django's unit tests and Pytest. As of now, you need two different commands to run all of them.
These commands assume you are running them from ConvoHub's root directory.
```asciidoc
python3 -m pytest tests/tests_yaml_linter.py
python3 -m manage test
```
