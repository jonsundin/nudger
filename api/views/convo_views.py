from datetime import datetime

from django.contrib.auth.models import User
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.decorators import api_view

from quizbot import serializers
from quizbot.models import Convo
from quizbot.models_convomeld import merge_convos


@csrf_exempt
@api_view(["GET"])
def get_convo_list(request):
    payload = request.data or request.query_params.dict()
    user_id = request.session.get("user_id", payload.get("user_id", None))
    is_list_public = payload.get("is_list_public", True) in ("true", "True", True)
    query_convo_by = {}
    if not is_list_public:
        query_convo_by["user__id"] = user_id
    else:
        query_convo_by = {"is_public": is_list_public}
    convos = [
        c for c in Convo.objects.filter(**query_convo_by).order_by("-activated_on")
    ]
    serialized_convos = [serializers.serialize_convo(c) for c in convos]
    return JsonResponse(serialized_convos, safe=False)


@csrf_exempt
@api_view(["POST"])
def merge_action(request):
    payload = request.data or request.query_params.dict()
    user_id = request.session.get("user_id", payload.get("user_id", None))
    user = User.objects.get(id=user_id)
    convo_ids = payload.get("convo_ids", [])
    if len(convo_ids) == 1:
        return JsonResponse({})
    convos = [Convo.objects.get(id=id) for id in convo_ids]
    merged_convo_name = payload.get("name", " + ".join([c.name for c in convos]))
    merged_convo_description = payload.get(
        "description", f"Merged {' + '.join([c.name for c in convos])}"
    )
    merged_convo = merge_convos(
        user, merged_convo_name, merged_convo_description, *convos
    )
    return JsonResponse(serializers.serialize_convo(merged_convo), safe=False)


def toggle_convos_public_affiliation(convo_ids, is_public):
    convos = set()
    for id in convo_ids:
        convo = Convo.objects.get(id=id)
        convo.is_public = True if is_public else False
        convo.save()
        convos.add(convo)
    ordered_convos = sorted(convos, key=lambda c: c.activated_on, reverse=True)
    serialized_convos = [serializers.serialize_convo(c) for c in ordered_convos]
    return serialized_convos


@csrf_exempt
@api_view(["PUT"])
def make_public_action(request):
    payload = request.data or request.query_params.dict()
    convo_ids = payload.get("convo_ids", [])
    published_convos = toggle_convos_public_affiliation(convo_ids, True)
    return JsonResponse(published_convos, safe=False)


@csrf_exempt
@api_view(["PUT"])
def make_private_action(request):
    payload = request.data or request.query_params.dict()
    convo_ids = payload.get("convo_ids", [])
    unpublished_convos = toggle_convos_public_affiliation(convo_ids, False)
    return JsonResponse(unpublished_convos, safe=False)


def get_query_by_attributes():
    search_by = [
        "id",
        "name",
        "description",
        "user__username",
    ]
    for i, item in enumerate(search_by.copy()):
        search_by[i] = item + "__contains"
    return search_by


@csrf_exempt
@api_view(["GET"])
def search(request):
    payload = request.data or request.query_params.dict()
    user_id = request.session.get("user_id", payload.get("user_id", None))
    search_value = payload.get("search", [])
    is_list_public = payload.get("is_list_public", True) in ("True", "true", True)
    search_by = get_query_by_attributes()
    unique_convos = set()
    query_convo_by = {}
    if not is_list_public:
        query_convo_by["user__id"] = user_id
    else:
        query_convo_by = {"is_public": is_list_public}
    for item in search_by:
        query_convo_by[item] = search_value
        convos = Convo.objects.filter(**query_convo_by)
        if convos:
            for c in convos:
                unique_convos.add(c)
        del query_convo_by[item]
    ordered_convos = sorted(unique_convos, key=lambda c: c.activated_on, reverse=True)
    serialized_convos = [serializers.serialize_convo(c) for c in ordered_convos]
    return JsonResponse(serialized_convos, safe=False)


def convert_str_date_to_datetime(js_date_string: str):
    """
    >>> convert_str_date_to_datetime("Wed Jul 28 1993")
    1993-07-28 00:00:00
    """
    js_date_format = "%a %b %d %Y"

    python_datetime = datetime.strptime(js_date_string, js_date_format)
    return python_datetime


@csrf_exempt
@api_view(["GET"])
def filter(request):
    payload = request.data or request.query_params.dict()
    user_id = request.session.get("user_id", payload.get("user_id", None))
    is_list_public = payload.get("is_list_public", True) in ("True", "true", True)
    filter_by = {}
    if not is_list_public:
        filter_by["user__id"] = user_id
    else:
        filter_by = {"is_public": is_list_public}
    for filter in get_query_by_attributes():
        for arg, val in payload.items():
            if filter.startswith(arg):
                filter_by[filter] = val
                break
    activated_on_range = [None, None]
    if ("start_date", "end_date") <= tuple(payload):
        activated_on_range[0] = convert_str_date_to_datetime(payload["start_date"])
        activated_on_range[1] = convert_str_date_to_datetime(payload["end_date"])
        filter_by["activated_on__range"] = activated_on_range
    convos = set()
    for c in Convo.objects.filter(**filter_by):
        convos.add(c)
    ordered_convos = sorted(convos, key=lambda c: c.activated_on, reverse=True)
    serialized_convos = [serializers.serialize_convo(c) for c in ordered_convos]
    return JsonResponse(serialized_convos, safe=False)


@csrf_exempt
@api_view(["PUT"])
def activate_convo(request):
    payload = request.data or request.query_params.dict()
    convo_id = payload.get("convo_id", None)
    convo = Convo.objects.get(id=convo_id)
    convo.activated_on = datetime.now()
    convo.save()
    return JsonResponse(serializers.serialize_convo(convo), safe=False)
