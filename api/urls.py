from django.urls import path
import api.views.convo_views as convo_views
import api.views.user_views as user_views


urlpatterns = [
    path("convo_list", convo_views.get_convo_list, name="convo-list"),
    path(
        "convo_list/action/merge_convos",
        convo_views.merge_action,
        name="merge-convos-action",
    ),
    path(
        "convo_list/action/make_public",
        convo_views.make_public_action,
        name="make-convos-public-action",
    ),
    path(
        "convo_list/action/make_private",
        convo_views.make_private_action,
        name="make-convos-private-action",
    ),
    path("convo_list/filter", convo_views.filter, name="filter-convos"),
    path("convo_list/search", convo_views.search, name="search-convos"),
    path(
        "users/authed_user",
        user_views.get_authenticated_user,
        name="authenticated-user",
    ),
    path(
        "convo_list/activate_convo", convo_views.activate_convo, name="activate-convo"
    ),
]
