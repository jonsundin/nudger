# run this script only when you start the database from zero
# if you add a new app to nudger add them below
# create migrations
python manage.py makemigrations convoscript
python manage.py makemigrations quizbot
python manage.py makemigrations sms_nudger
python manage.py makemigrations users

# apply migrations
python manage.py migrate

# if admin creation fails use the command below
# python manage.py migrate auth
