# Sprint Plan 7

A lot of work has continued on Nudger to get it ready to facilitate lesson sequences.  It's important to make sure that the spreadsheet-to-YAML script can build a set of microlessons in an appropriate sequence.  Additionally, the script needs to make sure that it is following the new rules for the YAML format.  The goal for this week is to test and refine the existing script in line with the broader work taking place in Nudger to set us up to work on further logic adjustments in the future.

* [ ] Nudger-Convomeld
	* [ ] Run the [convograph linter](https://gitlab.com/tangibleai/community/nudger/-/blob/staging/quizbot/convograph_linter.py) on the files output from your script.  Make sure that the files pass the linter steps.  May need to meet with Vlad S. to understand how this works.
	* [ ] Test microlesson output in Qary.ai
		* [ ] Generate output for one microlesson > upload > test
		* [ ] Generate output for one grade's microlessons > upload > test
		* [ ] Generate output for two grade's microlessons > upload > test
		* [ ] Generate output for all microlessons > upload > test
	* [ ] Order all the lessons together
		* [ ] May need to add more slashes (prefix grade levels, etc.) to represent sublevels of paths

We would like the mathtext-fastapi application to be able to run on any hosting platform.  As such, we need to Dockerize the application.  There was already a [minimal Dockerfile](https://gitlab.com/tangibleai/community/mathtext-fastapi/-/blob/staging/Dockerfile) before.  We need to update the Docker implementation to work with the existing application.

 * [ ] Mathtext-fastapi: Update the Dockerfile
	 * [ ] Should use Python 3.9 or above
	 * [ ] Should use the pyproject.toml to install dependencies
	 * [ ] Info:
		 * [ ] Current build command: source ./scripts/build.sh ([file link](https://gitlab.com/tangibleai/community/mathtext-fastapi/-/tree/staging/))
		 * [ ] Current start command: uvicorn app:app --host 0.0.0.0 --port 10000 --workers 3 ([file link](https://gitlab.com/tangibleai/community/mathtext-fastapi/-/blob/staging/scripts/build.sh))
 * [ ] Mathtext-fastapi: Update the tests for mathtext-fastapi
	 * [ ] Replace test_text2int.py with a new test class file test_nlu_endpoint.py. (mathtext examples: [test file](https://gitlab.com/tangibleai/community/mathtext/-/blob/main/tests/test_cases_all.py), [csv file](https://gitlab.com/tangibleai/community/mathtext/-/blob/main/mathtext/data/test_cases_all.csv) ) (mathtext-fastapi example: [make_requests.py](https://gitlab.com/tangibleai/community/mathtext-fastapi/-/blob/staging/scripts/make_request.py))
	 * [ ] The test should take in values from a csv.
	 * [ ] The test should compare the expected output from the csv to the output from the nlu endpoint ("data" field)
	 * [ ] The test should output failing cases.
	 * [ ] The test should give an accuracy score.  You can set the threshold for failing based on what seems reasonable from these tests.
	 * [ ] The test should ensure that the response object always has "data", "type", "confidence", and "intents", regard less of the evaluation(s) run
		* NOTE: You may make more than one test for different things (ie., structure and results might have different tests)
