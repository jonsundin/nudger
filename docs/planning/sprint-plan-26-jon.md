# Sprint 26 (June 6, 2023 - June 13, 2023)

### Frontend

**First goal**
- [x] YAML file displayed as minimally styled force directed graph ($100) in d3.js+css+html
- [x] Minimal force-directed graph of static yaml file displayed within django template (nudger/convoscript/templates/convoscript/convograph.HTML)

**2nd Goal**
- [x] Improve styling of graph:  
    - [x] Compute depth and breadth integer with breadth first walk of the graph e.g. start = 0, first question = 1, second question and hint for first question are both depth 2, **next** and **default** do not increment the depth integer  
	- [x] Only dotted integer node id f'{depth}.{breadth}' is displayed on nodes
	- [x] Mouseovers of node reveal state name and utterances for that state
    - [[improve styling of graph 2023-06-13 11.32.33.excalidraw]]
    - [[improve styling of graph 2023-06-13 11.32.33.excalidraw.png]]

**3rd Goal**
- [x] Improve layout of graph
	* ~~creating "strata" of invisible nodes at each depth and breadth integer in a tree~~
	* connect invisible spring (attactor) edges to the corresponding nodes that share those depth and breadth coordinates  
	* adjust force and damping constants  
	* allow user to drag nodes around to untangle the graph

**4th Goal**
- [x] Connect the graph visualization to the Django nudger/convoscript/models.py so that for a fixed (hard coded bot name) a SQL query of the State and Trigger tables retrieves the graph structure for that bot and displays it in the graph
 
**5th Goal**
- [ ] use the users bot name to compose the database query so that a user's bot is the one whose graph is displayed and it is refreshed with a button on the [qary.ai/convoscript/](http://qary.ai/convoscript/) page
  - [ ] Get username first (can use default), then get a Convo using the Convo.objects model, Convo.objects.get and State.objects.mod
  - [ ] /convoscript/views.py -> convograph function: switch from SQL query to Django ORM query.
    - [ ] Use Django QuerySet, which can contain a list of dicts (States and Triggers).
	  - use: queryset.values_list()
  - [ ] two sources of data for the graph view: 
    - [ ] form submission with `Next state` button click
	- [ ] fetching data from sqlite db using the convograph view function
  - [ ] Create a CSS document for graph classes

## Done: Sprint 25 (Sep 09 - Sep 16)


## Backlog


