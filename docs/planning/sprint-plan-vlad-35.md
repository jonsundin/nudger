## Sprint plan 35 (Sep 4, 2023)

- [x] integrate actions
- [x] simplify `quizbot/consumers.py` and `quizbot/actions.py` as good as possible
- [x] simplify widget code (create merge request to `faceofqary`) repo
- [x] naming conventions during all refactor, simplifications ...
- [x] finish developing Api application
- [x] move all non-views functions to api and make them as endpoints
- [x] from bothub and dashboard js files send requests to api endpoints
- [x] refactor `quizbot/consumers.py` + `quizbot/tasks.py`:
  - [x] leave only overriden methods in `QuizRoomConsumer` consumer class
  - [x] remove task from `quizbot/tasks.py` and in consumers use only left one
- [x] move constants module from `quizbot` to `nudger` and fix imports as well
- [x] remove useless modules and parts of code in `quizbot` application


## Sprint plan 36 (Sep 11, 2023)

- [] fix issue with CORS by moving to whitenoise essentially
- [] implement support for actions
