# Sprint 09 Plan


## Sprint 10: May 25 - June 1

### YAML upload/django-CRUD feature (convoscript)

* [ ] V8: yaml upload form/view
* [ ] V20: javascript app for pretty frontend convoscript
* [ ] V8: django views for CRUD (convoscript)
* [ ] G1: meet with Vish to explain how poly_api `models.py` and `views.py` work on moia (and soon on nudger)
* [ ] G1: meet with vish to explain how you integrated poly_api into nudger 

### Staging (nudger.qary.ai)

* [ ] H1: give Greg new sendgrid keys 
* [ ] H4: get qary passing (`from qary.init import maybe_download` fails in nudger)
* [ ] G2: finish debugging greg's branch so that cicd passes tests
* [ ] G4: push/merge (merge requsest already created) to staging and make sure admin interface works for nudger and poly_api apps
* [ ] G1: make sure django poly_api app and your chat view works on a deployed instance of nudger.qary.ai
* [ ] G1: make sure poly_api REST api works on nudger.qary.ai
* [ ] G1: communicate with maria some curl, wget, or postman requests that she can run

### Main (chat.qary.ai)

* [ ] HG4: use terraform to create new droplet with nginx.service and gunicorn.service running
* [ ] HG1: `git push origin staging` and `deploy.sh` with new droplet IP from tf output above to deploy staging to new instance of staging branch


## Sprint 11?

* [ ] HG: integrate User model from sms_nudger into poly_api (just make sure poly_api doesn't have any FKs to a different User model than sms_nudger), deploy and verigy user signup for nudger still works. 
* [ ] HG: create account on render.com and associate it with Maria's account to get free CPU hours
* [ ] HG: create gitlab repo for render.com django experiment
* [ ] HG: add gitlab-ci.yml and blueprint.yml for render.com within django experiment repo for some empty/boilerplate example django app
* [ ] HG: add badge on README.md that tells you when last push to main successfully deployed to render.com
* [ ] HG: gradually incorporate nudger django app into render.com django experiment repo and keep deploying it until deployment breaks
* [ ] HG: attempt to deploy the Docker image for prosocial-ai-playground repo to render.com
* [ ] H2: Merge feature-data-file-extraction and feature-test-creation to main and deploy to render.com or GCP depending on how render.com django and GCP resize experiments above went

## Done Sprint 09: May 16 - May 25

* [x] HG1-.5: push minor HTML change to the staging branch on the nudger repo
* [x] G: copy django polly_api app to nudger repo, merge to staging, and make sure deployment of other sms_nudger app doesn't break
* [x] G: moved new code from MOIA to nudger
* [x] G: add buttons from short yaml, remove timeout

## Done May 9th

* [x] H0.5: Send Greg the latest working short.yml file with buttons and no timeout
    * [moia...v3.dialog.yml](https://gitlab.com/tangibleai/moia-poly-chatbot/-/tree/main/data/moia-poly-dialog-tree-simplified-chinese.v3.dialog.yml)
* [x] H4-4: Manually deploy the recent version of the django app to poly.qary.ai
    * made several attempts, but GCP VM freezes and goes offline each time
    * GCP VM is only 1 core (smallest instance)
    * crash seems to be related to installation of large `qary` dependencies like `pytorch`
* [x] G-2.5: models.py Model class for InteractionLog with static text at top for state currently selected (default = `State(state_name='select-language')` `.text` `user_message = TextField()` and `state_name =TextField()` and possibly (stretch) `state = ForeignKey(State)`
* [x] G-2.5: View with Model-based view/form with two fields (state_name or state) and user_message. (to submit user message (creates an entry into InteractionsLog)
* [x] G-2.5: Same form as above submits message and state-name to next_state function and replies with json dump of the next state message and state name
* [x] G-2.5: Render the state as actual text from the bot to the user below or above the the form
* [x] H2-32: fix qary install on moia-poly-chatbot (mpc)

## Wont Do

* [ ] HG: resize GCP instance and attempt moia redeploy

## Plan May 9 - May 15

* [x] G : Add url parameter to the view that uses the state name (instead of primary key)
* [x] G : Delete the timeout state
* [x] G : Add buttons to `short` yml file 
* [ ] G : Put docstrings in the functions, FIXME for idea to do next 
* [ ] GH: Do code review 


## Backlog 

* [ ] H2: `next_state()` function that works using Django State & Trigger tables
* [x] H1-1: Pair programming session for Hobson and Greg
* [ ] H1: Create Domain table with fields for name, description, tags
* [ ] H1: Create Tag table with fields for name and canonical name
* [ ] H1: Foreign keys to connect Tag and Domain tables to State
* [ ] H4: recover Clynton Zoom videos from old laptop
* [ ] H8: `models.export` function


* [ ] G8: get the CI/CD pipeline passing (see qary/.gitlab-ci.yml)
* [ ] GH4: dump entire firebase data to a raw yaml or json file 
* [ ] GH4: parser to find English dialog in firebase dump to json or yaml file from above



## Backlog 


## Rodri and Girolabs

* [ ] H: Write a script to query Firebase for the next piece of content
* [ ] H: Write a query  for the first 1 steps of English dialog 
* [ ] H: Manually extract some dialog to .yaml
* [ ] H4: create a fresh build of javascript 
* [ ] H8: run the build locally
* [ ]   H1: tutorial on building/running a typescript web form or dialog box
* [ ]   H1: connect built package to django Base.html or Home.html Template
* [ ]   H1: svelte typescript component like tutorial
* [ ]   H1: tutorial on grunt
* [ ]   H1: tutorial on buildpack
* [ ]   H1: tutorial on minify for typescript















## Sprint 4 Tasks

* [ ] M1: List Clynton videos in README
* [ ] G4: Configure `/reply` endpoint so that the output has the format:
{
    state_name: "welcome",
    bot_text: "hello", 
    ...
}
The input is GET request:
{
    state_name: "select-lang", 
    user_text: "english", 
    ...
}
* [ ] G4: Start understanding the v3.preprocess (yml) file and create a function that populates data models.
* [ ] G1: Enable and test the admin interface 
* [ ] G1: Merge request to `moia-poly-chatbot` 
* [ ] G1: Move `next_state` function from `qary` to `moia-poly-chatbot/tests`
* [ ] H2: CI/CD to run `next_state` API test
* [ ] H8: `models.export` function
- [ ] H4: create a fresh build of javascript 
- [ ] H8: run the build locally












## Sprint 03 tasks

- [x] G2-5: install the latest version of qary and make sure you can run the function and get a dictionary 
- [x] G2-5: add qary to environment.yml and requirements.txt using https gitlab package url
- [x] G2-5: urls.py: render json from previous task at an endpoint with url chat.qary.ai/reply 
- [x] G2-5: views.py: render json from qary.skills.poly.reply(GET args)
- [x] G2-6: test the view for 1 turn of the dialog
- [ ] G2: test it for 6 levels of dialog
- [ ] H4: create a fresh build of javascript 
- [ ] H8: run the build locally
- [x] H2-0.5: find and catalog Clynton videos
- [x] H8-12: creating a working qary-based backend
- [x] H2-12: finalize v3 format 
- [x] H2-4: experimented with grad.io to build QA interface
- [ ] H : (nice-to-have): create a view/form that implements the poly frontend 
- [x] M1: connect FirebaseDev4 Firestore to BigQuery  with realtime connection
- [x] M3: export data from FirebaseDev4 Firestore and import it to BigQuery 
- [x] M1: connect BigQuery to DataStudio for `user` and `metrics_prep` objects 
- [x] M1.5: imported `FirebaseDev4` data to `polly-staging-tangibleai` 


Total Hours: 
G - 26
H - 28.5

## Backlog



### Maria

#### Convoscript (Maria)
- [ ] manually enter `moia-poly-dialog-tree*.v3.dialog.yml` data using your GUI for several states
- [ ] export your new dialog tree to `moia-poly-dialog-tree*.convoscript.json`
- [ ] revise python script that converts `*.convoscript.json` to `*.v2.dialog.yml` so that it also converts to `*.v3.dialog.yml`
- [ ] create python script `*.v3.dialog.yml` -> `*.convo.json`
- [ ] ? input v3.dialog.yml ?
- [ ] ? output v3.dialog.yml ?

- [ ] M2: simple connection to firebase from convoscript?
- [ ] H: yulkendy shared access to a list of users for the dashboard

### Django Dashboard (Hobson, John, Greg)
- [ ] H1: organize, rewrite, and estimate tasks for sprint1
- [ ] J4: CI-CD yaml file that deploys after passing tests
- [ ] J1: incorporate it back into nudger
- [ ] J2: Research Azure/Microsoft/Windows/Outlook single-signon to Django (our dashboard) (1)
- [ ] H4: find and download the json/content in firebase
- [ ] H2: find Clynton videos and put in README as links
- [ ] H4: yaml file for the english dialog
    - [ ] H1: levels 0-3
    - [ ] H1: levels 4-5
    - [ ] H1: levels 5-7
    - [ ] H1: level 8
- [ ] H8: yaml file for the chinese+english dialog
    - [ ] H4: levels 0-3
    - [ ] H4: levels 4-5
    - [ ] H4: levels 5-7
    - [ ] H4: level 8

### `Qary` State Machine (Hobs)

Function that takes in a state name and an user utterance and returns a state name and bot utterance

- [ ] qary tests passing
- [ ] release latest tested version to pypi
- [ ] fresh/clean install of qary
- [ ] add qary as dependency in moia-poly-chatbot (and nudger) 
- [ ] fresh install of nudger using README.md
- [ ] fresh install of moia-poly-chatbot using README.md
- [ ] *.v3.dialog.yml loader in qary
- [ ] create dicts/mappings for each language
- [ ] exact lookup for intent matching (NLU) state transitions
- [ ] keyword intent matching 
- [ ] spacy intent matching
- [ ] bert intent matching

### Django (Greg)

- [ ] create local (laptop) environment to test nudger app (conda env create...)
- [ ] models.py for any dialog tree like [this](../data/moia-poly-dialog-tree-simplified-chinese.v3.dialog.yml)
- [ ] admin interface to dialog content
- [ ] create DRF interface to dialog content
- [ ] local postgres connection/database within django settings.py

- [ ] stretch: edit settings.py to connect Django to firebase as a database (don't know if this is possible)

### Frontend Typescript (John)

- [ ] download production `carmenai....min.js`
- [ ] put `carmenai...min.js` into django app in a path like `static/js/carmenai...min.js`
- [ ] download all the dependencies using the console log error messages
- [ ] put all the poly.js dependencies into the appropriate static path
- [ ] paste min.js directly into html template and fix any console.log errors
- [ ] add an {% load static %} or equivalent to the index.html template

### Firebase (John)

- [ ] J2: create copy of minified js file currently served to chat.qary.ai
- [ ] J2: rerun build on staging to create new carmenai.minified.js
- [ ] J4: make data qtn_sheet2_g3 and document,look for links to firebase in typescript
- [ ] J: find list of all tables(schemas) + object-types + db/scollections in firebase
- [ ] MJH: ERD from models  

#### john Devops
- [ ] 4 Staging server new javascript
- [ ] 4 all moia functionality to nudger docs/chat/js/ts from moia -> nudger
- [ ] 4 git branches staging-moia staging-slcc/qary
- Admin login (MS SSO TBD)

#### complete staging system at chat.qary.ai

- [x] staging vm
- [x] ensure staging IP address is static IP address

- [ ] share credentials bitwarden
- [ ] staging database
    - [ ] copy prod database
- [ ] staging branch
    - [ ] change firebase url to point to new firebase database
- [ ] ci/cd
    - [ ] include instructions in pre-commit scripts


### Backlog

- [ ] get clynton to transfer the Dev4 project ownership to us

## by end of month





## References

- [Proposal](https://docs.google.com/document/d/1xPciv_GbKxW2-x0bmK4vYfVlcHU1VHRHoZXKjNPoGwE/edit#heading=h.gjdgxs)
- [MOIA IT Dept Cloud Review](https://gitlab.com/tangibleai/internal/-/blob/master/projects/MOIA/20220113%20-%20MOIA%20-%20POLY%20AI%20for%20chatbot.pdf)



