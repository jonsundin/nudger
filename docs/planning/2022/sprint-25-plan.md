# Sprint 25 (Sep 09 - Sep 16)

* [x] R0.2-0.2: Reduce the header size 
* [x] R1-1: Change the robot icon in the chat widget to Poly
* [x] R1-1.5: Create a script tag that embed the chat widget component in any website
* [x] R1-0.5: Test the tags locally and in production
* [x] R1-1: Consolidate the global css with the build css (within components)
* [x] R1-1: Document the instructions for any developer to deploy our widget on their own website
* [x] R1-2: Serve up the bundle files within the `nudger` app as static files
* [x] R1-1: Do some manual testing to find where the out of order bug occurs in Poly


## Done: Sprint 24 (Sep 02 - Sep 09) 

* [x] R0.5-0.5: Estimate the effort/time you spent on the tasks for the sprint 4
* [x] R0.1-0.1: Rename the `new-ideas.md` file to `backlog.md`
* [x] R0.1-0.1: Move the `backlog.md` file to the `planning/` directory in `nudger` repo
* [x] R0.2-0.2: Make sure there are no tasks or bugs in more than one file
* [x] R0.1-0.1: Replace the name **Purple Alien** with **Poly**
* [x] R0.2-0.2: Replace the cat profile with Poly logo
* [x] R1-1.2: Watch the video again to clearly understand how to update the MOIA report
* [] R2: Try to update the MOIA report to include data up to Aug 31
* [x] R2-2.8: Continue learning and practicing the basics of Django channels/Web Sockets in the [learning repo](https://gitlab.com/rochdikhalid/django-channels)
    * [x] R1-0.5: Basic setup 
    * [x] R0.5-0.2: Create a template for room view
    * [x] R0.5-0.5: Write a consumer 
    * [x] R0.5-0.5: Enable the channel layer 
    * [x] R0.5-R0.1: Rewrite the chat server as asynchronous
    * [x] R1-1: Review and understand what I have learned about Django channels
* [x] R1.5-1: Understand how channels are implemented in `nudger`
* [x] R0.5-0.2: Read and understand what Celery is used for
* [x] R2-1: Practice Celery in the [learning repo](https://gitlab.com/rochdikhalid/dj-celery)
    * [x] R0.1-0.1: Create a new repository named dj-celery and clone it
    * [x] R0.1-0.1: Create a virtual environment and install dependencies
    * [x] R0.1-0.1: Create a Django project named simpletask
    * [x] R0.2-0.1: Incorporate Celery into the Django project
    * [x] R0.2-0.1: Configure Redis server
    * [x] R0.1-0.1: Create a new app called sendmessage
    * [x] R0.5-0.2: Create Celery tasks and test them
* [x] R1-0.5: Fix the bug that causes the appearance of Poly triggers (buttons) before the bot text
* [x] R1-1: Test manually the bug fix locally and in production
* [x] R0.1-0.1: Estimate the points on the tasks you've put in the sprint plan 5

> Learning NLP with **NLPiA**

* [x] R1-2: Add some learning tasks to read chapter 2 in **NLPiA**
    * [x] R2-1.5: Read and understand **2.2 Building your vocabulary with a tokenizer**
    * [x] R1-0.5: Read and understand **2.2.1 Dot product**
* [x] R1-1.5: Do an exercise to create BOW vectors for each document in the MAITAG (Maya) dataset of anonymized utterances
