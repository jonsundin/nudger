# Sprint 37 (Dec 02 - Dec 09)

## High priority
* [x] R0.5-0.2: Review Cetin's merge request and merge it into `main`
* [x] R0.5-0.5: Generate new bundle files that connects to `nudger/quizbot`'s web socket
* [x] R2-2.5: Help Cetin to deploy `quizbot` to qary.ai and get the widget working
* [x] R1.5-1.2: Create MOIA report analysis for November
* [x] R0.2-0.2: Replace the front image with "for testing only"
* [x] R0.2-0.2: Replace the text **We Speak NYC** with **We Test Websites**
* [x] R1-0.9: Keep only the `index.html` and few pages in the [test-poly-wespeaknyc](https://gitlab.com/tangibleai/test-poly-wespeaknyc) repository
* [x] R0.2-0.2: Go to the **wespeakny.cityofnyc.us** website and reproduce the [Bug 1](https://gitlab.com/tangibleai/moia-poly-chatbot/-/blob/secure/docs/deploy-bugs.md#bug-1-1)
* [x] R0.1-0.1: Send wespeaknyc.qary.ai to someone to test the widget on Windows

### Done Sprint 36 (Nov 25 - Dec 02)

* [x] R1-0.5: Create a document called `deploy.md` for MOIA team
* [x] R0.5-0.2: Download the source code of the wespeaknyc.cityofnewyork.us website
* [x] R2-2: Examine the `index.html` and fix the URL/file paths to get it working locally
* [x] R1.5-2: Deploy the downloaded wespeaknyc.cityofnewyork.us website
* [x] R0.2-0.2: Embed the widget into the downloaded web page
* [x] R0.1-0.1: Create a new file called `deploy-bugs.md`
* [x] R0.1-0.2: Test the widget by opening the web page and having two interactions 
* [x] R2-2: Test for one or two hours in order to search for new bugs
* [x] R1-0.2: Add one more interaction to the integration test
* [x] R1-0.8: Add three more intractions to the integration test
* [x] R0.1-0.1: Add delay before every interaction
* [x] R0.2-0.2: Try to run integration tests in two terminals
* [x] R0.5-0.5: Launch the integration tests in five or seven terminals