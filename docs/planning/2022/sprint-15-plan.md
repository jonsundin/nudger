# Sprint Plan

## Sprint 15: June 27 - July 4

* [ ] H1: Research Svelte
* [ ] Update yml file (based on tasks below)
* [ ] G: Feature 1: buttons (turn full to v3)
* [ ] G: Feature 2: pagination of messages (turn v3 to v4) (en: dictionary)
* [ ] G: Feature 3: logging all state variables (entire context [request]) - user id (store json b-field)
* [ ] H1: Set up Sentry and send Greg
* [ ] G: Set up a pair programming session with John
  - send agenda to John
  - deploy playground backend on render.com
  - set up a DNS for playground
* [ ] Get all the code that needs to move over


### Done
#### Sprint 14: June 20 - June 27

* [X] G1-3.5: Configure Sentry and put into existing API
* [X] G2: Make the backend work for a proper json= data request
* [X] G4-6: In the view, query the API
* [X] G1-0.25: Change repository for render.com nudger test site to Staging
* [X] G2-0.5: Add DNS entry on Digital Ocean for www.qary.ai (Maria will use /reply/package/ endpoint)
* [X] G4-0.5: Within the render server, set up let's encrypt (certbot) (assign a domain name to a server, using let's encrypt via certbot)(www.qary.ai | qary.ai)
* [X] G-2: Refined API endpoint and cleaned up code

#### Sprint 13

* [X] H1-1: Sent a formatted JSON statement to Maria with input and output (json.dumps)
* [X] GX-9: Revise API to use user_message and state_name to return response (throw error if response isn't acceptable)
* [X] GX-1: Write tests for next-state (/reply/packages/) API endpoint

#### Sprint 12

* [X] G2.5: GET request that allows you to interact with a bot to get the next state (request needs to run over a REST view / same fields send in a get request to API URL / respond with JSON)
* [X] G1: Made an API endpoint that send all the material for the current state for the frontend
* [X] G4-1: Follow TDD tutorial (once on theirs, attempt on my own w/o looking at answers)
* [X] G4: Add a textbox as an alternative to the buttons (hit submit without hitting buttons / select both text and buttons - need business logic - What to do?)
* [X] G0.5: Added CICD pipeline badge to the README.md
* [X] G1.5: Added comments to most classes/functions
* [X] G6: Change the RadioSelect widget template to custom template
* [X] G0.5: Added automatic submission of chat interface form fields
* [X] G0.5: Retrieve blueprint.yml from manzana app
* [X] G(with above): Make a blueprint.yml file for render.com (based on Rochdi's) and set up to execute

#### Sprint 10 and Sprint 11

* [X] G1: meet with Vish to explain how poly_api `models.py` and `views.py` work on moia (and soon on nudger)
* [X] G1: meet with vish to explain how you integrated poly_api into nudger
* [X] H1: give Greg new sendgrid keys
* [X] H4: get qary passing (`from qary.init import maybe_download` fails in nudger)
* [X] G2: finish debugging greg's branch so that cicd passes tests
* [X] G4: push/merge (merge requsest already created) to staging and make sure admin interface works for nudger and poly_api apps
* [X] G1: make sure django poly_api app and your chat view works on a deployed instance of nudger.qary.ai
* [X] G1: make sure poly_api REST api works on nudger.qary.ai
* [X] G1: communicate with maria some curl, wget, or postman requests that she can run
* [X] updated chat interface CSS styling and the interaction history
* [X] HG: create account on render.com and associate it with Maria's account to get free CPU hours

### Backlog

#### YAML upload/django-CRUD feature (convoscript)

* [ ] V8: yaml upload form/view
* [ ] V20: javascript app for pretty frontend convoscript
* [ ] V8: django views for CRUD (convoscript)

#### Main (chat.qary.ai)

* [ ] HG4: use terraform to create new droplet with nginx.service and gunicorn.service running
* [ ] HG1: `git push origin staging` and `deploy.sh` with new droplet IP from tf output above to deploy staging to new instance of staging branch
* [ ] Edit the view for the root URL to reflect general ki information (links to docs, gitlab, etc.)

#### Other

* [ ] HG: integrate User model from sms_nudger into poly_api (just make sure poly_api doesn't have any FKs to a different User model than sms_nudger), deploy and verigy user signup for nudger still works. 
* [-] HG: create gitlab repo for render.com django experiment
* [ ] HG: add gitlab-ci.yml and blueprint.yml for render.com within django experiment repo for some empty/boilerplate example django app
* [ ] HG: add badge on README.md that tells you when last push to main successfully deployed to render.com
* [ ] HG: gradually incorporate nudger django app into render.com django experiment repo and keep deploying it until deployment breaks
* [ ] HG: attempt to deploy the Docker image for prosocial-ai-playground repo to render.com
* [ ] H2: Merge feature-data-file-extraction and feature-test-creation to main and deploy to render.com or GCP depending on how render.com django and GCP resize experiments above went



### References

* [Demo Website](https://nudger-test-tusf.onrender.com/reply/poly/interactive)
* [Test-Driven Development Tutorial](https://brntn.me/blog/django-tdd-practice-time-api)
