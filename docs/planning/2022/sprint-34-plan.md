# Sprint 34 (Nov 11 - Nov 18)

* [x] R0.1-0.1: Create a branch called `fix-outoforder` to fix the OOO bug
* [x] R2-2.5: Take a tutorial to learn how to create an integration test
* [x] R2-2: Create a simple integration test for MOIA to be tested automatically after every deployment (check your notebook for more details) 
* [x] R1-1: Incorporate the `poly_api` app into the secure branch 
* [x] R0.2-0.1: Rename the `poly_api` to `convoscript`
* [x] R0.2-0.1: Rename the `poly` to `convosite`
* [x] R1-0.5: Fix the `QaryMessage()` validation and JSON serialization issues
* [x] R1-0.5: Remove delay from the consumer
* [x] R1: Check for the sequence number of messages in the frontend
* [x] R1: Use the sequence number to display messages in correct order
* [x] R1: Simplify the ChatConsumer class to make readable by having less code 
* [x] R0.2-0.1: Create an app called `monitor` 
* [x] R1-0.2: Create an HTML template and view for the PDF report
* [x] R1-0.5: Embed the MOIA report of October into the view `wespeaknyc.qary.ai/monitor`
* [x] R0.5-1: Merge the new changes of `fix-outoforder` into the `main` branch
* [x] R2-2: Add some improvments the chat messages flow and update the Svelte bundle

## Done Sprint 33 (Nov 04 - Nov 11)

* [x] R1-1: Create a md file that includes the MOIA report analysis notes of October
* [x] R0.1-R0.1: Share the file with Hobson and Maria for getting feedback