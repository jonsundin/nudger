# Sprint Plan

* [X] Implement email to the Pulsar
* [X] Merge staging into main after checking it
* [X] Create a container for nudger together with redis and celery
  - Celery finished
  - Redis is not possible, render.com does not allow to install packages inside container
* [X] Fix the URL bug in the convo creation script
