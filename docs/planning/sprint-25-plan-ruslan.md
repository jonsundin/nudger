# 2023-06-13 convoscript graph merge sprint-plan.md

latest plan: https://gitlab.com/tangibleai/community/nudger/-/blob/main/docs/planning/sprint-25-plan-ruslan.md

Tangible AI will provide you with 2+ "linear" conversation scripts similar to what a client might provide as "happy path" user stories or example conversations from their chat logs (see below).
Your script will be designed merge linear scripts with a conversation network/graph defined in a yaml file.

#### Definitions

**script**: linear script or chat log of a single conversation between a human and a bot, as plain text or list of strings.
**conversation graph**: directed cyclic graph where the edges are triggers or utterances by the human, and the nodes are states or messages from the bot
**thread**: linear conversation graph with no branches, only __next__, __default__, and one trigger per state - stored in a list of nested dictionaries or a yaml file

1. `thread = script_to_graph(script)`
  * 1.1. make sure it has one "start" state
  * 1.2. add "stop" state
  * 1.3 create a linear network/graph yaml file from a linear text script using just the triggers and text 
    * 1.3.1. split utterance sequences into separate states with __next__ triggers between them.
    * 1.3.2. add __default__ triggers that loop back to the existing state
    * 1.3.2. if it does not have a "start" state, create a donothing start state
2. `graph = merge_threads(left_thread, right_thread)`
  * 2.1. input 2 graphs created with feature 1
  * 2.2. merge linear graphs together
    * 2.2.1 whenever you encounter a duplicate sequence of states (sequences of identical utterances by the bot) they should not be duplicated in the merge graph
  * 2.3. add a new trigger for any new utterance by the human
  * 2.4. test that the merge function works for identical scripts to confirm that no new state names or triggers (edges) are created)
  * 2.5. test the merge function on a graph with only one new trigger/branch
  * 2.6. test the merge function on a graph with a new "hint" or state that includes a new state in the middle of 3 states (bot messages)
3. `list_of_threads = extract_threads(graph)`
  * 3.1. create graph walking algorithm that enumerates (explores) all the nodes and edges that reach the the "stop" node
  * 3.2. output a list threads, one for each graph pathgraph that reaches the stop leaf
  * 3.3. merge two threads using algorithm 2 above, then try to merge another thread again (duplicate) to ensure that it doesn't create new states
  * 3.4. merge a new thread with the existing graph
  
#### Example linear conversation script in text format 
```text
teacher: Let's learn how to count by 1s. I will give you three numbers and you tell me what the next number is. If I say '2, 3, 4' then you should say '5'
teacher: 1, 2, 3?
student: 4
teacher: Perfect!
teacher: 11, 12, 13?
student: 14
teacher: Good job!
teacher: 97, 98, 99?
student: 101
teacher: Not quite. Try again.
student: 102
teacher: That's still not right.
teacher: Notice that 98 is one more than 97. And 99 is one more than 98.
teacher: So the answer to 97, 98, 99 is the number that is one more than 99.
teacher: 97, 98, 99?
student: 100
teacher: Excellent work!
```

## Parse linear convsocript ($100?)

- python code to parse a script "teacher: 1,2,3\n student: 4\n teacher: 11,12,13 student: 14\n..."""
- output a yaml file similar to those in nudger (list dictionaries containing states and triggers) - nodes=bot-statements or states, edges=student statements or triggers
- you can use a unique integer to create a state name (e.g. state01, state02, ...)
- make start/stop states empty and always present, filling the states inbetween

## Merge linear convoscripts ($100?)

- convert two scripts to two yamls
- combine two yamls into one yaml
- whenever a new variation of a user utterance (trigger) is found for an identical state (chatbot message) then append it to the triggers for that state

## Detect semantically similar bot messages and user utterances ($100?)

- improve the exact string match comparison of identical states (bot statements) using cosine similarity of SpaCy or BERT embedding vectors
- improve the exact string match comparison of identical triggers using SpaCy or BERT embedding vector cosine similarity
- combine two yamls into one for bot messages or user utterances that mean the same thing but have different words (e.g. "Great job!", "Good work!", "Right!", "Correct!")
- Detect when bot state message (action) appears to mean the same thing  using SpaCy cosine similarity for vector embedding of the utterances
- TBD: determine the appropriate way to add the new (similar) bot message to the list of actions for that original state such that it creates a random message from the bot for the associated trigger message from the user

## state name and ID ($100?)

- create semantic ID in the format f'{depth_int}.{breadth_int}' to represent state (in addition to state name)
- add id to all yaml files
- assist frontend developer (Jon Sundin) in creating dynamic d3 visualization of conversation graphs utilizing these ID numbers

## Task 4 ($100?)

- automatically create a state name from the first N words in the chatbot message to create unique name for every unique chatbot message (state)
- if the bot message at the end (a leaf) of one conversation plan (yaml file) is the same as the __start__ state message of another conversation plan (yaml file) then combine them end-to-end while adding a state-name prefix for a "bot name" based on the file name of the yaml file
- find the end state of each yaml file and connect it to the start state of the appropriate subsequent microlesson.

The microlessons should be combined in the following order so that a new student can progress from easiest to hardest: 

```text
data/countByOne_0003.yml

data/multyplByTwo_0022.yml
data/multyplByThree_0003.yml
data/multyplByFour_0004.yml
data/multyplBySix_0006.yml
data/multyplBySeven_0007.yml
data/multyplByEight_0008.yml
data/multyplByNine_0009.yml

data/DevByOne_0001.yml
data/DevByTwo_0002.yml
data/DevByThree_0003.yml
data/DevByFour_0004.yml
data/DevByFive_0005.yml
data/DevBySix_0006.yml
data/DevByEight_0008.yml
data/DevByNine_0009.yml
```

# Task 5 ($100)

- Concatenate approximately 100 yaml files from the Rori v2 conversation design (provided by another developer, Vlad Z) into a single conversation graph
- integrate and test the conversation design into qary.ai (gitlab.com/tangibleai/community/nudger) chatbot platform (upload yaml) home page.
