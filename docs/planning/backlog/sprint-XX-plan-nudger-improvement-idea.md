# Nudger improvement ideas

## High priority

* [] R2: Create a descriptive **README** for users/developers to setup and use `nudger`
* [] RC?: 
* [] RC?: 
* [] R?:
* [] C?:

## Medium priority

* [] RC?: Exception inside chatbot models 'context_functions'. 'NoneType' object has no attribute 'keys'. Before, it was masked with a 'pass', replaced it with a 'print' now too much exception happening. Something might be wrong with the logic.  

## Low priority

* [] R?: 
* [] C?:
