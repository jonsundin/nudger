# Logs and Monitoring

We use Grafana Cloud for performance monitoring of our servers.
Crude analytics are also possible.

- [On Ubuntu](https://lindevs.com/install-promtail-on-ubuntu/) laptop or server install [Promtail](https://grafana.com/docs/loki/latest/clients/promtail/) a Grafana/prometheus client for transmitting logs
- Figure out how to install Promtail on render server(s) using build.sh and start.sh scripts
- Create grafana cloud account and connect it to tangibleai.grafana.net (grafana.com/orgs/tangibleai/)
- connect nudger Django/Daphne server logs to [Grafana Loki on our org account](https://grafana.com/orgs/tangibleai/hosted-logs/348080#sending-logs) (get API keys)
- connect nudger Celery Worker server logs to [Grafana Loki on our org account](https://grafana.com/orgs/tangibleai/hosted-logs/348080#sending-logs)
- connect nudger redis server logs [Grafana Loki on our org account](https://grafana.com/orgs/tangibleai/hosted-logs/348080#sending-logs)
