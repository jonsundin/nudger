# Sprint Plan

[X] Fix CORS bug
[X] Fix Deployment bug related to the default convo creation
[X] Create consumer test within django
[X] Implement redis and celery to the GitLab CI/CD
[X] Define log level for Django
[X] Create an additional service to test production and staging servers
[X] Create a CRON job
[X] Implement coverage badge to readme file in GitLab, and test passed badge
