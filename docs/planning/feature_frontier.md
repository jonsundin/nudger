# Feature Frontier

The edges of our feature space that we are expanding to add new capabilities.

Suggested next steps have `- [ ]` prefix
Longer term ideas have `- ` prefix



- pre-defined actions (functions) and variables
	- [ ] extract_proper_nouns integrated into demo.yml
	- [ ] extract_lang integrated into a working demo.yml
    - [ ] C: get_wikipedia_summary(user_text='Barack Obama')
    - [ ] C: send_image(image_url=...)
    - [ ] H: actions.wait_for(dict(wait=1))  # waits one second before performing next action
	- [ ] H: extract_email integrated into a working demo.yml
	- [ ] H: actions.extract_url integrated into a working demo.yml
	- actions.signup_email
	- actions.signup_email_username
	- actions.summarize
	- extract_text integrated into a working demo.yml - persist a `text`
	- extract_bad_words
	- extract_command - bash-like command such as `wiki obama`
	- extract_emotions/extract_intents
- custom user variables in templates
    - [ ] V: "You have selected the language {{lang or language}}"
    - [ ] V: "The first extracted proper noun is {{proper_nouns[0]}}"
    - [ ] V: "The extracted proper nouns are {{proper_nouns}}"
    - [ ] V: "Your username is {{username}}"
    - [ ] V: "Your email is {{email}}"
- chatbot user profile/variables (like language choice or name that persists across sessions)
    - [ ] V: Save context as JsonField in database after every action and trigger
- LLM integration
    - H: actions.reply_with_llm
    - actions.filter_prompt_injections
    - actions.
- Messaging (SMS/email/slack) integration
    - C: actions.send_email
    - actions.send_sms
    - twilio can send message to user
- visual builder (graph)
    - J/Vish: Fix convoscript form to connect to a convo ID and state name within the DB
    - J: GET params with convoid and statename to render page for that state and convograph: `qary.ai/convoscript/?convoid=123& statename=start`
- chatbot widget
    - [x] V: handle asynchronous messages and ensure they are presented in the correct order 
- accessibility
    - connect to SMS
    - make useable for the blind with screen readers
    - Voice interface?
- payment / subscriptions
- account management
    - User can upload an images for use in convo design or as chatbot avatar ()
- bot templates
    - [ ] V: demo.yml with buttons, get_proper_nouns, and variables in bot messages
    - C: demo_image.yml with HTML/frontend display of image from external URL or the user's uploaded media: https://gitlab.com/tangibleai/community/convohub/-/issues/51  
    - syndee with 
- platform security
    - when a user attempts to edit a public bot it is copied and they are given ownership of a "fork" and the copy defaults to public
    - ensure only authorized users can read or write bot content that they own
    - verify e-mails during signup
    - user can generate API_KEY
- bot project dashboard / management
    - [ ] V: action in pulldown to enable edit (triggering GET request to convoscript)
    - action in pulldown to enable merge 
- social (bot sharing, remixing, contributing, etc.)
    - V: action in pulldown to generate URL to a user's faceofqary demo page
- multilingual site (platform)
    - G: follow Django tutorials on internationalization and use of gettext strings `_"Hola!"` and `_f"Hola {{name}}"`
- multilingual bot development
    - V: demo.yml that allows user to select lang=Ukrainean and subsequent content is in Ukrainian 
- analytics dashboard (for each bot project)
    - V: convohub saves userid, timestamp, context (jsonfield) in a new ContextLog table, after each transition
    - M: Delvin can ETL the messages in ContextLog
    - V: convohub logs message/context objects in Delvin's preferred logging service endpoint (e.g. Kafka, Firebase, Redis, BigQuery)
    - M: Delvin queries the ConvoHub ContextLog in realtime or with batch processing to have near real-time analytics
    - H: Admin has dashboard to select and monitor any single chatbot room
    - H: Admin dashboard shows list of all current convo sessions and their stats
    - H: Admin can select a convo on their dashboard and see the convo live
    - H: Admin can enable "wait for admin" on a convo monitoring view that allows them to edit and send bot messages.
    - H: Admin can select/enable a timeout on the "wait for admin" feature
- documentation / copy
    - Front page explains what ConvoHub is and how to get started
    - Front page includes suggestions of things to say to the front page chatbot to start learning how it works
- developer documentation (terms, procedures, etc.)
    - link to software LICENSEs on front page footer
    - link to ToS on front page footer
    - Privacy Policy on front page footer

