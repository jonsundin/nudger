
## Background and Overall Goal
The Rori content team provides a lot of the math quizzes in CSV format.  It can be easy for conversation designers and content developers to work with the CSV format for individual quizzes.  However, we need to convert the quizzes into a format our chatbots can read.  The Nudger chatbot stack uses YAML as the foundational data structure for conversational content.

We'd like you to develop a script that can convert math quiz CSV files into YAML files.

## Sprint Plan 1 Tasks
* [x] G: Post Sprint plan to Nudger repository
* [x] G: Give Vlad access to the Nudger repository (if not already done)
* [x] G: Make sure Vlad has access to slack channel
* V: Get familiar with YAML format
* V: Get familiar with the [Rori Quiz format](https://drive.google.com/file/d/10AnDwtdsp5EnlneG_QqE1iBzXIerYd1Q/view?usp=share_link)
* V: May benefit from a meeting with Cetin about the project in general.  Feel free to schedule one with him in Slack if you'd like additional clarification or overview of Nudger
* V: Get the Nudger repository set up and running on your local machine
* V: Get familiar with the Rori microlesson content / structure
* V: Write a script that can convert the [math quiz CSV file](https://drive.google.com/file/d/10AnDwtdsp5EnlneG_QqE1iBzXIerYd1Q/view?usp=share_link) into YAML
	* Read one tab from a CSV into the script (later we will need to handle more than one tab)
	* Identify the "states" of the conversation script (directions | start,  questions | answer)
	* Convert a given state into a YAML block that handles when the student responds correctly (2 + 2 = 4) (look at "_ next _ ") keyword
	* Adjust the conversion code to handle when a student enters the wrong input (2 + 2 = 5) (look at "_ default _ ")

* **Note:** Feel free to break down tasks differently or adjust the task(s) as you encounter issues you think need to be addressed.  The above is just to get you started.

## Resources
* [Nudger Repository](https://gitlab.com/tangibleai/nudger)
* [Example Converted Quizzes](https://gitlab.com/tangibleai/nudger/-/tree/main/data)

