## Sprint Plan 6 Tasks

* [ ] Continue working on refinements to the spreadsheet-to-yaml file converter
* [ ] Test out Rori periodically from UTC 13:00-18:00 (whenever you are available)
	* [ ] Try to video record your sessions.
	* [ ] If you receive, "Apologies, it looks like I am having some trouble working with your answer, it seems I am not functioning correctly." during your testing, try to send the video and timestamp to me. (after testing)
* [ ] Get a copy of `mathtext` working locally on your machine
* [ ] Get a copy of `mathtext-fastapi` working locally on your machine
* [ ] Get a copy of [Argilla](https://argilla.io/) working locally on your machine
