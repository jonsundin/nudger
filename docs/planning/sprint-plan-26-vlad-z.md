## Background and Overall Goal
The Rori content team provides a lot of the math quizzes in CSV format.  It can be easy for conversation designers and content developers to work with the CSV format for individual quizzes.  However, we need to convert the quizzes into a format our chatbots can read.  The Nudger chatbot stack uses YAML as the foundational data structure for conversational content.

We'd like you to develop a script that can convert math quiz CSV files into YAML files.


## Sprint Plan 2 Tasks
* [x] G: Collect various quizzes with example formats into a new spreadsheet
* [ ] VZ: Push the current version of the script to the nudger repo `scripts` folder
* [ ] VZ: The script converter should be able to handle multiple answer types
* [ ] VZ: Play with the Rising Academies Rori bot ([https://wa.me/12065906259?](https://wa.me/12065906259?)) to understand how microlessons work and connect together
* [ ] VZ: The script should be able to work with [multiple tabs of a spreadsheet](https://docs.google.com/spreadsheets/d/1R_uiRF4OeE0Ev0blEzISpl1ixmxqhq94/edit?usp=drive_link)
	* [ ] Convert each tab in the Excel file into the YAML format  (How might we retain the ability to use either a csv for a single microlesson or an Excel format for single or more microlessons?)
	* [ ] To avoid collision of state names, prepend the microlesson title at the start of the state name (ie., {microlesson_code_start})
	* [ ] Concatenate the individual YAML-formatted microlessons together into one long file
	* [ ] Ensure there is an appropriate transition between the individual YAML-converted microlessons (like the Rori bot)
	* [ ] Ensure the transition between microlessons is done through the -next- trigger
* [ ] VZ: Error handling  (Note that tab `G6.N1.3.6.2` in [the updated spreadsheet](https://docs.google.com/spreadsheets/d/1R_uiRF4OeE0Ev0blEzISpl1ixmxqhq94/edit?usp=drive_link) has errors)
	* [ ] The script should check that all columns have the necessary information (ex. Question number or question)
	* [ ] The script should throw an error if the script cannot be processed
		* [ ] Error example: Column C is missing a row label (ie., Q7)
	* [ ] The script should throw a warning message (or collect all warnings into a single message)
		* [ ] Warning example: The answer value seems to be in the wrong format (ie., Column E: c > C)
		* [ ] Warning example: The question seems to be missing a hint
* [ ] VZ: Share the final version of the script output with Greg by putting it in [this Google Drive folder](https://drive.google.com/drive/folders/1QpIWM9r6NQ-fzNyqnBB7lD400AQ8iCUa?usp=sharing)
* [ ] VZ: Update the script in the nudger repo `scripts` folder with the changes after Sprint Plan 2 is complete (Feel free to push more frequently, even as you are still working out bugs)


## Sprint Plan 1 Tasks
* [x] G: Post Sprint plan to Nudger repository
* [x] G: Give Vlad access to the Nudger repository (if not already done)
* [x] G: Make sure Vlad has access to slack channel
* V: Get familiar with YAML format
* V: Get familiar with the [Rori Quiz format](https://drive.google.com/file/d/10AnDwtdsp5EnlneG_QqE1iBzXIerYd1Q/view?usp=share_link)
* V: May benefit from a meeting with Cetin about the project in general.  Feel free to schedule one with him in Slack if you'd like additional clarification or overview of Nudger
* V: Get the Nudger repository set up and running on your local machine
* V: Get familiar with the Rori microlesson content / structure
* V: Write a script that can convert the [math quiz CSV file](https://drive.google.com/file/d/10AnDwtdsp5EnlneG_QqE1iBzXIerYd1Q/view?usp=share_link) into YAML
	* Read one tab from a CSV into the script (later we will need to handle more than one tab)
	* Identify the "states" of the conversation script (directions | start,  questions | answer)
	* Convert a given state into a YAML block that handles when the student responds correctly (2 + 2 = 4) (look at "_ next _ ") keyword
	* Adjust the conversion code to handle when a student enters the wrong input (2 + 2 = 5) (look at "_ default _ ")

* **Note:** Feel free to break down tasks differently or adjust the task(s) as you encounter issues you think need to be addressed.  The above is just to get you started.


## Resources
* [Nudger Repository](https://gitlab.com/tangibleai/nudger)
* [Example Converted Quizzes](https://gitlab.com/tangibleai/nudger/-/tree/main/data)
