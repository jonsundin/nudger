# Changelog

## 0.0.6 

2023-06-03

- move and disable chat_async/tests.py
- delete chat_async app: A websocket application works with the chat widget.
- delete chat app: A websocket application works with a view, where you enter messages in a text box.
- delete sms_nudger app: An application to send sms, email.
- delete faqbot: An application to store questions and answer pairs in a database, and displays all of them as a list view.
