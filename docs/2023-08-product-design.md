# Qary.ai product design

Here are some qary.ai chatbot platform product design documents by Greg:

* [Wireframes](./nudger-wireframes-v1-8.8.pdf)
* [Market research](https://docs.google.com/spreadsheets/d/1tCLjFZP1h1PAwwNLlFsf1DDdlIBTgWzaWRsP_NeyGJE/edit?usp=sharing)
* [Product feature stories](https://docs.google.com/spreadsheets/d/1LxOce0Ng2Zyof4xKp5zZKXh0Lxo-qLaxdq65knDx65E/edit?usp=sharing)
