# Developer Workflow

First, make sure you have your tools set up:

- Install and configure Python (preinstalled on most OSes)
- Windows: git-bash (a POSIX compliant shell)
- Windows: install and and configure Anaconda
- [Set up your environment](#set-up-your-environment)
- [Set up git](#set-up-git)
- Future work: set up Docker, Ansible, Terraform, Render SDK, Digital Ocean SDK, and/or Kubernetes

Now you're ready to build some software:

- [Start a new task](#start-a-new-task)
- [Implement a new feature](#implement-a-new-feature)
- [Merge request](#merge-request)
- [Deploy to Render](#deploy-to-render)

To support your team, when you are between tasks:

- Study the [Cardinal rules](#cardinal-rules)
- Use the cardinal rules to [Review a merge request](#review-a-merge-request)
- See if we're ready for a [Merge to main](#merge-to-main)

## Set up your environment
The bash script `source scripts/build-start-dev.sh` should accomplish these steps on a linux system.
1. Clone the repository
2. Install redis
3. Create and activate a Python virtual environment
4. Install nudger and all it's dependencies and data files (use --editable)
5. Make sure you can run the celery server (see `build-start-dev.sh`) and leave it running in the background
6. Make sure you can launch the django web server (`gunicorn` or `runserver`) and leave it running in the background
7. Make sure you can browse to localhost:8000 or 127.0.0.1:8000 and interact with the chatbot

## Set up git
1. Make sure you get to see merge conflicts during `pull`s: `git config --global pull.ff only`
2. Use a name that others on the team will recognize: `git config --global user.name "Hobson"`
3. Use the same email you use in GitLab, preferably a TangibleAI.com address: `git config --global user.email "user@tangibleai.com"`
4. Use the same email you use in GitLab, preferably a TangibleAI.com address: `git config --global user.email "user@tangibleai.com"`


## Start a new task
1. Search the existing GitLab issues for the feature you have in mind: https://gitlab.com/tangibleai/community/nudger/-/issues
2. Edit or create an "ticket" (GitLab Issue) 
3. Add a doctest or docstring to your code or the ticket
  - Explain the inputs, outputs, and side effects of a function
  - Explain how the UX will change
  - Explain how to manually test the new feature once you're done
  - If you have a doctest, make sure your module (py file) is listed in `tests/test_doctests.py`
4. create a `feature-*` or `fix-*` branch using the suggestion in your GitLab Issue or using git: `git checkout staging -b feature-whatever` 
5. `git pull origin main`
6. `git pull origin staging`

## Implement a new feature
1. change the code
2. run `black .`
3. Only if you have created a new file should you use `git add path/to/file.py`
  - Rather than add large compiled files (>5 MB) consider creating a downloader to retrieve them from object storage.
  - For large Javascript/Typescript project bundles (`bundle.js`) consider adding them to a free CDN or Digital Ocean object storage.
  - For large datasets (`*.csv.gz`), consider creating a Python object storage downloader to run when your module is imported. See `spacy_language_model.py` for an example.
4. `git commit -am 'short message'`
5. git push -u origin feature-whatever-working-on  # (or `git push` if second time)
6. write a minimal test, e.g. import your module file within a django test (tests/test_*.py)
7. `python manage.py test -vvv`
8. start local server and manually test: `source scripts/build-start-dev.sh`
9. `git pull origin staging`
10. `git push`
11. run tests locally: `python manage.py test`
12. go back to step 1 to fix tests or improve your feature until you acheive a minimal improvement to the UX

## Merge request
1. Create merge request from your branch to staging (Source: feature-whatever, Target: staging) using the alert popup in GitLab or manually here: https://gitlab.com/tangibleai/community/nudger/-/merge_requests/new
2. Create a short title that starts with DRAFT if not ready to merge or FIX if fixes a bug
3. Select a template that matches your needs
4. Ensure a line at top `f"fixes #{{issue_number}}"` (e.g. "fixes #42") for any associated gitlab issues
5. Assign yourself as the "Assignee" who will ultimately be responsible for the merge and deployment to staging
6. Add at least one reviewer.
7. Create another branch off the existing feature so you can keep working while waiting on reviewers: Go to [start a new task](#start-a-new-task)
7. Implement any suggestions/comments by reviewers (adjusting them based on your understanding of how the feature is supposed to work).
8. Wait for at least one Approval by somone besides yourself.
9. Before hitting the merge button check to see that the staging .env files are ready - SEE [Deploy to Render](#deploy-to-render) below
10. Click the blue Merge button!

## Deploy to Render
Once your MR is merged, your code will auto deploy
If you have changed any `os.environ` variables or `.env`/`.env_example` files the changes are also propagated to:
- [ ] `.env_example` # WARNING: Do not put secrets/passwords/tokens here!!!
- [ ] BitWarden: A "Secure note" named `Nudger .env staging` - add a date to the name if you have changed anything
- [ ] Render: `.env` file in [environment group](https://dashboard.render.com/env-groups) named [`staging.qary.ai`](https://dashboard.render.com/env-group/evg-cibc3h95rnuk9q80pfrg) 

## Review a merge request
Support your team with timely code reviews and merge request approvals.

1. Review the files that have changed looking for possible bugs
2. Suggest no more than one or two readability or style improvements focusing on functionality and objective measures of readability (Linter and _Clean Code_ guidelines) rather than your particular style or patterns you perfer
3. Hit the Approve button. Your approval means:
  - [ ] You assume the Assignee will implement your suggestions.
  - [ ] You think the merge request will pass tests once your suggestions are implemented
  - [ ] You think the implementation meets the requirements/documentation specified in the Issue or Feature description or docstring(s)

## Merge to main
Whenever everyone has manually tested the new features on staging and is confident that the new code will work on the main branch, you can create a merge request from `staging` to `main` - [Merge request](#merge-request), replacing `staging` with `main` or `production` as appropriate.

## Cardinal rules
- Don't `print()` anything in Django apps or tests!
- Anything that affects the code or data for the project should be reviewed and approved
- Use a linter (Anaconda/Black/Flake8) that warns you of common errors such as unused imports, and undefined variables.
- Always use `log = logging.getLogger(__name__)` at beginning of all modules
- Always use `log.warning()/.info()/.debug()` -- this will automatically send messages to Sentry or the console
- No function should have more than 12 significant lines of code (cycolomatic complexity)
- Refactor carefully, using your IDE or search/replace with exact string match e.g. `r'\bfunction_or_variable_name\b', reviewing each change one at a time.

