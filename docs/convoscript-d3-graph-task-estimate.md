Hi Jon,

What do you think about $600 fixed price contract to creating an MVP of your d3 graph visualization?
* [x] $100 YAML file displayed as minimally styled force directed graph ($100) in d3.js+css+html
* [ ] $100 minimal force-directed graph of static yaml file displayed within django template (nudger/convoscript/templates/convoscript/convograph.HTML)
* [ ] $100 improve styling of graph:
    * compute depth and breadth integer with breadth first walk of the graph e.g. start = 0, first question = 1, second question and hint for first question are both depth 2, __next__ and __default__ do not increment the depth integer
    * only dotted integer node id f'{depth}.{breadth}' is displayed on nodes
    * mouseovers of node reveal state name and utterances for that state
* [ ] $100 improve layout of graph
    * creating "strata" of invisible nodes at each depth and breadth integer in a tree
    * connect invisible spring (attactor) edges to the corresponding nodes that share those depth and breadth coordinates
    * adjust force and damping constants
    * allow user to drag nodes around to untangle the graph
* [ ] $100 connect the graph visualization to the Django nudger/convoscript/models.py so that for a fixed (hard coded bot name) a SQL query of the State and Trigger tables retrieves the graph structure for that bot and displays it in the graph
* [ ] $100 use the users bot name to compose the database query so that a user's bot is the one whose graph is displayed and it is refreshed with a button on the qary.ai/convoscript/ page
