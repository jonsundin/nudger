# README ConvoHub DevOps

### Production server on render

Three services/servers on render.com:

1. nudger-main: django app (daphne async web server) `daphne -b 0.0.0.0 nudger.asgi:application`
2. nudger-celery-main render server: start-celery.sh (with a copy of django app to be able to run django tasks)
3. nudger-redis: RAM database for django to communicate tasks to celery server
