# Fresh Installation Database and Admin User Setup

## Background
You will need to set up a database when creating a new instance of this project.  Django will set up the database.  Due to complexities with the model relationships, you will need to manually create a superuser.  

## Set up steps

### Add Media folder
Add a media folder to the nudger folder.  It should be at the same level as the `docs/`, `users/` folders.  Inside there should be a default.jpg image for profile pictures.

### Add Migrations folder to Users app folder
Add a migrations folder to the users folder.  Inside the migrations folder, there should be an empty \_\_init__.py file.

### Set up database tables

    >>> python manage.py makemigrations
    >>> python manage.py migrate

### Make admin user
Run the createsuperuser command.  Add the necessary admin information.

    >>> python manage.py createsuperuser
