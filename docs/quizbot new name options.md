The nudger django project will soon be refactored as `django-qary`. 
The django application `quizbot` needs a new name too, here are some options:

- CMS (Conversation Management System)
- conms (Conversation Management System)
- convoms (Conversation Management System)
- convo_man (Conversation Manager)
- convo_manager (Conversation Manager)
- conman (Conversation Manager)
- qary manager
- qary studio
- convo studio

