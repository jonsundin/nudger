# Processes for Engaging in Nudger Merge Requests and Staging Deployment


#### Overview
Nudger is a chatbot development platform that relies on a collection of applications and packages as well as external storage and hosting platforms.  Successfully deploying Nudger requires attention to all of these systems, including:
* the Django chatbot development platform application
* the Svelte chatbot widget application
* the Digital Ocean object storage for files
* Celery / Redis for task scheduling and handling
* Render for deployments

As development branches mature, they must be merged into `staging` for inclusion in the deployed application.  Merge requests, and the processes for working with them, must take into consideration note only code changes for additional functionality but also the implications of those changes on the deployment systems.  These considerations should be addressed in each merge request from creation to review to acceptance/merge.


#### Responsibilities of Merge Request Participants
These roles may overlap.  They are not necessarily Git-specific roles.  However, these roles attempt to define the responsibilities of each type of participant in a merge request.  Some participants may take on more than one role as a branch develops.

* Initiator (Merge request creator)
	* Clearly defines the purpose of the merge request.  For example, the initiator may describe the problem the merge request is trying to solve, as well as a proposed solution.

* Contributor
	* Makes contributions that directly address the scope outlined by the initiator (merge request creator).
	* Focus on the problem at hand.  Until the problem in the original scope is solved, do not make stylistic or out-of-scope changes.  If you identify stylistic improvements, wait until the original problem is solved and then refine the style.  If you identify another issue not related to the scope of the merge request, make a new merge request.  Breaking changes can easily be rolled back to a viable commit if it's clear where a viable commit was.  The exception to this is branches focused on stylistic improvements.
	* Tests code after making changes.

* Assignee
	* Marks/unmarks the request as a draft
	* Completes the merge request
	* Merges new versions of staging into the branch
	* Merges the branch into staging
	* Updates staging deployment based on the merge request
	* Tests code prior to completing the merge request

* Reviewer
	* Comments on code style
	* Suggests improvements for code
	* Tests code before commenting and prior to approving a merge request


#### Merge and Deploy Procedures
* Creating a merge request
	* Create a new branch for your changes: `git checkout -b {new-branch-name} staging`
	* Run tests locally `python manage.py test -v 2`
	* Run manual tests (1. log in, 2. view chat widget, 3. upload conversation, 4. view uploaded chat in chat widget)
		* If any step fails, check to make sure your local environment is functional based on comments from the team.
		* If your local environment is set up correctly, notify the team that there may be an issue with `staging`.
		* If your work doesn't relate to the error, you may want to continue development on your feature while ensuring you stay aware of ongoing fixes.

* Requesting reviewers
	* Run tests locally `python manage.py test -v 2`
	* Run manual tests (1. log in, 2. view chat widget, 3. upload conversation, 4. view uploaded chat in chat widget)
	* The Svelte chat widget needs to be able to communicate with the relevant systems to function correctly, including 1) the backend via the `context` object, 2) Digital Ocean, and 3) the /static/ folder.  If your changes affect any of these three things, you will need to make corresponding changes to the Svelte application by:
		* rebuilding and redeploying Svelte chat widget
			* INSERT STEPS FOR WORKING WITH SVELTE HERE  
		* local testing
			* INSERT STEPS FOR WORKING WITH SVELTE HERE 
	 * Add a clear description of the purpose of the branch by describing (You may benefit from the structure of the `merge_request_template`: Click "Choose a template" to see it):
		* What work the merge request encompasses
		* What additional .env variables are added
		* What database/model changes your work necessitates
		* Any specific things the reviewers should look for
		* Any important notes about why you approached the objective of the merge request in the way that you did
	* Tag any relevant reviewers in the merge request (Note: No one should be tagged until the tests are passing)

* Reviewing a merge request
	* Run tests locally `python manage.py test -v 2`
	* Run manual tests (1. log in, 2. view chat widget, 3. upload conversation, 4. view uploaded chat in chat widget)
		* If any step fails, check to make sure your local environment is functional based on comments from the description of the merge request.
		* If your local environment is set up correctly, note in a comment in the merge request anything that's broken and suggest fixes.  Tag the assignee to ensure they receive notification about the failure.
	 * Address any specific things you were asked to look at
	 * Put comments regarding proposed changes in the relevant file near the relevant line of code as a FIXME or TODO.
		 * FIXME - suggested to be fixed in this merge request
		 * TODO - suggested to be addressed at some point in the future, but not necessarily for this merge request
	* If you make changes, make incremental changes and commit often.  Afterwards, provide a summary of all the changes you made and why in the merge request comment section.
	* If you make changes, rerun tests locally (`python manage.py -v 2`) and rerun manual tests.  Fix any new errors that have come up, or revert your changes.

* Completing a merge request / deployment to staging
	1. Run tests locally `python manage.py test -v 2`
	2. Run manual tests (1. log in, 2. view chat widget, 3. upload conversation, 4. view uploaded chat in chat widget).  Fix failures if there are any.
	3. Merge the branch and make sure "Delete source branch" is checked.
	4. Render `nudger-staging` automatically tries redeployment.  Ensure it passes. 
		* Make sure new .env variables are added to the relevant **Environment group** on Render.
		* If the deployment doesn't pass, create a merge request for remediation and flag the reason for failure.
	5. Render `nudger-celery-staging` does not automatically redeploy.  Restart it, and make sure it redeploys successfully.  
		* If it doesn't pass, create a merge request for remediation and flag the reason for failure.
	6. The Svelte chat widget needs to be able to communicate with the relevant systems to function correctly, including 1) the backend via the `context` object, 2) Digital Ocean, and 3) the /static/ folder.  If your changes affect any of these three things, you will need to make corresponding changes to the Svelte application by:
		* rebuilding and redeploying Svelte chat widget
			* INSERT STEPS FOR WORKING WITH SVELTE HERE  
		* local testing
			* INSERT STEPS FOR WORKING WITH SVELTE HERE  
	7. If migrations have been made, you may need to flush the database and recreate it.
		* INSERT HOW THIS IS DONE HERE
	8. Flag in Slack the status of the deploy post-merge request.  If it succeeds, tell everyone in `nudger`.  If it fails, tell everyone in `nudger` and put the failure cause and a link to the new remediation merge request in the comments.
	9. Upon noticing a merge request has been successfully implemented, all open merge request "assignees" should 1) `git pull` their local `staging`, 2) merge the new `staging` branch into their active development branch, 3) push the staging merge to their merge request.
