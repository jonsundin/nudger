# Django quizbot.models v1

```python
In [1]: convo_set = Convo.objects.all()
In [3]: [(c.name, c.user.username) for c in convo_set]
Out[3]: 
[('default', 'djadmin'),
 ('botnar microgrants', 'vlad123snisar'),
 ('multy by two', 'vlad123snisar'),
 ('botnar/microgrants12', 'vlad123snisar')]
In [4]: convo = Convo.objects.get(name='multy by two')
In [5]: convo.name
Out[5]: 'multy by two'
In [6]: convo.description
Out[6]: 'in order'
In [7]: convo.user
Out[7]: <User: vlad123snisar>
In [8]: convo.user.email
Out[8]: 'email@gmail.com'

In [9]: State.objects.filter(convo=convo)
FieldError: Cannot resolve keyword 'convo' into field. Choices are: checkpoint, checkpoint_id, id, interactiondata, level, message, next_states, rel_from_state, rel_to_state, state_name, update_args, update_context, update_kwargs

In [10]: Checkpoint.objects.filter(convo=convo)
Out[10]: <QuerySet []>
In [11]: [cp for cp in Checkpoint.objects.all()]
Out[11]: 
[<Checkpoint: Checkpoint object (2)>,
 <Checkpoint: Checkpoint object (1)>,
 <Checkpoint: Checkpoint object (3)>,
 <Checkpoint: Checkpoint object (4)>,
 <Checkpoint: Checkpoint object (5)>,
 <Checkpoint: Checkpoint object (6)>,
 <Checkpoint: Checkpoint object (7)>,
 <Checkpoint: Checkpoint object (8)>,
 <Checkpoint: Checkpoint object (9)>,
 <Checkpoint: Checkpoint object (10)>,
 <Checkpoint: Checkpoint object (11)>,
 <Checkpoint: Checkpoint object (12)>,
 <Checkpoint: Checkpoint object (13)>]

In [17]: cp = Checkpoint.objects.all()[12]
In [18]: cp._meta.fields
Out[18]: 
(<django.db.models.fields.BigAutoField: id>,
 <django.db.models.fields.related.ForeignKey: user>,
 <django.db.models.fields.DateTimeField: created_on>,
 <django.db.models.fields.CharField: description>,
 <django.db.models.fields.BooleanField: published>,
 <django.db.models.fields.related.ForeignKey: convo>)
In [19]: cp.convo.name
Out[19]: 'botnar/microgrants12'
In [20]: [cp.convo.name for cp in Checkpoint.objects.all()]
Out[20]: 
['default',
 'default',
 'botnar microgrants',
 'botnar microgrants',
 'botnar microgrants',
 'botnar microgrants',
 'botnar microgrants',
 'botnar microgrants',
 'botnar microgrants',
 'botnar microgrants',
 'botnar microgrants',
 'botnar microgrants',
 'botnar/microgrants12']
In [21]: Checkpoint.objects.filter(convo__name='default').count()
Out[21]: 2
In [22]: queryset = Checkpoint.objects.filter(convo__name='default')
In [25]: cp = list(queryset.order_by('created_on'))[-1]
```

## SSH

To access the production server you will need the SSH link from render (that is regenerated on each deployment).
And you will need to create an SSH key so you can upload the public half of the key to your Render account (or ask someone to add a key for you).
Render only works with ECDSA keys (not RSA or id_rsa keys).

```bash
$ ssh -i ~/.ssh/ecdsa_private_key_file srv-abcd123zxk99is0undoj0@ssh.oregon.render.com
==> Using Node version 14.17.0 (default)
==> Docs on specifying a Node version: https://render.com/docs/node-version

render@srv-abcd123zxk99is0undoj0-786f855b5-f8fbb:~/project/src$ python manage.py shell_plus
{'default': {'NAME': 'db_nudger', 'USER': 'db_nudger', 'PASSWORD': 'd...ID', 'HOST': 'dpg-cei4gfpgp3jvlf1egkj0-a', 'PORT': '', 'CONN_MAX_AGE': 0, 'CONN_HEALTH_CHECKS': False, 'ENGINE': 'django.db.backends.postgresql'}}
Python 3.9.16 (main, Jul  8 2023, 18:00:43) 
Type 'copyright', 'credits' or 'license' for more information
IPython 8.10.0 -- An enhanced Interactive Python. Type '?' for help.

In [1]: convo_set = Convo.objects.all()
```