""" FIXME: python manage.py test does not discover these tests! """
import doctest
import unittest

from users import utils


testsuite = unittest.TestLoader().discover("..")


def load_tests(*args, **kwargs):
    test_all_doctests = unittest.TestSuite()
    for module in (utils,):
        test_all_doctests.addTest(
            doctest.DocTestSuite(
                module, optionflags=(doctest.ELLIPSIS | doctest.NORMALIZE_WHITESPACE)
            )
        )
    return test_all_doctests
