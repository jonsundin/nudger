from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as AuthUserAdmin
from django.contrib.auth.models import User

from users.models import Profile


class UserProfileInline(admin.StackedInline):
    """Captures admin form and adds profile inline"""

    model = Profile
    can_delete = False


class AccountsUserAdmin(AuthUserAdmin):
    def add_view(self, *args, **kwargs):
        """We remove inlines from add view to be able to create user without additional information"""
        self.inlines = []
        return super(AccountsUserAdmin, self).add_view(*args, **kwargs)

    def change_view(self, *args, **kwargs):
        """We add inlines to the change view"""
        self.inlines = [UserProfileInline]
        return super(AccountsUserAdmin, self).change_view(*args, **kwargs)


# To activate the new form we have to first unregister and register after
admin.site.unregister(User)
admin.site.register(User, AccountsUserAdmin)
