import json
import logging

from django.conf import settings
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import AnonymousUser
from django.contrib.auth.models import User
from django.shortcuts import render, redirect
from django.utils.datastructures import MultiValueDictKeyError
from django.http import JsonResponse

from quizbot.models import Convo
from users.helpers import digitalocean, image
from .forms import (
    ProfileRegisterForm,
    ProfileUpdateForm,
    UserRegisterForm,
    UserUpdateForm,
)
from .utils import make_serializable

log = logging.getLogger(__name__)

ENV = settings.ENV


def home(request):
    file_css = "polyface/polyface.css"
    file_js = "polyface/polyface.js"
    active_convo = (
        Convo.objects.filter(user=request.user).order_by("-activated_on").first()
    ) or Convo.objects.get(
        user=User.objects.get(username=ENV.get("DJANGO_DEFAULT_USERNAME"))
    )
    url = ENV.get("WEBSOCKET_URL") + f"/ws/quiz/{request.user}/{active_convo.id}"
    fetch_image_endpoint_url = "http://localhost:8000/quiz/bot_widget_icon"

    if isinstance(request.user, AnonymousUser):
        bot_picture = "/media/bot_widget_pics/default.jpg"
        if settings.USE_S3:
            bot_picture = settings.DEFAULT_DIGITALOCEAN_BUCKET_URL + bot_picture
    else:
        bot_picture = request.user.profile.bot_widget_image.url

    return render(
        request,
        "users/home.html",
        {
            "file_css": file_css,
            "file_js": file_js,
            "welcome_popup_on": json.dumps(False),
            "empty_message_allowed": json.dumps(False),
            "name": "Qary",
            "bot_picture": bot_picture,
            "fetch_image_endpoint_url": fetch_image_endpoint_url,
            "ws_url": url,
            "is_s3": json.dumps(str(settings.USE_S3) in (True, "True", "true")),
        },
    )


def about(request):
    return render(request, "users/about.html")


def register(request):
    user_form = UserRegisterForm()
    profile_form = ProfileRegisterForm()
    if request.method == "POST":
        user_form = UserRegisterForm(request.POST)
        profile_form = ProfileRegisterForm(request.POST)
        if user_form.is_valid() and profile_form.is_valid():
            user_form.save()
            user = User.objects.get(username=user_form.cleaned_data["username"])
            profile_form.save(user=user)
            messages.success(
                request, f"Your account has been created!  You are now able to login."
            )
            return redirect("login")
    return render(
        request,
        "users/register.html",
        {"user_form": user_form, "profile_form": profile_form},
    )


def healthcheck(request):
    """Pulsar and other apps to check the health of the deployed webapp at `healthcheck.json`

    Always return 200 and a valid json str.

    WARNING: Users can view their request info that is received by qary.ai here.
    """
    log.error(f"users.views.healthcheck with {request}")

    return JsonResponse(
        {
            "request": make_serializable(request),
            "request.session": make_serializable(request.session),
        }
    )


@login_required
def profile(request):
    if request.method == "POST":
        u_form = UserUpdateForm(request.POST, instance=request.user)
        try:
            request.FILES["bot_widget_image"] = image.resize_image(
                request.FILES["bot_widget_image"], 70, 70
            )
            request.FILES["profile_image"] = image.resize_image(
                request.FILES["profile_image"], 300, 300
            )
        except MultiValueDictKeyError:
            pass

        p_form = ProfileUpdateForm(
            request.POST, request.FILES, instance=request.user.profile
        )
        if u_form.is_valid() and p_form.is_valid():
            u_form.save()
            p_form.save()
            messages.success(request, f"Your account has been updated.")
            # Post GET redirect pattern - avoid running another POST request
            return redirect("profile")

    u_form = UserUpdateForm(instance=request.user)
    p_form = ProfileUpdateForm(instance=request.user.profile)

    context = {
        "u_form": u_form,
        "p_form": p_form,
        "profile_pic": digitalocean.get_private_image_url(
            "media/" + request.user.profile.profile_image.url.split("media/")[-1]
        )
        if settings.USE_S3
        else request.user.profile.profile_image.url,
    }

    return render(request, "users/profile.html", context)
