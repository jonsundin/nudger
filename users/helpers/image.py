import sys
from django.core.files.uploadedfile import InMemoryUploadedFile
from PIL import Image
from io import BytesIO


def resize_image(image_to_resize, width, height):
    image = Image.open(image_to_resize)
    resized_image = image.resize((width, height))

    resized_image_io = BytesIO()
    resized_image.save(resized_image_io, format=image.format)
    resized_image_io.seek(0)
    return InMemoryUploadedFile(
        resized_image_io,
        None,
        "resized." + image.format.lower(),
        "image/" + image.format.lower(),
        sys.getsizeof(resized_image_io),
        None,
    )
