import boto3
from django.conf import settings


def get_private_image_url(key, expiration=3600):
    """
    Returns a temporary publicly-accessible link of private image on DigitalOcean space
    :params:
        key: should be relative path to image within DigitalOcean space. E.g. If private image link have url https://nudger-space.nyc3.digitaloceanspaces.com/media/profile_pics/default.jpg the relative path would be media/profile_pics/default.jpg
    """
    session = boto3.session.Session()
    s3_client = session.client(
        "s3",
        region_name="nyc3",
        endpoint_url=settings.ENV.get("AWS_S3_ENDPOINT_URL"),
        aws_access_key_id=settings.ENV.get("AWS_ACCESS_KEY_ID"),
        aws_secret_access_key=settings.ENV.get("AWS_SECRET_ACCESS_KEY"),
    )

    # Generate a temporary URL with limited access for the image
    url = s3_client.generate_presigned_url(
        "get_object",
        Params={"Bucket": "nudger-space", "Key": key},
        ExpiresIn=expiration,
    )

    return url
