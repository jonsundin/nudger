from django.contrib.auth.models import User
from django.db import models
from phonenumber_field.modelfields import PhoneNumberField

from nudger.constants import (
    DEFAULT_BOT_WIDGET_IMAGE_PATH,
    DEFAULT_PROFILE_IMAGE_PATH,
    DEFAULT_CONVO_NAME,
)


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    convo_name = models.CharField(
        max_length=100, null=True, blank=True, default=DEFAULT_CONVO_NAME
    )
    profile_image = models.ImageField(
        default=DEFAULT_PROFILE_IMAGE_PATH, upload_to="profile_pics"
    )
    bot_widget_image = models.ImageField(
        default=DEFAULT_BOT_WIDGET_IMAGE_PATH, upload_to="bot_widget_pics"
    )
    phone_number = PhoneNumberField(blank=True, null=False, default="")
    does_accept_sms = models.BooleanField(blank=True, null=False, default=False)

    def __str__(self):
        return f"{self.user.username} Profile"
