from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.forms.widgets import Widget
from django.utils.html import format_html
from phonenumber_field.formfields import PhoneNumberField

from .models import Profile


class UserRegisterForm(UserCreationForm):
    email = forms.EmailField()

    class Meta:
        model = User
        fields = ["username", "email", "password1", "password2"]

    def save(self):
        passed_data = self.cleaned_data
        user = User.objects.create(
            username=passed_data["username"], email=passed_data["email"]
        )
        user.set_password(passed_data["password1"])
        user.save()


class SmsCheckboxWidget(Widget):
    def render(self, name, value, attrs=None, renderer=None):
        checkbox_html = f"""<div class="pretty p-svg p-curve">
            <input type="checkbox" name="{name}" {'checked' if value else ''}/>
            <div class="state p-success">
                <svg class="svg svg-icon" viewBox="0 0 20 20">
                    <path
                        d="M7.629,14.566c0.125,0.125,0.291,0.188,0.456,0.188c0.164,0,0.329-0.062,0.456-0.188l8.219-8.221c0.252-0.252,0.252-0.659,0-0.911c-0.252-0.252-0.659-0.252-0.911,0l-7.764,7.763L4.152,9.267c-0.252-0.251-0.66-0.251-0.911,0c-0.252,0.252-0.252,0.66,0,0.911L7.629,14.566z"
                        style="stroke: white;fill:white;"></path>
                </svg>
                <label>I would like to receive SMS messages</label>
            </div>
        </div>"""
        return format_html(checkbox_html)


class ProfileRegisterForm(forms.ModelForm):
    phone_number = PhoneNumberField(required=False)
    does_accept_sms = forms.BooleanField(
        widget=SmsCheckboxWidget(), required=False, label=""
    )

    class Meta:
        model = Profile
        fields = ["phone_number", "does_accept_sms"]

    def save(self, user: User):
        passed_data = self.cleaned_data
        Profile.objects.create(
            user=user,
            phone_number=passed_data["phone_number"],
            does_accept_sms=passed_data["does_accept_sms"],
        )


class UserUpdateForm(forms.ModelForm):
    email = forms.EmailField()

    class Meta:
        model = User
        fields = ["username", "email"]


class ProfileUpdateForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ["profile_image", "bot_widget_image"]
