"""
Test users and app views
"""
from pathlib import Path

from django.contrib.auth import get_user_model, authenticate
from django.shortcuts import reverse
from django.test import TestCase, Client
from rest_framework import status

from nudger.constants import ENV, DEFAULT_CONVOGRAPH_YAML_PATH
from scripts.create_default_convo_on_db import create_convo


class SigninTest(TestCase):
    """
    Tests user creation and authentication
    """

    user1 = {
        "username": "test_user",
        "password": "Pwd5544**",
        "email": "test_user@example.com",
    }

    def setUp(self):
        self.client = Client()
        self.user = get_user_model().objects.create_user(
            username=self.user1.get("username"),
            password=self.user1.get("password"),
            email=self.user1.get("email"),
        )

    def tearDown(self):
        self.user.delete()

    def test_user_can_authenticate(self):
        """Test a user can authenticate"""
        user = authenticate(
            username=self.user1.get("username"), password=self.user1.get("password")
        )
        self.assertTrue(user.is_authenticated)

    def test_wrong_username(self):
        """Test a wrong username can not authenticate"""
        user = authenticate(
            username="wrong_username", password=self.user1.get("password")
        )
        self.assertFalse(user)

    def test_wrong_password(self):
        """Test a wrong password can not authenticate"""
        user = authenticate(
            username=self.user1.get("username"), password="wrong_password"
        )
        self.assertFalse(user)


class SigninViewTest(TestCase):
    """
    Tests user creation and authentication
    """

    user1 = {
        "username": "test_user",
        "password": "Pwd5544**",
        "email": "test_user@example.com",
    }

    def setUp(self):
        self.client = Client()
        self.user = get_user_model().objects.create_user(
            username=self.user1.get("username"),
            password=self.user1.get("password"),
            email=self.user1.get("email"),
        )

    def tearDown(self):
        self.user.delete()

    def test_login_url(self):
        """Test the login url exists"""
        response = self.client.get(reverse("login"))
        self.assertEquals(response.status_code, status.HTTP_200_OK)

    def test_login_success(self):
        """Simulate a login request with valid credentials"""
        user = get_user_model().objects.get(username="test_user")
        response = self.client.post(
            reverse("login"), {"username": user.username, "password": user.password}
        )

        # Assert the response
        self.assertEqual(
            response.status_code, 200
        )  # Assuming a successful login returns a 200 status code

    def test_login_failure(self):
        """Simulate a login request with invalid credentials"""
        response = self.client.post(
            "/login/", {"username": self.user1["username"], "password": "wrongpass"}
        )

        # Assert the response
        self.assertEqual(
            response.status_code, 200
        )  # Assuming a failed login returns a 200 status code
        self.assertContains(
            response, "Please enter a correct username and password."
        )  # Assuming an error message is displayed


class AppViewsTest(TestCase):
    """
    Tests if the views exist
    """

    user1 = {
        "username": "test_user",
        "password": "Pwd5544**",
        "email": "test_user@example.com",
    }

    def setUp(self):
        self.client = Client()

        # To test the home view we need to create a default user
        self.default_user = get_user_model().objects.create_user(
            username=ENV.get("DJANGO_DEFAULT_USERNAME"),
            password=ENV.get("DJANGO_DEFAULT_PASSWORD"),
        )
        self.default_user_convo = create_convo(
            file_path=Path(DEFAULT_CONVOGRAPH_YAML_PATH),
            user=self.default_user,
            convo_name="default_user_convo_name",
            convo_description="default_user_convo_description",
        )

        self.staff_user = get_user_model().objects.create_user(
            username=self.user1["username"],
            email=self.user1["email"],
            password=self.user1["password"],
        )
        self.staff_user.is_staff = True
        self.staff_user.save()
        logged_in = self.client.login(
            username=self.user1["username"], password=self.user1["password"]
        )
        self.assertTrue(logged_in)

    def tearDown(self):
        self.client.logout()
        self.default_user.delete()
        self.staff_user.delete()
        self.default_user_convo.delete()

    def test_home_url(self):
        """Test home url exists"""

        response = self.client.get(reverse("users-home"))
        self.assertEquals(response.status_code, status.HTTP_200_OK)

    def test_about_url(self):
        """Test about url exists"""
        response = self.client.get(reverse("users-about"))
        self.assertEquals(response.status_code, status.HTTP_200_OK)

    def test_login_url(self):
        """Test login url exists"""
        response = self.client.get(reverse("login"))
        self.assertEquals(response.status_code, status.HTTP_200_OK)

    def test_logout_url(self):
        """Test logout url exists"""
        response = self.client.get(reverse("logout"))
        self.assertEquals(response.status_code, status.HTTP_200_OK)

    def test_password_reset_url(self):
        """Test password reset url exists"""
        response = self.client.get(reverse("password_reset"))
        self.assertEquals(response.status_code, status.HTTP_200_OK)

    def test_password_reset_done_url(self):
        """Test password reset done url url exists"""
        response = self.client.get(reverse("password_reset_done"))
        self.assertEquals(response.status_code, status.HTTP_200_OK)

    def test_password_reset_complete_url(self):
        """Test password reset complete url exists"""
        response = self.client.get(reverse("password_reset_complete"))
        self.assertEquals(response.status_code, status.HTTP_200_OK)

    def test_profile_url(self):
        """Test profile url exists"""
        response = self.client.get(reverse("profile"))
        self.assertEquals(response.status_code, status.HTTP_200_OK)

    def test_register_url(self):
        """Test register url exists"""
        response = self.client.get(reverse("register"))
        self.assertEquals(response.status_code, status.HTTP_200_OK)

    def test_quiz_system_url(self):
        """Test quiz system url exists"""
        response = self.client.get(reverse("bothub"))
        self.assertEquals(response.status_code, status.HTTP_200_OK)

    def test_dashboard_system_url(self):
        """Test dashboard url exists"""
        response = self.client.get(reverse("dashboard"))
        self.assertEquals(response.status_code, status.HTTP_200_OK)

    def test_upload_system_url(self):
        """Test upload url exists"""
        response = self.client.get(reverse("quizbot-upload"))
        self.assertEquals(response.status_code, status.HTTP_200_OK)

    def test_spreadsheet_upload_system_url(self):
        """Test spreadsheet upload url exists"""
        response = self.client.get(reverse("spreadsheet-upload"))
        self.assertEquals(response.status_code, status.HTTP_200_OK)
